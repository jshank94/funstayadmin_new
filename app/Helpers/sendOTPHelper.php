<?php

function sendOTP($otp, $mobile_no) 
{ 
    $smsGatewayConfigurations = config('sms_gateway');
    $smsGatewayEndpoint = $smsGatewayConfigurations['endpoint'];
    $smsGatewayParameters = $smsGatewayConfigurations['parameters'];
    $messageToSend = $otp;
    $recepientMobile = $mobile_no;

    $smsGatewayParameters['message'] = $messageToSend;
    $smsGatewayParameters['dest_mobileno'] = $recepientMobile;
    $queryStringParams = http_build_query($smsGatewayParameters);

    $curl = curl_init();
    curl_setopt ($curl, CURLOPT_URL, $smsGatewayEndpoint);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $queryStringParams);
    curl_exec ($curl);
    curl_close ($curl);

    return;
} 

?>