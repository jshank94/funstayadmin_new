<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use \Illuminate\Auth\Authenticatable;
    public function users()
    {
        return $this->belongsTo('App\User');
    }
    
    public function propertyListing()
    {
        return $this->hasMany('App\PropertyListing');
    }
}
