<?php
namespace App\Communication;

/**
 *
 */
class SMS
{
    private $type;
    private $url;
    private $auth_key;
    private $route;
    private $phone;
    private $sender;
    private $message;

    public function __construct()
    {
        $this->auth_key = config('sms_gateway.msg91.auth_key');
        $this->route = config('sms_gateway.msg91.transactional_route');
        $this->sender = config('sms_gateway.msg91.sender_id');
        $this->url = "http://api.msg91.com/api/sendhttp.php";
        $this->country = "91";
    }

    public function send($param)
    {
        $this->type = $param['type'];
        return $this->sendMessage($param);
    }

    private function sendMessage($param)
    {
        $data = $this->prepareMessage($param);
        return $this->sendSMS($data);
    }

    private function prepareMessage($param)
    {
        $this->phone = $param['phone'];
        switch ($this->type) {
            case 'otp':
                $this->message = urlencode("An OTP has generated for your Funstay account. OTP: ".$param['otp'].".");
                break;

            default:
                # code...
                break;
        }
        return true;
    }

    private function sendSMS($data)
    {
        $postData = array(
           'authkey' => $this->auth_key,
           'mobiles' => $this->phone,
           'message' => $this->message,
           'sender' => $this->sender,
           'route' => $this->route,
           "country" => $this->country,
        );
        $ch = curl_init();
        curl_setopt_array($ch, array(
           CURLOPT_URL => $this->url,
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_POST => true,
           CURLOPT_POSTFIELDS => $postData
        ));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        return $output;
    }
}
