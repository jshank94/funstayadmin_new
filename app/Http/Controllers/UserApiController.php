<?php

namespace App\Http\Controllers;

use App\Property;
use App\PropertyListing;
use App\InventoryData;
use App\User;
use App\userLoginOtp;

use App\Communication\SMS;

use Illuminate\Http\Request;

class UserApiController extends Controller
{
    public function postLogin(Request $request, SMS $sms)
    {
        $this->validate($request, [
            'mobile_no' => 'required',
        ]);

        $data = User::where(['mobile_no'=>$request->input('mobile_no'),'role_id'=>3])->first();
        if ($data === null) {
            return response()->json(['message' => 'Inavlid Credentials'], 404);
        }

        $generateOTP = generateOTP();

        // $hashOTP = bcrypt($generateOTP);

        $otpDB = new userLoginOtp();
        $otpDB->id_user = $data->id;
        $otpDB->otp = $generateOTP;
        $otpDB->save();

        // $sendUserOTP = sendOTP($generateOTP, $request->input('mobile_no'));
        $sms->send([
            'type' => 'otp',
            'otp' => $generateOTP,
            'phone' => $request->mobile_no
        ]);

        return response()->json(['message' => 'OTP Sent','id_user' => $data->id], 201);
    }

    public function otpVerify(Request $request)
    {
        $this->validate($request, [
            'otp' => 'required',
            'id_user' => 'required',
        ]);

        // $otpData = userLoginOtp::where(['otp'=>bcrypt($request->input('otp')),'id_user'=>$request->input('id_user')])->get();

        $otpData = userLoginOtp::where(['otp'=>$request->input('otp'),'id_user'=>$request->input('id_user')])->get();

        if (!empty($otpData)) {

            //Delete OTP
            $otpDelete = userLoginOtp::find($otpData[0]->id);
            $otpDelete->delete();

            //Get User Info
            $data = User::find($request->input('id_user'));
            return response()->json(['success'=>true,'user_data' => $data], 201);

        } else {
            return response()->json(['message' => 'Inavlid OTP'], 404);
        }
    }

    public function resendOtp(Request $request)
    {
        $this->validate($request, [
            'id_user' => 'required',
        ]);

        $generateOTP = generateOTP();

        // $hashOTP = bcrypt($generateOTP);

        $otpDB = new userLoginOtp();
        $otpDB->id_user = $request->input('id_user');
        $otpDB->otp = $generateOTP;
        $otpDB->save();

        $data = User::find($request->input('id_user'));

        $sendUserOTP = sendOTP($generateOTP, $data->mobile_no);

        return response()->json(['message' => 'OTP Sent'], 201);
    }
}
