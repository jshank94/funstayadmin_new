<?php

namespace App\Http\Controllers;

use App\User;
use App\Property;
use App\PropertyListing;
use App\PropertyRoomType;
use App\PropertyType;
use App\PropertyMeal;
use App\PropertyListAmenityMap;
use App\Amenity;
use App\PropertyImage;
use App\PropertyListImage;
use App\Tag;
use App\SubTag;
use App\TagType;
use App\PropertyListingTag;
use App\PropertyListingMealMap;
use App\PropertySeoData;
use App\InventoryData;
use App\InventorySource;

use Calendar;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;


class PropertyRoomController extends Controller
{
    public function getPropertyRooms(Request $request, $id_property){
        $request->session()->forget('propertyRoomCreate');
        $rooms = PropertyListing::where('id_property',$id_property)->get();
        return view('vendor/voyager/property-room-mappings/propertyRoomListing', ['rooms'=>$rooms]);  
    }
    
    public function editPropertyRoom(Request $request, $id_property_list){

        if(isset($request['submit'])){
            $this->validate($request, [
                'room_title' => 'required',
                'property' => 'required',
                'propertytype' => 'required',
                'roomtype' => 'required',
                'accommodation' => 'required|integer|between:1,500',
                'booking_type' => 'required',
                'min_stay' => 'required|integer|between:1,500',
                'max_stay' => 'required|integer|between:1,500',
                'extrabed' => 'integer|between:1,500',
                'no_of_rooms' => 'integer|between:1,500',
                'description' => 'required|max:1000',

            ]);

            $propertyRoom = PropertyListing::find($id_property_list);

            $propertyRoom->room_title = trim($request['room_title']);
            $propertyRoom->description = trim($request['description']);
            $propertyRoom->room_type = trim($request['roomtype']);
            $propertyRoom->property_type = trim($request['propertytype']);
            $propertyRoom->minimum_stay = trim($request['min_stay']);
            $propertyRoom->maximum_stay = trim($request['max_stay']);
            $propertyRoom->no_rooms = trim($request['no_of_rooms']);
            $propertyRoom->accommodates_no = trim($request['accommodation']);
            $propertyRoom->booking_type = trim($request['booking_type']);
            $propertyRoom->allowed_extra_bed = trim($request['extrabed']);
            $propertyRoom->is_active = 0;
            
            $message = 'There was an error';
            
            if ($propertyRoom->save()) {
                $message = 'Basic Info Updated!';
                return redirect()->route('addRoomPriceInfo')->with(['message' => $message]);
            }
        } 
        $propertyRoomInfo = PropertyListing::find($id_property_list);

        if(!empty($propertyRoomInfo)){
            $request->session()->put('propertyRoomCreate', $id_property_list);
            $properties = Property::find(Session::get('propertyCreate'));
            $propertyType = PropertyType::orderBy('created_at', 'desc')->get();
            $roomTypes = PropertyRoomType::orderBy('created_at', 'desc')->get();
            return view('vendor/voyager/property-room-mappings/addPropertyRoom', ['properties'=>$properties, 'propertyTypes'=>$propertyType,'propertyRoomInfo'=>$propertyRoomInfo, 'roomTypes'=>$roomTypes]);
        } else {
            return redirect('admin/properties')->with(['message' => 'Invalid Property']);
        }
    }
    
    public function addPropertyRoom(Request $request){

        $request->session()->forget('propertyRoomCreate');

        if(isset($request['submit'])){
            $this->validate($request, [
                'room_title' => 'required|unique:property_listings',
                'property' => 'required',
                'propertytype' => 'required',
                'roomtype' => 'required',
                'accommodation' => 'required|integer|between:1,500',
                'booking_type' => 'required',
                'min_stay' => 'required|integer|between:1,500',
                'max_stay' => 'required|integer|between:1,500',
                'no_of_rooms' => 'integer|between:1,500',
                'extrabed' => 'integer|between:1,500',
                'description' => 'required|max:1000',
            ]);
            
            $propertyRoom = new PropertyListing();
            $propertyRoom->room_title = trim($request['room_title']);
            $propertyRoom->description = trim($request['description']);
            $propertyRoom->room_type = trim($request['roomtype']);
            $propertyRoom->property_type = trim($request['propertytype']);
            $propertyRoom->minimum_stay = trim($request['min_stay']);
            $propertyRoom->maximum_stay = trim($request['max_stay']);
            $propertyRoom->no_rooms = trim($request['no_of_rooms']);
            $propertyRoom->accommodates_no = trim($request['accommodation']);
            $propertyRoom->booking_type = trim($request['booking_type']);
            $propertyRoom->id_property = trim($request['property']);
            $propertyRoom->allowed_extra_bed = trim($request['extrabed']);
            $propertyRoom->is_active = 0;

            $message = 'There was an error';

            if ($propertyRoom->save()) {
                $request->session()->put('propertyRoomCreate', $propertyRoom->id);
                $message = 'Basic Info Added!';
                return redirect()->route('addRoomPriceInfo')->with(['message' => $message]);
            }
        } 
    
        // $properties = Property::orderBy('created_at', 'desc')->get();
        $properties = Property::find(Session::get('propertyCreate'));
        $roomTypes = PropertyRoomType::orderBy('created_at', 'desc')->get();
        $propertyType = PropertyType::orderBy('created_at', 'desc')->get();

        return view('vendor/voyager/property-room-mappings/addPropertyRoom', ['properties'=>$properties, 'propertyTypes'=>$propertyType, 'roomTypes'=>$roomTypes]);
    }

    public function addRoomPriceInfo(Request $request){

        if(isset($request['submit'])){
            $this->validate($request, [
                'currency' => 'required',
                'per_person' => 'integer|between:1,100000',
                'per_unit' => 'integer|between:1,100000',
                'max_guest' => 'integer|between:1,100000',
                'extra_bed_price' => 'integer|between:1,100000',
                'child_price' => 'integer|between:1,100000',
                'weekend_price' => 'integer|between:1,100000',
            ]);

            $propertyRoom = PropertyListing::find(Session::get('propertyRoomCreate'));
            $propertyRoom->price_per_person = (!empty($request['per_person']))?trim($request['per_person']):0;
            $propertyRoom->price_per_unit = (!empty($request['per_unit']))?trim($request['per_unit']):0;
            $propertyRoom->number_guests = (!empty($request['max_guest']))?trim($request['max_guest']):0;
            $propertyRoom->extra_bed_price = (!empty($request['extra_bed_price']))?trim($request['extra_bed_price']):0;
            $propertyRoom->extra_child_price = (!empty($request['child_price']))?trim($request['child_price']):0;
            $propertyRoom->room_weekend_price = (!empty($request['weekend_price']))?trim($request['weekend_price']):0;
            $propertyRoom->currency = trim($request['currency']);
           
            $propertyRoom->save();

            $message = 'Price Info Added!';

            return redirect()->route('addRoomMealPlan')->with(['message' => $message]);

        }
        
        if(Session::get('propertyRoomCreate')){
            // $property = PropertyListing::find($request->session()->get('propertyRoomCreate'))->first();
            $propertyRoomInfo = PropertyListing::where('id',Session::get('propertyRoomCreate'))->get();
            
            if(!empty($propertyRoomInfo)){
                return view('vendor/voyager/property-room-mappings/addRoomPriceInfo', ['propertyRoomInfo'=>$propertyRoomInfo]);
            } else {
                return view('vendor/voyager/property-room-mappings/addRoomPriceInfo');
            }
        }  else {
            return redirect()->route('addPropertyRoom');
        }
    }

    public function addRoomMealPlan(Request $request){
        
        if(isset($request['submit'])){
            // $this->validate($request, [
            //     'id_mealplan' => 'required',
            //     'meal_plan_price' => 'required|integer|between:1,10000',
            // ]);

            if(Session::get('propertyRoomCreate')){
                PropertyListingMealMap::where('id_property_listing',Session::get('propertyRoomCreate'))->delete();
            }
            
            $allMealMap = [];
            foreach($request['id_mealplan'] as $key=>$val){
                $propertyRoomMealMap = new PropertyListingMealMap;
                $propertyRoomMealMap->id_property_listing = trim(Session::get('propertyRoomCreate'));
                $propertyRoomMealMap->id_meal = trim($val);
                $propertyRoomMealMap->price = trim($request['meal_plan_price'][$key]);
                $allMealMap[] = $propertyRoomMealMap->attributesToArray();
            }

            PropertyListingMealMap::insert($allMealMap);

            $message = 'Meals Added!';

            return redirect()->route('addRoomImages')->with(['message' => $message]);
        } 
        $mealplans = PropertyMeal::orderBy('created_at', 'desc')->get();

        if(Session::get('propertyRoomCreate')){
            $propertyRoomInfo = PropertyListingMealMap::where('id_property_listing',Session::get('propertyRoomCreate'))->get();
            if(count($propertyRoomInfo)>0){
                return view('vendor/voyager/property-room-mappings/addRoomMealInfo', ['mealplans'=>$mealplans, 'propertyRoomInfo'=>$propertyRoomInfo]);
            } else {
                return view('vendor/voyager/property-room-mappings/addRoomMealInfo',['mealplans'=>$mealplans]);
            }
        } else {
            return redirect()->route('addPropertyRoom');
        }
    }
    
    public function addRoomImages(Request $request){

        if(isset($request['submit'])){
            $this->validate($request, [
                'image' => 'required',
                'image.*' => 'image|mimes:jpeg,png,jpg|max:8048'
            ]);

            if($request->hasfile('image'))
            {
                foreach($request->file('image') as $key=>$val){
                    $filename = random_strings(8);
                    $storageName = 'property-room-public';
                    storeFile($storageName, $filename, $val);
                    
                    $propertyRoomImage = new PropertyListImage();
                    $propertyRoomImage->id_property_list = trim(Session::get('propertyRoomCreate'));
                    $propertyRoomImage->image = trim($filename);
                    $propertyRoomImage->save();    
                }
            }

            $message = 'Image Added!';

            return redirect()->back()->with(['message' => $message]);

        } 
        if(Session::get('propertyRoomCreate')){
            $propertyImgInfo = PropertyListImage::where('id_property_list',Session::get('propertyRoomCreate'))->get();
            if(!empty($propertyImgInfo)){
                return view('vendor/voyager/property-room-mappings/addPropertyRoomImages', ['propertyImages'=>$propertyImgInfo]);
            } else {
                return view('vendor/voyager/property-room-mappings/addPropertyRoomImages');
            }
        } else {
            return redirect()->route('addPropertyRoom');
        }
    }

    public function addRoomAmenity(Request $request){

        if(isset($request['submit'])){
            $this->validate($request, [
                'amenities' => 'required',
            ]);
            
            if(Session::get('propertyRoomCreate')){
                PropertyListAmenityMap::where('id_property_list',Session::get('propertyRoomCreate'))->delete();
            }
            
            $allAmenities = [];
            foreach($request['amenities'] as $key=>$val){
                $propertyRoomAmenity = new PropertyListAmenityMap();
                $propertyRoomAmenity->id_property_list = trim(Session::get('propertyRoomCreate'));
                $propertyRoomAmenity->id_amenity = trim($val);
                $allAmenities[] = $propertyRoomAmenity->attributesToArray();
            }

            PropertyListAmenityMap::insert($allAmenities);
            
            $message = 'Amenity Info Added!';

            return redirect()->route('addRoomTags')->with(['message' => $message]);

        } 
        $amenities= Amenity::orderBy('created_at', 'desc')->get();
        if(Session::get('propertyRoomCreate')){
            $propertyRoomAmenityInfo = PropertyListAmenityMap::where('id_property_list',Session::get('propertyRoomCreate'))->get();
            if(!empty($propertyRoomAmenityInfo)){
                return view('vendor/voyager/property-room-mappings/addRoomAmenities', ['amenities' => $amenities, 'propertyRoomAmenities'=>$propertyRoomAmenityInfo]);
            } else {
                return view('vendor/voyager/property-room-mappings/addRoomAmenities', ['amenities' => $amenities]);
            }
        } else {
            return redirect()->route('addPropertyRoom');
        }
    }

    public function addRoomTags(Request $request){
        
        if(isset($request['submit'])){
            $this->validate($request, [
                'tagtype' => 'required',
            ]);
            if(Session::get('propertyRoomCreate')){
                PropertyListingTag::where('id_property_list',Session::get('propertyRoomCreate'))->delete();
            }

            if(isset($request['tags'])){
                $allTags = [];
                foreach($request['tags'] as $key=>$val){
                    $propertyRoomTag = new PropertyListingTag();
                    $propertyRoomTag->id_property_list = trim(Session::get('propertyRoomCreate'));
                    $propertyRoomTag->id_tag = trim($val);
                    $allTags[] = $propertyRoomTag->attributesToArray();
                }
                PropertyListingTag::insert($allTags);
            }
            if(isset($request['subtags'])){
                $allSubTags = [];
                foreach($request['subtags'] as $key=>$val){
                    $propertyRoomTag = new PropertyListingTag();
                    $propertyRoomTag->id_property_list = trim(Session::get('propertyRoomCreate'));
                    $propertyRoomTag->id_sub_tag = trim($val);
                    $allSubTags[] = $propertyRoomTag->attributesToArray();
                }
                PropertyListingTag::insert($allSubTags);
            }
            
            $message = 'Room Added!';
            // $request->session()->forget('propertyRoomCreate');
            // return redirect('admin/properties')->with(['message' => $message]);
            return redirect()->back()->with(['message' => $message]);

        } 
        $tagTypes= TagType::orderBy('created_at', 'desc')->get();
        $tags= Tag::orderBy('created_at', 'desc')->get();
        $subtags= SubTag::orderBy('created_at', 'desc')->get();
        if(Session::get('propertyRoomCreate')){
            $propertyRoomTags = PropertyListingTag::where('id_property_list',Session::get('propertyRoomCreate'))->get();
            if(!empty($propertyRoomTags)){
                return view('vendor/voyager/property-room-mappings/addRoomTags', ['tags'=>$tags, 'subTags'=>$subtags, 'tagTypes' => $tagTypes, 'propertyRoomTags'=>$propertyRoomTags]);
            } else {
                return view('vendor/voyager/property-room-mappings/addRoomTags', ['tags'=>$tags, 'subTags'=>$subtags, 'tagTypes' => $tagTypes]);
            }
        } else {
            return redirect()->route('addPropertyRoom');
        } 
    }
    
    public function addRoomSEO(Request $request){

        if(isset($request['submit'])){
            $this->validate($request, [
                'meta_title' => 'required',
                'meta_description' => 'required',
                'meta_keyword' => 'required'
            ]);

            $propertySeo = new PropertySeoData();
            $propertySeo->id_listing = trim(Session::get('propertyRoomCreate'));
            $propertySeo->meta_title = trim($request['meta_title']);
            $propertySeo->meta_desc = trim($request['meta_description']);
            $propertySeo->meta_tags = trim($request['meta_keyword']);
           
            $propertySeo->save();

            $message = 'SEO Info Added!';

            return redirect()->back()->with(['message' => $message]);

        }
        
        if(Session::get('propertyRoomCreate')){
            
            $propertyRoomInfo = PropertySeoData::where('id_listing',Session::get('propertyRoomCreate'))->get();
            
            if(count($propertyRoomInfo)>0){
                return view('vendor/voyager/property-room-mappings/addRoomSEOInfo', ['propertyRoomInfo'=>$propertyRoomInfo]);
            } else {
                return view('vendor/voyager/property-room-mappings/addRoomSEOInfo');
            }
        }  else {
            return redirect()->route('addPropertyRoom');
        }
    }

    public function addCalendar(Request $request){

        if(isset($request['submit'])){
            // $this->validate($request, [
            //     'meta_title' => 'required',
            //     'meta_description' => 'required',
            //     'meta_keyword' => 'required'
            // ]);

            // $propertySeo = new PropertySeoData();
            // $propertySeo->id_listing = trim(Session::get('propertyRoomCreate'));
            // $propertySeo->meta_title = trim($request['meta_title']);
            // $propertySeo->meta_desc = trim($request['meta_description']);
            // $propertySeo->meta_tags = trim($request['meta_keyword']);
           
            // $propertySeo->save();

            $message = 'Inventory Info Added!';

            return redirect()->back()->with(['message' => $message]);

        }
        
        if(Session::get('propertyRoomCreate')){
            
            $propertyRoomInfo = InventoryData::where(['id_listing'=>Session::get('propertyRoomCreate'),'id_property'=>Session::get('propertyCreate'),])->get();
            $inventorySources = InventorySource::all();
            // if(count($propertyRoomInfo)>0){
                $ims = [];
                if(count($propertyRoomInfo)>0){
                    foreach($propertyRoomInfo as $key => $val){
                        $ims[] = Calendar::event(
                            $val->customer_name.
                            true,
                            new \DateTime($val->booked_date)
                        );
                    }
                }
                $calendar_details = Calendar::addEvents($ims);
                return view('vendor/voyager/property-room-mappings/addRoomCalendarInfo', ['propertyRoomInfo'=>$propertyRoomInfo, "inventorySources" =>$inventorySources, "calendar_details"=> $calendar_details]);
            // } else {
            //     return view('vendor/voyager/property-room-mappings/addRoomCalendarInfo',["inventorySources" =>$inventorySources]);
            // }
        }  else {
            return redirect()->route('addPropertyRoom');
        }
    }

    public function deletePropertyRoom($id_property_list){
        if(isset($id_property_list) && ($id_property_list>0)){
            
            $property = PropertyListing::find($id_property_list);
            $message = 'Property Room Data Not found!';
            if(!empty($property)){
                $property->is_deleted = 'Y';
                $property->status = 0;
                $property->update();
                $message = 'Property Room Deleted!';
            }

            return redirect()->back();

        } 
    }
    
    public function deletePropRoomImage($id_property_list_img){
        if(isset($id_property_list_img) && ($id_property_list_img>0)){
            PropertyListImage::find($id_property_list_img)->delete();
            // Storage::delete($old_filename);
            return redirect()->back();
        } 
    }


    public function updateRoomCover(Request $request){
        $id = $request['img_id'];
        $val = $request['cover_val'];
        $message = 'Property Room Image Data Not found!';
        
        $prop_img = PropertyListImage::find($id);
        if(!empty($prop_img)){
            
            PropertyListImage::where('id_property_list',$prop_img->id_property_list)->update(['is_cover' => 'N']);

            $prop_img->is_cover = $val;
            $prop_img->update();
            $message = 'Property Room Image Cover Set!';
        }

        return $message;

    }

    public function updateRoomSerial(Request $request){
        $id = $request['img_id'];
        $val = $request['serial_val'];
        
        $message = 'Property Room Image Data Not found!';
        
        $prop_img = PropertyListImage::find($id);
        if(!empty($prop_img)){
            
            $check_serial_exist = PropertyListImage::where('id_property_list',$prop_img->id_property_list)->where('ordernum',$val)->first();
            if(empty($check_serial_exist)){
                $prop_img->ordernum = $val;
                $prop_img->update();
                $message = 'Property Rooom Image Serail Set!'; 
            } else {
                $message = 'Property Room Serial Already Exist!';
            }
        }

        return $message;
    }

    public function updateRoomImgAlt(Request $request){
        $id = $request['img_id'];
        $val = $request['alt_val'];
        
        $message = 'Property Room Image Data Not found!';
        
        $prop_img = PropertyListImage::find($id);
        if(!empty($prop_img)){
            $prop_img->img_alt = $val;
            $prop_img->update();
            $message = 'Property Room Image Caption Set!'; 
        }

        return $message;
    }

    public function getAllTags($id_tag_type){
        $tags= Tag::where('id_tag_type',$id_tag_type)->get();        
        return json_encode($tags);
    }

    public function getAllSubTags($id_tag){
        $subtags= SubTag::where('id_tag',$id_tag)->get();        
        return json_encode($subtags);
    }
}
