<?php

namespace App\Http\Controllers;

use App\User;
use App\UserInfo;
use App\Property;
use App\PropertyVendorLanguage;
use App\Language;
use App\PropertyAmenityMap;
use App\Amenity;
use App\Address;
use App\City;
use App\State;
use App\Country;
use App\PropertyImage;
use App\TagType;
use App\Tag;
use App\SubTag;
use App\PropertyTag;
use App\UserHasRole;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;


class PropertyController extends Controller
{
    public function importRole(Request $request){   
        $users= User::offset(2400)->limit(200)->get(); 
        $info= UserInfo::get();  
        foreach($users as $key1=>$val1){
            // echo "<pre>";
            // print_r($val1['id']); 
            // echo "</pre>";
            // die();
            foreach($info as $key2=>$val2){
                // echo "<pre>";
                // print_r($val2['user_id']); 
                // echo "</pre>";
                // die();
                if($val1['id'] == $val2['id_user']){
                    $property = User::find($val1['id']);
                    $property->name = $val2['first_name'].' '.$val2['last_name'];
                    $property->save();
                }
            }
        } 
        echo "Done";       
    }

    public function editProperty(Request $request, $id_property){

        if(isset($request['submit'])){
            $this->validate($request, [
                'title' => 'required|max:100',
                'id_user' => 'required',
                'language' => 'required',
            ]);

            $property = Property::find($id_property);
            $property->title = trim($request['title']);
            $property->id_user = trim($request['id_user']);
            
            if ($property->save()) {
                $message = 'Basic Info Updated!';

                if(Session::get('propertyCreate')){
                    PropertyVendorLanguage::where('id_property',Session::get('propertyCreate'))->delete();
                }
    
                $allLanguages = [];
                foreach($request['language'] as $key=>$val){
                    $vendorLanguage = new PropertyVendorLanguage();
                    $vendorLanguage->id_property = trim(Session::get('propertyCreate'));
                    $vendorLanguage->id_language = trim($val);
                    $allLanguages[] = $vendorLanguage->attributesToArray();
                }

                PropertyVendorLanguage::insert($allLanguages);

                return redirect()->route('addLocationInfo')->with(['message' => $message]);
            }
        } 

        $property = Property::find($id_property);
        $languages = Language::orderBy('created_at', 'desc')->get();
        if(!empty($property)){

            $request->session()->put('propertyCreate', $id_property);
            $users = User::where('role_id',3)->orderBy('created_at', 'desc')->get();
            $propertyVendInfo = PropertyVendorLanguage::where('id_property',$id_property)->get();
            
            return view('vendor/voyager/properties/addProperty', ['users' => $users, 'property'=>$property, 'languages' => $languages, 'vendLanguages'=>$propertyVendInfo, 'idProperty'=>$id_property]);
        } else {
            return redirect('admin/properties')->with(['message' => 'Invalid Property']);
        }
    }
    
    public function addProperty(Request $request){

        $request->session()->forget('propertyCreate');
        
        if(isset($request['submit'])){
            $this->validate($request, [
                'title' => 'required|unique:properties',
                'id_user' => 'required',
                'language' => 'required',
            ]);

            $property = new Property();
            $property->title = trim($request['title']);
            $property->id_user = trim($request['id_user']);
            $property->is_active = 0;
            $property->is_all_info = 1;

            if ($property->save()) {
                
                $request->session()->put('propertyCreate', $property->id);
    
                $allLanguages = [];
                foreach($request['language'] as $key=>$val){
                    $vendorLanguage = new PropertyVendorLanguage();
                    $vendorLanguage->id_property = trim(Session::get('propertyCreate'));
                    $vendorLanguage->id_language = trim($val);
                    $allLanguages[] = $vendorLanguage->attributesToArray();
                }

                PropertyVendorLanguage::insert($allLanguages);

                $message = 'Basic Info Added!';

                return redirect()->route('addLocationInfo')->with(['message' => $message]);
            }
        } 

        $languages = Language::orderBy('created_at', 'desc')->get();
        $users = User::where('role_id',3)->orderBy('created_at', 'desc')->get();

        return view('vendor/voyager/properties/addProperty', ['users' => $users, 'languages' => $languages]);
    }

    public function addLocationInfo(Request $request){
        
        if(isset($request['submit'])){
            $this->validate($request, [
                'address1' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'city' => 'required',
                'pincode' => 'required',
                'state' => 'required',
                'country' => 'required',
            ]);
            
            if(isset($request['id_address'])){
                $propertyLocation = Address::find($request['id_address']);
            } else {
                $propertyLocation = new Address();
            }

            $propertyLocation->id_property = trim($request->session()->get('propertyCreate'));
            $propertyLocation->address_line_1 = trim($request['address1']);
            $propertyLocation->address_line_2 = trim($request['address2']);
            $propertyLocation->latitude = trim($request['latitude']);
            $propertyLocation->longitude = trim($request['longitude']);
            $propertyLocation->id_city = trim($request['city']);
            $propertyLocation->id_state = trim($request['state']);
            $propertyLocation->zipcode = trim($request['pincode']);
            $propertyLocation->id_country = trim($request['country']);

            $propertyLocation->save();

            $property = Property::find(Session::get('propertyCreate'));
            
            $propertyAllInfoCount = 1+$property->is_all_info;
            if($propertyAllInfoCount == 9){
                $property->is_active = 1;
            } else {
                $property->is_all_info = $propertyAllInfoCount;
            }

            $property->save();

            $message = 'Location Info Added!';

            return redirect()->route('addImages')->with(['message' => $message]);

        } 

        $cities= City::orderBy('created_at', 'desc')->get();
        $states= State::get();
        $countries= Country::orderBy('id', 'desc')->get();

        if(Session::get('propertyCreate')){
            $propertyLocInfo = Address::where('id_property',Session::get('propertyCreate'))->first();
            if(!empty($propertyLocInfo)){
                return view('vendor/voyager/properties/addLocationInfo', ['cities' => $cities, 'states' => $states, 'countries' => $countries, 'locationInfo'=>$propertyLocInfo]);
            }
            return view('vendor/voyager/properties/addLocationInfo', ['cities' => $cities, 'states' => $states, 'countries' => $countries]);
        }  else {
            return redirect()->route('addProperty');
        }
    }

    public function addImages(Request $request){

        if(isset($request['submit'])){
            $this->validate($request, [
                'image' => 'required',
                'image.*' => 'image|mimes:jpeg,png,jpg|max:8048'
            ]);

            if($request->hasfile('image'))
            {

                foreach($request->file('image') as $key=>$val)
                {
                    // $filename=$val->getClientOriginalName();
                    $filename = random_strings(8);
                    $storageName = 'property-public';
                    storeFile($storageName, $filename, $val);

                    $propertyImage = new PropertyImage();
                    $propertyImage->id_property = trim(Session::get('propertyCreate'));
                    $propertyImage->image = trim($filename);
                    $propertyImage->save(); 
                }
            }

            $property = Property::find(Session::get('propertyCreate'));
            
            $propertyAllInfoCount = 1+$property->is_all_info;
            if($propertyAllInfoCount == 9){
                $property->is_active = 1;
            } else {
                $property->is_all_info = $propertyAllInfoCount;
            }

            $property->save();

            $message = 'Image Added!';

            return redirect()->back()->with(['message' => $message]);

        } 
        if(Session::get('propertyCreate')){
            $propertyImgInfo = PropertyImage::where('id_property',Session::get('propertyCreate'))->get();
            if(!empty($propertyImgInfo)){
                return view('vendor/voyager/properties/addPropertyImages', ['propertyImages'=>$propertyImgInfo]);
            } else {
                return view('vendor/voyager/properties/addPropertyImages');
            }
        }  else {
            return redirect()->route('addProperty');
        }
    }

    public function addAmenity(Request $request){

        if(isset($request['submit'])){
            $this->validate($request, [
                'amenities' => 'required',
            ]);
            
            if(Session::get('propertyCreate')){
                PropertyAmenityMap::where('id_property',Session::get('propertyCreate'))->delete();
            }
            
            $allAmenities = [];
            foreach($request['amenities'] as $key=>$val){
                $propertyAmenity = new PropertyAmenityMap();
                $propertyAmenity->id_property = trim(Session::get('propertyCreate'));
                $propertyAmenity->id_amenity = trim($val);
                $allAmenities[] = $propertyAmenity->attributesToArray();
            }

            PropertyAmenityMap::insert($allAmenities);

            $property = Property::find(Session::get('propertyCreate'));
            
            $propertyAllInfoCount = 1+$property->is_all_info;
            if($propertyAllInfoCount == 9){
                $property->is_active = 1;
            } else {
                $property->is_all_info = $propertyAllInfoCount;
            }

            $property->save();

            $message = 'Amenity Info Added!';

            return redirect()->route('addTags')->with(['message' => $message]);

        } 
        $amenities= Amenity::orderBy('created_at', 'desc')->get();
        if(Session::get('propertyCreate')){
            $propertyAmenInfo = PropertyAmenityMap::where('id_property',Session::get('propertyCreate'))->get();
            if(!empty($propertyAmenInfo)){
                return view('vendor/voyager/properties/addAmenities', ['amenities' => $amenities, 'propertyAmenities'=>$propertyAmenInfo]);
            }
        }  else {
            return redirect()->route('addProperty');
        }

        return view('vendor/voyager/properties/addAmenities', ['amenities' => $amenities]);
    }

    public function addTags(Request $request){
        
        if(isset($request['submit'])){
            $this->validate($request, [
                'tags' => 'required',
                'tagtype' => 'required',
            ]);
            
            if(Session::get('propertyCreate')){
                PropertyTag::where('id_property',Session::get('propertyCreate'))->delete();
            }

            if(isset($request['tags'])){
                $allTags = [];
                foreach($request['tags'] as $key=>$val){
                    $propertyTag = new PropertyTag();
                    $propertyTag->id_property = trim(Session::get('propertyCreate'));
                    $propertyTag->id_tag = trim($val);
                    $allTags[] = $propertyTag->attributesToArray();
                }
                PropertyTag::insert($allTags);
            }

            if(isset($request['subtags'])){
                $allSubTags = [];
                foreach($request['subtags'] as $key=>$val){
                    $propertyTag = new PropertyTag();
                    $propertyTag->id_property = trim(Session::get('propertyCreate'));
                    $propertyTag->id_sub_tag = trim($val);
                    $allSubTags[] = $propertyTag->attributesToArray();
                }
                PropertyTag::insert($allSubTags);
            }
            
            $property = Property::find(Session::get('propertyCreate'));
            
            $propertyAllInfoCount = 1+$property->is_all_info;
            if($propertyAllInfoCount == 9){
                $property->is_active = 1;
            } else {
                $property->is_all_info = $propertyAllInfoCount;
            }

            $property->save();

            $message = 'Tag Added!';
            
            return redirect()->back()->with(['message' => $message]);

        } 

        $tagTypes= TagType::orderBy('created_at', 'desc')->get();
        $tags= Tag::orderBy('created_at', 'desc')->get();
        $subtags= SubTag::orderBy('created_at', 'desc')->get();

        if(Session::get('propertyCreate')){
            $propertyTags = PropertyTag::where('id_property',Session::get('propertyCreate'))->get();
            if(!empty($propertyTags)){
                return view('vendor/voyager/properties/addTags', ['tags'=>$tags, 'subTags'=>$subtags, 'tagTypes' => $tagTypes, 'propertyTags'=>$propertyTags]);
            } else {
                return view('vendor/voyager/properties/addTags', ['tags'=>$tags, 'subTags'=>$subtags, 'tagTypes' => $tagTypes]);
            }
        }  else {
            return redirect()->route('addProperty');
        }
    }

    public function addHouseRules(Request $request){

        if(isset($request['submit']) && ($request['submit']=='submit')){
            $this->validate($request, [
                'house_rules' => 'required|max:2000',
            ]);

            $property = Property::find(Session::get('propertyCreate'));
            $property->house_rules = trim($request['house_rules']);
            $propertyAllInfoCount = 1+$property->is_all_info;
            if($propertyAllInfoCount == 9){
                $property->is_active = 1;
            } else {
                $property->is_all_info = $propertyAllInfoCount;
            }

            $property->save();

            $message = 'Property & Rules Added!';

            return redirect()->route('addAdditionalInfo')->with(['message' => $message]);
        } 
        if(Session::get('propertyCreate')){
            $propertyRules = Property::where('id',Session::get('propertyCreate'))->get();
            
            if(!empty($propertyRules)){
                return view('vendor/voyager/properties/addHouseRules', ['rules'=>$propertyRules]);
            }
        }  else {
            return redirect()->route('addProperty');
        }
        return view('vendor/voyager/properties/addHouseRules');
    }

    public function addAdditionalInfo(Request $request){

        if(isset($request['submit']) && ($request['submit']=='submit')){
            $this->validate($request, [
                'additional_notes' => 'required|max:2000',
            ]);

            $property = Property::find(Session::get('propertyCreate'));
            $property->additional_notes = trim($request['additional_notes']);
            $propertyAllInfoCount = 1+$property->is_all_info;
            if($propertyAllInfoCount == 9){
                $property->is_active = 1;
            } else {
                $property->is_all_info = $propertyAllInfoCount;
            }

            $property->save();

            $message = 'Property & Rules Added!';
            
            // $request->session()->forget('propertyCreate');

            return redirect()->back()->with(['message' => $message]);
        } 
        if(Session::get('propertyCreate')){
            $propertyNotes = Property::where('id',Session::get('propertyCreate'))->get();
            
            if(!empty($propertyNotes)){
                return view('vendor/voyager/properties/addAdditionalNotes', ['notes'=>$propertyNotes]);
            }
        }  else {
            return redirect()->route('addProperty');
        }
        return view('vendor/voyager/properties/addHouseRules');
    }
    
    public function addActivities(){
        return view('vendor/voyager/properties/addActivities');
    }

    public function addNearBy(){
        return view('vendor/voyager/properties/addNearby');
    }

    public function deleteProperty($id_property){
        if(isset($id_property) && ($id_property>0)){
            
            $property = Property::find($id_property);
            $message = 'Property Data Not found!';
            if(!empty($property)){
                $property->is_deleted = 'Y';
                $property->is_active = 0;
                $property->update();
                $message = 'Property Deleted!';
            }

            return redirect()->back();

        } 
    }
    
    public function deletePropImage($id_property_img){
        if(isset($id_property_img) && ($id_property_img>0)){
            PropertyImage::find($id_property_img)->delete();
            // Storage::delete($old_filename);
            return redirect()->back();
        } 
    }

    public function updateCover(Request $request){
        $id = $request['img_id'];
        $val = $request['cover_val'];
        $message = 'Property Image Data Not found!';
        
        $prop_img = PropertyImage::find($id);
        if(!empty($prop_img)){
            
            PropertyImage::where('id_property',$prop_img->id_property)->update(['is_cover' => 'N']);

            $prop_img->is_cover = $val;
            $prop_img->update();
            $message = 'Property Cover Set!';
        }

        return $message;

    }

    public function updateSerial(Request $request){
        $id = $request['img_id'];
        $val = $request['serial_val'];
        
        $message = 'Property Image Data Not found!';
        
        $prop_img = PropertyImage::find($id);
        if(!empty($prop_img)){
            
            $check_serial_exist = PropertyImage::where('id_property',$prop_img->id_property)->where('ordernum',$val)->first();
            if(empty($check_serial_exist)){
                $prop_img->ordernum = $val;
                $prop_img->update();
                $message = 'Property Image Serial Set!'; 
            } else {
                $message = 'Property Serial Already Exist!';
            }
        }

        return $message;
    }

    public function updateImgAlt(Request $request){
        $id = $request['img_id'];
        $val = $request['alt_val'];
        
        $message = 'Property Image Data Not found!';
        
        $prop_img = PropertyImage::find($id);
        if(!empty($prop_img)){
            $prop_img->img_alt = $val;
            $prop_img->update();
            $message = 'Property Image Caption Set!'; 
        }

        return $message;
    }

    public function getAllTags($id_tag_type){
        $tags= Tag::where('id_tag_type',$id_tag_type)->get();        
        return json_encode($tags);
    }

    public function getAllSubTags($id_tag){
        $subtags= SubTag::where('id_tag',$id_tag)->get();        
        return json_encode($subtags);
    }
}
