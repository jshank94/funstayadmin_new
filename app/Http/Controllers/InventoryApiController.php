<?php

namespace App\Http\Controllers;

use App\Property;
use App\PropertyListing;
use App\InventoryData;
use App\InventorySource;
use App\User;

use Illuminate\Http\Request;

class InventoryApiController extends Controller
{
    public function getAllInventorySource()
    {
        $inventory_source = InventorySource::all();
        $response = [
          'inventory_sources' => $inventory_source
        ];
        return response()->json($response, 200);
    }
    
    public function postInventoryData(Request $request)
    {
        $this->validate($request, [
            'id_user' => 'required',
            'id_property' => 'required',
            'id_listing' => 'required',
            'id_source' => 'required',
            'customer_name' => 'required|max:250',
            'mobile_no' => 'required',
            'no_of_rooms' => 'required',
            'notes' => 'max:500',
            'booking_date' => 'required',
            'booking_time_to' => 'required',
            'booking_time_from' => 'required',
            'status' => 'required'
        ]);
        
        $allInventoryData = [];

        foreach($request->input('booking_date') as $key=>$val){ 
            $getPreviousEntry = InventoryData::where(['booking_date'=>$val,'id_listing'=>$request->input('id_listing')])->get();
            if(empty($getPreviousEntry[0])){
                $data = new InventoryData();
            } else {
                $data = InventoryData::find($getPreviousEntry[0]['id']);
            } 
            $data->id_user = $request->input('id_user');
            $data->id_property = $request->input('id_property');
            $data->id_listing = $request->input('id_listing');
            $data->id_source = $request->input('id_source');
            $data->customer_name = $request->input('customer_name');
            $data->mobile_no = $request->input('mobile_no');
            $data->no_of_adult = $request->input('no_of_adult');
            $data->no_of_infant = $request->input('no_of_infant');
            $data->no_of_child = $request->input('no_of_child');
            $data->no_of_rooms = $request->input('no_of_rooms');
            $data->notes = trim($request->input('notes'));
            $data->booking_date = $val;
            $data->booking_time_from = $request->input('booking_time_from');
            $data->booking_time_to = $request->input('booking_time_to');
            $data->status = $request->input('status');
            
            if(empty($getPreviousEntry[0])){
                $data->created_at = date('Y-m-d h:m:i');
                $allInventoryData[] = $data->attributesToArray();
            } else {
                $data->updated_at = date('Y-m-d h:m:i');
                $data->save();
            } 
        }

        InventoryData::insert($allInventoryData);

        return response()->json(['data' => 'Data Inserted'], 201);
    }

    public function getAllInventoryData()
    {
        $inventory = InventoryData::all();
        $response = [
          'inventory' => $inventory
        ];
        return response()->json($response, 200);
    }
    
    public function getInventoryDataUserBasis($id)
    {
        $inventory = InventoryData::where('id_user',$id)->get();
        $response = [
          'inventory' => $inventory
        ];
        return response()->json($response, 200);
    }
    
    public function getInventoryByListingAndProperty(Request $request)
    {
        $inventory = InventoryData::where(['id_property'=>$request->input('id_property'), 'id_listing'=>$request->input('id_listing')])->get();
        $response = [
          'inventory' => $inventory
        ];
        return response()->json($response, 200);
    }
    
    public function getInventoryDataPropertyBasis($id)
    {
        $inventory = InventoryData::where('id_property',$id)->get();
        $response = [
          'inventory' => $inventory
        ];
        return response()->json($response, 200);
    }

    public function getInventoryDataPropertyListingBasis($id)
    {
        $inventory = InventoryData::where('id_listing',$id)->get();
        $response = [
          'inventory' => $inventory
        ];
        return response()->json($response, 200);
    }

    public function putInventoryData(Request $request, $id)
    {
        $data = InventoryData::find($id);
        if (!$data) {
            return response()->json(['message' => 'Not found'], 404);
        }
        $data->id_source = $request->input('id_source');
        $data->customer_name = $request->input('customer_name');
        $data->mobile_no = $request->input('mobile_no');
        $data->no_of_adult = $request->input('no_of_adult');
        $data->no_of_infant = $request->input('no_of_infant');
        $data->no_of_child = $request->input('no_of_child');
        $data->booking_date = $request->input('booking_date_from');
        $data->booking_time_to = $request->input('booking_time_to');
        $data->booking_time_from = $request->input('booking_time_from');
        $data->status = $request->input('status');
        $data->save();

        return response()->json(['inventory' => $data], 200);
    }

    public function deleteInventoryData($id)
    {
        $data = InventoryData::find($id);
        $data->delete();
        return response()->json(['message' => 'Inventory deleted'], 200);
    }
}
