<?php

namespace App\Http\Controllers;

use App\Property;
use App\PropertyListing;
use App\InventoryData;
use App\User;

use Illuminate\Http\Request;

class PropertyApiController extends Controller
{
    public function getUserProperties($id)
    {
        $property = Property::where('id_user', $id)->get();

        if (!$property) {
            return response()->json(['message' => 'No Data Found'], 404);
        }

        $response = [
          'properties' => $property
        ];
        return response()->json($response, 200);
    }

    public function getPropertyListings($id)
    {
        $listings = PropertyListing::where('id_property', $id)->get();

        if (!$listings) {
            return response()->json(['message' => 'No Data Found'], 404);
        }

        $response = [
          'listings' => $listings
        ];
        return response()->json($response, 200);
    }
}
