<?php

namespace App\Http\Controllers;


use App\Test;
use App\Language;
use App\Property;
use App\PropertyListing;
use App\PropertyListingMealMap;
use App\PropertyVendorLanguage;
use App\PropertyImage;
use App\PropertyMeal;
use App\PropertyMealMap;
use App\PropertyRoomImage;
use App\PropertyStayType;
use App\TagType;
use App\PropertyRoomMapping;
use App\SubTag;
use App\PropertyTag;
use App\Tag;
use App\Location;
use App\PropertyHasSubTag;
use App\PropertyNearbyLocation;
use App\PropertyRoomAmenityMapping;
use App\PropertyListAmenityMap;
use App\User;
use App\Role;
use App\UserType;
use App\RoomType;
use App\Amenity;
use App\PropertyType;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CsvController extends Controller
{
    public function addCSV(){
        return view('vendor/voyager/csv/addCSV');
    }

    public function readCSV(Request $request){
        $message = "Invalid File";
        // $fileName=basename($request['file']->getClientOriginalName());
        $fileName =$request['file_name'];
        switch($fileName){
            case 'tag_types':
                if(isset($request['submit']))
                {
                    $this->validate($request, [
                        'file' => 'required',
                        'file.*' => 'mimes:csv|max:8048'
                    ]);
                if($request->hasfile('file'))
                {   $row = 0;
                    $file = fopen($request['file'], "r");
                    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
                    {   $row++;
                        if($row == 1) continue;
                        $data['id']=$getData[0];
                        $data['tag_type_name'] = $getData[1];
                        $test = new TagType();
                        if(TagType::where('id',$data['id'])->first()){
                            TagType::where('id',$data['id'])->update(["tag_type_name"=> $data['tag_type_name']]);
                        }else{
                            $test->id=  $data['id'];
                            $test->tag_type_name =trim($data['tag_type_name']);
                            $test->save();
                        }
                        if(!TagType::where('tag_type_name',  $data['tag_type_name'])->first())
                        {
                    
                        }
                    }
                    fclose($file);
                    $message = "Updated CSV File";
                }}
                break;
            case 'sub_tags':
                if(isset($request['submit']))
                {
                    $this->validate($request, [
                        'file' => 'required',
                        'file.*' => 'mimes:csv|max:8048'
                    ]);
                if($request->hasfile('file'))
                {   $row = 0;
                    $file = fopen($request['file'], "r");
                    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
                    {   $row++;
                        if($row == 1) continue;
                        $data['id'] = $getData[0];
                        $data['id_tag'] = $getData[1];
                        $data['name'] = $getData[2];
                        $data['icon'] = $getData[3];
                        $data['description'] = $getData[4];
                        $test = new SubTag();
                        if(Tag::where('id',$data['id_tag'])->first()){
                            if(SubTag::where('id', $data['id'])->first()){
                                SubTag::where('id', $data['id'])->update(["id_tag"=> $data['id_tag'],"name"=>  $data['name'],"icon"=> $data['icon'],"description"=>$data['description']]);
                            }
                            else
                            {
                            $test->id=$data['id'];
                            $test->id_tag =trim($data['id_tag']);
                            $test->name =trim($data['name']);
                            $test->icon = trim($data['icon']);
                            $test->description = trim($data['description']);
                            $test->save();
                            }
                        }
                    }
                    fclose($file);
                    $message = "Updated CSV File";
                }}
                break;
            case 'tags':
                if(isset($request['submit']))
                {
                    $this->validate($request, [
                        'file' => 'required',
                        'file.*' => 'mimes:csv|max:8048'
                    ]);
                if($request->hasfile('file'))
                {   $row = 0;
                    $file = fopen($request['file'], "r");
                    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
                    {   $row++;
                        if($row == 1) continue;
                        $data['id']=$getData[0];
                        $data['id_tag_type'] = $getData[1];
                        $data['name'] = $getData[2];
                        $data['icon'] = $getData[3];
                        $data['description'] = $getData[4];
                        $test = new Tag();

                        if(TagType::where('id',$data['id_tag_type'])->first())
                        {
                            if(Tag::where('id', $data['id'])->first())
                            {
                                Tag::where('id', $data['id'])->update(["id_tag_type"=> $data['id_tag_type'],"name"=>  $data['name'],"icon"=> $data['icon'],"description"=>$data['description']]);
                            }
                            else{
                                $test->id =  $data['id'];
                                $test->id_tag_type =trim($data['id_tag_type']);
                                $test->name =trim($data['name']);
                                $test->icon = trim($data['icon']);
                                $test->description = trim($data['description']);
                                $test->save();
                            }
                        }
                    }
                    fclose($file);
                    $message = "Updated CSV File";
                }}
                break;
            case 'properties': 
                if(isset($request['submit'])){
                    $this->validate($request, [
                        'file' => 'required',
                        'file.*' => 'mimes:csv|max:8048'
                    ]);
                        
                if($request->hasfile('file'))
                {   $row = 0;
                    $file = fopen($request['file'], "r");
                    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
                    {   $row++;
                        if($row == 1) continue;
                        $data['id'] = $getData[0];
                        $data['id_user'] = $getData[1];
                        $data['title'] = $getData[2];
                        $data['house_rules']=$getData[3];
                        // $data['additional_notes']=null;
                        // $data['is_all_info']=$getData[6];
                        
                        $user = User::where('id', $data['id_user'])->first();
                        if($user)
                        {  
                            if(Property::where('id', $data['id']))
                            {
                                Property::where('id', $data['id'])->update(["id_user"=> $data['id_user'],"title"=>$data['title'],"house_rules"=>$data['house_rules']]);
                            }
                            else{
                                $properties = new Property();
                                $properties->id =  $data['id'];
                                $properties->id_user =  $data['users_id'];
                                $properties->title= $data['title'];
                                $properties->house_rules = $data['house_rules'];
                                // $properties->additional_notes = $data['additional_notes'];
                                // $properties->is_all_info =  $data['is_all_info'];
                                $properties->save();
    
                                }
                            }
            
                    
                    }//while
                    fclose($file);
                    $message = "Updated CSV File";
                }}
        break;
        
        case 'property_listings':
                if(isset($request['submit'])){
                    $this->validate($request, [
                        'file' => 'required',
                        'file.*' => 'mimes:csv|max:8048'
                    ]);
                        
                if($request->hasfile('file'))
                {   $row = 0;
                    $file = fopen($request['file'], "r");
                    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
                    {   $row++;
                        if($row == 1) continue;
                        $data['id'] = $getData[0];
                        $data['id_property'] = $getData[1];
                        $data['room_title'] = $getData[2];
                        $data['room_price']=$getData[3];
                        $data['room_weekend_price']=$getData[4];
                        $data['property_commission']=$getData[5];
                        $data['property_commission_type']=$getData[6];
                        $data['property_commission_status']=$getData[7];
                        $data['property_type']=$getData[8];
                        $data['number_guests']=$getData[9];
                        $data['no_rooms']=$getData[10];
                        $data['allowed_extra_bed']=$getData[11];
                        $data['extra_bed_price']=$getData[12];
                        $data['room_type']=$getData[13];
                        $data['minimum_stay']=$getData[14];
                        $data['maximum_stay']=$getData[15];
                        $data['booking_type']=$getData[16];
                        $data['cancellation_policy']=$getData[17];
                        $data['description']=$getData[18];
                        $data['image']=$getData[19];
                        $data['vendor_status']=$getData[20];
                        $data['created_at']=$getData[21];
                        $data['updated_at']=$getData[22];
                        $data['rating_score']=$getData[23];
                        $data['checkin_time']=$getData[24];
                        $data['checkout_time']=$getData[25];
                        $data['guest_price']=$getData[26];
                        $data['accommodates_no']=$getData[27];
                        $data['food_included']=$getData[28];
                        $data['food_price']=$getData[29];
                        $data['price_per_person']=$getData[30];
                        $data['price_per_unit']=$getData[31];
                        $data['currency']=$getData[32];
                        $data['extra_child_price']=$getData[33];
                        $data['is_active']=$getData[34];
                        $data['deleted_at']=$getData[35];
                        if(Property::where('id', $data['id_property'])->first())
                        {  
                            if(PropertyListing::where('id', $data['id'])->first())
                            {
                                PropertyListing::where('id', $data['id'])->update([ 
                                "id_property" => $getData[1],
                                "room_title"=> $getData[2],
                                "room_price"=>$getData[3],
                                "room_weekend_price"=>$getData[4],
                                "property_commission"=>$getData[5],
                                "property_commission_type"=>$getData[6],
                                "property_commission_status"=>$getData[7],
                                "property_type"=>$getData[8],
                                "number_guests"=>$getData[9],
                                "no_rooms"=>$getData[10],
                                "allowed_extra_bed"=>$getData[11],
                                "extra_bed_price"=>$getData[12],
                                "room_type"=>$getData[13],
                                "minimum_stay"=>$getData[14],
                                "maximum_stay"=>$getData[15],
                                "booking_type"=>$getData[16],
                                "cancellation_policy"=>$getData[17],
                                "description"=>$getData[18],
                                "image"=>$getData[19],
                                "vendor_status"=>$getData[20],
                                "created_at"=>$getData[21],
                                "updated_at"=>$getData[22],
                                "rating_score"=>$getData[23],
                                "checkin_time"=>$getData[24],
                                "checkout_time"=>$getData[25],
                                "guest_price"=>$getData[26],
                                "accommodates_no"=>$getData[27],
                                "food_included"=>$getData[28],
                                "food_price"=>$getData[29],
                                "price_per_person"=>$getData[30],
                                "price_per_unit"=>$getData[31],
                                "currency"=>$getData[32],
                                "extra_child_price"=>$getData[33],
                                "is_active"=>$getData[34],
                                "deleted_at"=>$getData[35]]);
                            }
                            else{
                                $properties = new PropertyListing();
                                $properties->id = $getData[0];
                                $properties->id_property = $getData[1];
                                $properties->room_title = $getData[2];
                                $properties->room_price=$getData[3];
                                $properties->room_weekend_price=$getData[4];
                                $properties->property_commission=$getData[5];
                                $properties->property_commission_type=$getData[6];
                                $properties->property_commission_status=$getData[7];
                                $properties->property_type=$getData[8];
                                $properties->number_guests=$getData[9];
                                $properties->no_rooms=$getData[10];
                                $properties->allowed_extra_bed=$getData[11];
                                $properties->extra_bed_price=$getData[12];
                                $properties->room_type=$getData[13];
                                $properties->minimum_stay=$getData[14];
                                $properties->maximum_stay=$getData[15];
                                $properties->booking_type=$getData[16];
                                $properties->cancellation_policy=$getData[17];
                                $properties->description=$getData[18];
                                $properties->image=$getData[19];
                                $properties->vendor_status=$getData[20];
                                $properties->created_at=$getData[21];
                                $properties->updated_at=$getData[22];
                                $properties->rating_score=$getData[23];
                                $properties->checkin_time=$getData[24];
                                $properties->checkout_time=$getData[25];
                                $properties->guest_price=$getData[26];
                                $properties->accommodates_no=$getData[27];
                                $properties->food_included=$getData[28];
                                $properties->food_price=$getData[29];
                                $properties->price_per_person=$getData[30];
                                $properties->price_per_unit=$getData[31];
                                $properties->currency=$getData[32];
                                $properties->extra_child_price=$getData[33];
                                $properties->is_active=$getData[34];
                                $properties->deleted_at=$getData[35];
                                $properties->save();
                                }
                            }
            
                    
                    }//while
                    fclose($file);
                    $message = "Updated CSV File";
                }}
        break;
        case 'users': 
            if(isset($request['submit'])){
                $this->validate($request, [
                    'file' => 'required',
                    'file.*' => 'mimes:csv|max:8048'
                ]);
                
            if($request->hasfile('file'))
            {   $row = 0;
                $file = fopen($request['file'], "r");
                while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
                {   $row++;
                    if($row == 1) continue;
                    $data['id']=$getData[0];
                    $data['role_id'] =$getData[1];
                    $data['email']=$getData[2];
                    $data['name']=$getData[3];
                    $data['avatar'] = $getData[4];
                    $data['mobile_no'] = $getData[5];
                    $data['password']=$getData[6];
                    $users = new User();
                    $User = User::where('id',$data['id'])->first();
                    if(!$User){
                        $users->id=$getData[0];
                        $users->role_id =$getData[1];
                        $users->email=$getData[2];
                        $users->name=$getData[3];
                        $users->avatar = $getData[4];
                        $users->mobile_no = $getData[5];
                        $users->password=$getData[6];
                    }
                    else{
                        $user = User::find($data['id']);
                        $user->update([
                            'role_id' => 1,'email'=> $data['email'],'name'=> $data['name'],'avatar'=> $data['avatar'],'mobile_no'=>$data['mobile_no'],'password'=>$data['password']
                        ]);
                    }      
                }
                fclose($file);
                $message = "Updated CSV File";
            }}
        break;
            case 'languages': 
                if(isset($request['submit'])){
                    $this->validate($request, [
                        'file' => 'required',
                        'file.*' => 'mimes:csv|max:8048'
                    ]);
                if($request->hasfile('file'))
                {   $row = 0;
                    $file = fopen($request['file'], "r");
                    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
                    {   $row++;
                        if($row == 1) continue;
                        $data['id'] = $getData[0];
                        $data['language_name'] = $getData[1];
                        $data['language_code'] = $getData[2];
                        $test = new Language();
                        if( Language::where('id', $data['id'])->first()){
                            $language = Language::where('language_name',  $data['language_name'])->first();
                            if(!$language){
                                Language::where('id',$data['id'])->update(["language_name"=> $data['language_name'],"language_code"=>$data['language_code']]);
                            }
                        }else{
                            $test->id = trim($data['id']); 
                            $test->language_name = trim($data['language_name']);
                            $test->language_code = trim($data['language_code']);
                            $test->save();
                        }
                    }
                    fclose($file);
                    $message = "Updated CSV File";
                }}
            break;
            case 'property_vendor_languages': 
            if(isset($request['submit']))
            {
                $this->validate($request, [
                    'file' => 'required',
                    'file.*' => 'mimes:csv|max:8048'
                ]);
            if($request->hasfile('file'))
            {   $row = 0;
                $file = fopen($request['file'], "r");
                while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
                {   $row++;
                    if($row == 1) continue;
                    $data['id']=$getData[0];
                    $data['property_id'] = $getData[1];
                    $data['language_id'] = $getData[2];
                    if(PropertyVendorLanguage::where('id',$data['id'])->first())
                    {
                        if(!Property::where('id', $data['property_id'])->first() && !Language::where('id',$data['language_id'])->first())
                        {
                            PropertyVendorLanguage::where('id', $data['id'])->update(['id_property'=> $data['property_id'],'id_language'=> $data['language_id']]);
                        }
                    }else{
                        $test = new PropertyVendorLanguage();
                        $test->id_property = $data['property_id'];
                        $test->id_language =  $data['language_id'];
                        $test->save();
                    }
                }
                fclose($file);
                $message = "Updated CSV File";
            }}
        break;
        case 'amenities': 
        if(isset($request['submit']))
        {
            $this->validate($request, [
                'file' => 'required',
                'file.*' => 'mimes:csv|max:8048'
            ]);
        if($request->hasfile('file'))
        {   $row = 0;
            $file = fopen($request['file'], "r");
            while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
            {   $row++;
                if($row == 1) continue;
                $data['id'] = $getData[0];
                $data['name'] = $getData[1];
                $data['icon'] = $getData[2];
                $data['description']=$getData[3];
                $data['id_amenity_category']=$getData[4];
                $test = new Amenity();
                 if(Amenity::where('id', $data['id'])->first())
                 {
                    Amenity::where('id', $data['id'])->update(["name"=> $data['name'],"icon"=> $data['icon'],"description"=>$data['description'],"id_amenity_category"=>$data['id_amenity_category']]);
                 }
                 else{
                    $test->id =trim( $data['id']);
                    $test->name =trim( $data['name']);
                    $test->icon = trim( $data['icon']);
                    $test->description = trim($data['description']);
                    $test->id_amenity_category = trim($data['id_amenity_category']);
                    $test->save();
                 }
            }
            fclose($file);
            $message = "Updated CSV File";
        }}
    break;
    case 'property_list_amenity_maps': 
    if(isset($request['submit']))
    {
        $this->validate($request, [
            'file' => 'required',
            'file.*' => 'mimes:csv|max:8048'
        ]);
    if($request->hasfile('file'))
    {   $row = 0;
        $file = fopen($request['file'], "r");
        // print_r($file); die();
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
        {   $row++;
            if($row == 1) continue;
            $data['id']=$getData[0];
            $data['id_property_list'] = $getData[1];
            $data['id_amenity'] = $getData[2];
            if(PropertyListing::where('id', $data['id_property_list'])->first() && Amenity::where('id', $data['id_amenity'])->first())
            {   if(PropertyListAmenityMap::where('id',$data['id'])->first())
                {
                    PropertyListAmenityMap::where('id',$data['id'])->update(["id_property_list" => $data['id_property_list'],"id_amenity" => $data['id_amenity']]);
                }else{
                    $roomAmenitiesMapping = new PropertyListAmenityMap();
                    $roomAmenitiesMapping->id= $data['id'];
                    $roomAmenitiesMapping->id_property_list = $data['id_property_list'];
                    $roomAmenitiesMapping->id_amenity = $data['id_amenity'];
                    $roomAmenitiesMapping->save();
                }
        }
        fclose($file);
        $message = "Updated CSV File";
    }
}}
break;
case 'property_list_images': 
if(isset($request['submit']))
{
    $this->validate($request, [
        'file' => 'required',
        'file.*' => 'mimes:csv|max:8048'
    ]);
if($request->hasfile('file'))
{   $row = 0;
    $file = fopen($request['file'], "r");
    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
    {   $row++;
        if($row == 1) continue;
        $data['id'] = $getData[0];
        $data['id_property_room'] = $getData[1];
        $data['image'] = $getData[2];
        $data['ordernum']=$getData[3];
        $data['img_alt']=$getData[4];
        $data['is_cover']=$getData[5];

        if(PropertyRoomImage::where('id',$data['id'])->first()){
            $property = PropertyRoomMapping::where('id', $data['id_property'])->first();
            if( $property)
            {
                PropertyRoomImage::where('id',$data['id'])->update(["id_property_room"=> $data['id_property_room'],"image"=> $data['image'],"img_alt"=>$data['img_alt'],"ordernum"=>$data['ordernum']]);
            }
        }
        else{
            $property = PropertyRoomMapping::where('id', $data['id_property'])->first();
            if( $property)
            {
                $test = new PropertyRoomImage();
                $test->id_property_room =trim($data['id_property']);
                $test->image = trim($data['image']);
                $test->img_alt = trim($data['img_alt']);
                $test->ordernum = trim($data['ordernum']);
                $test->is_cover = $data['is_cover'];
                $test->save();
            }
        }
       
    }
    fclose($file);
    $message = "Updated CSV File";
}}
break;
        case 'property_images': 
        if(isset($request['submit']))
        {
            $this->validate($request, [
                'file' => 'required',
                'file.*' => 'mimes:csv|max:8048'
            ]);
        if($request->hasfile('file'))
        {   $row = 0;
            $file = fopen($request['file'], "r");
            while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
            {   $row++;
                if($row == 1) continue;
                $data['id']=$getData[0];
                $data['id_property'] = $getData[1];
                $data['image'] = $getData[2];
                $data['ordernum']=$getData[3];
                $data['img_alt']=$getData[4];
                $data['is_cover']=$getData[5];
                $property = Property::where('id', $data['id_property'])->first();
                if(PropertyImage::where('id', $data['id'])->first())
                {
                    if($property)
                    {
                        PropertyImage::where('id',$data['id'])->update(["id_property"=>$data['id_property'],"image"=> $data['image'],"ordernum"=> $data['ordernum'],"img_alt"=>$data['img_alt'],"is_cover"=>$data['is_cover']]);
                    }
                }else
                {
               
                if( $property)
                {
                    $test = new PropertyImage();
                    $test->id =trim( $data['id']);
                    $test->id_property =trim($data['id_property']);
                    $test->image = trim($data['image']);
                    $test->img_alt = trim($data['img_alt']);
                    $test->ordernum = trim($data['ordernum']);
                    $test->is_cover = trim($data['is_cover']);
                    $test->save();
                }
            }
            }
            fclose($file);
            $message = "Updated CSV File";
        }}
    break;
    case 'property_types': 
    if(isset($request['submit']))
    {
        $this->validate($request, [
            'file' => 'required',
            'file.*' => 'mimes:csv|max:8048'
        ]);
    if($request->hasfile('file'))
    {   $row = 0;
        $file = fopen($request['file'], "r");
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
        {   $row++;
            if($row == 1) continue;
            $data['id']= $getData[0];
            $data['property_type'] = $getData[1];
            $data['type_description']=$getData[2];
            $data['icon']=$getData[3];
            if(PropertyType::where('id', $data['id'])->first())
            {
            PropertyType::where('id', $data['id'])->update(["property_type"=>$getData[1],"type_description"=>$getData[2],"icon"=>$data['icon']]);
            }else{
                $property_type = new PropertyType();
                $property_type->id= $data['id'];
                $property_type->property_type= $data['property_type'];
                $property_type->type_description= $data['type_description'];
                $property_type->icon= $data['icon'];
                $property_type->save();
            }
        }
        fclose($file);
        $message = "Updated CSV File";
    }}
break;
    case 'property_meals': 
    if(isset($request['submit']))
    {
        $this->validate($request, [
            'file' => 'required',
            'file.*' => 'mimes:csv|max:8048'
        ]);
    if($request->hasfile('file'))
    {   $row = 0;
        $file = fopen($request['file'], "r");
        while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
        {   $row++;
            if($row == 1) continue;
            $data['id']=$getData[0];
            $data['meal_name'] = $getData[1];
            $data['description'] = $getData[2];
            if(!PropertyMeal::where('id',$data['id'])->first()){
                $property_meal = new PropertyMeal();
                $property_meal->id = trim($data['id']);
                $property_meal->meal_name =trim($data['meal_name']);
                $property_meal->description = trim($data['description']);
                $property_meal->save();
            }else{
                $property_meal = PropertyMeal::where('id',$data['id'])->update(array("meal_name"=> $data['meal_name'],"description"=>$data['description']));
            }
            
        }
        fclose($file);
        $message = "Updated CSV File";
    }}
break;
case 'property_rooms_meals_map': 
if(isset($request['submit']))
{
    $this->validate($request, [
        'file' => 'required',
        'file.*' => 'mimes:csv|max:8048'
    ]);
if($request->hasfile('file'))
{   $row = 0;
    $file = fopen($request['file'], "r");
    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
    {   $row++;
        if($row == 1) continue;
        $data['id'] = $getData[0];
        $data['id_property_listing'] = $getData[1];
        $data['id_meal'] = $getData[2];
        $data['price'] = $getData[3];
        $test = new PropertyListingMealMap();
        if(PropertyMeal::where('id', $data['id_meal'])->first() && PropertyListing::where('id',$data['id_property_listing'] )->first())
            {  
                if(!PropertyListingMealMap::where('id',$data['id'])->first()){
                    $test->id=$data['id'];
                    $test->id_property_listing =trim($data['id_property_listing']);
                    $test->id_meal = trim($data['id_meal']);
                    $test->price=$data['price'];
                    $test->save();
                }else{
                    $property_meal_map = PropertyListingMealMap::where('id',$data['id'])->update(array("id_property_listing"=> $data['id_property_listing'],"id_meal"=>$data['id_meal'],"price"=>$data['price']));
                }
        }
    }
    fclose($file);
    $message = "Updated CSV File";
}}
break;
case 'locations':
if(isset($request['submit']))
{
    $this->validate($request, [
        'file' => 'required',
        'file.*' => 'mimes:csv|max:8048'
    ]);
if($request->hasfile('file'))
{   $row = 0;
    $file = fopen($request['file'], "r");
    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
    {   $row++;
        if($row == 1) continue;
        $data['id']=$getData[0];
        $data['title'] = $getData[1];
        $data['description'] = $getData[2];
        $data['type'] = $getData[5];
        if(Location::where('id',$data['id'])->first()){
            Location::where('id',$data['id'])->update(array("title"=> $data['title'],"description"=>$data['description']));
        }else{
        $test = new Location();
        $test->title =trim($data['title']);
        $test->description =trim($data['description']);
        // $test->longitude =trim($data['longitude']);
        // $test->latitude =trim($data['latitude']);
        // $test->type =trim($data['type']);
        $test->save();
        }
    }
    fclose($file);
    $message = "Updated CSV File";
}}
break;
case 'property_has_subtags':
if(isset($request['submit']))
{
    $this->validate($request, [
        'file' => 'required',
        'file.*' => 'mimes:csv|max:8048'
    ]);
if($request->hasfile('file'))
{   $row = 0;
    $file = fopen($request['file'], "r");
    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
    {   $row++;
        if($row == 1) continue;
        $data['id_property'] = $getData[0];
        $data['id_tag'] = $getData[1];
        $data['id_subtag'] = $getData[2];
        $test = new PropertyHasSubTag();
        $test->id_property =trim($data['id_property']);
        $test->id_tag =trim($data['id_tag']);
        $test->id_subtag =trim($data['id_subtag']);
        $test->save();
    }
    
// print_r($data); die();

    fclose($file);
    $message = "Updated CSV File";
}}
break;
 case 'property_location_nearby':
 if(isset($request['submit']))
 {
     $this->validate($request, [
         'file' => 'required',
         'file.*' => 'mimes:csv|max:8048'
     ]);
 if($request->hasfile('file'))
 {   $row = 0;
     $file = fopen($request['file'], "r");
     while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
     {   $row++;
         if($row == 1) continue;
         $data['id_location'] = $getData[0];
         $data['id_property'] = $getData[1];
         $data['distance'] = $getData[3];
         $test = new PropertyNearbyLocation();
         $test->id_location =trim($data['id_location']);
         $test->id_property =trim($data['id_property']);
         $test->distance =trim($data['distance']);
         $test->save();
     }
    
//   print_r($data); die();

     fclose($file);
     $message = "Updated CSV File";
 }}
 break;
case 'property_amenities':
if(isset($request['submit']))
{
    $this->validate($request, [
        'file' => 'required',
        'file.*' => 'mimes:csv|max:8048'
    ]);
if($request->hasfile('file'))
{   $row = 0;
    $file = fopen($request['file'], "r");
    while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)
    {   $row++;
        if($row == 1) continue;
        $data['name'] = $getData[0];
        $data['icon'] = $getData[1];
        $data['description'] = $getData[2];
        $data['category'] = $getData[3];
        $test = new PropertyHasSubTag();
        $test->id_property =trim($data['id_property']);
        $test->id_tag =trim($data['id_tag']);
        $test->id_subtag =trim($data['id_subtag']);
        $test->save();
    }
    
// print_r($data); die();

    fclose($file);
    $message = "Updated CSV File";
}}
break; 
        }

        return view('vendor/voyager/csv/addCSV')->with(['message' => $message]);
    }
   
}
