<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('login');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    //Import
    Route::get('/import',[
        'uses'=>'PropertyController@import',
        'as'=>'import',
        'middleware'=>'auth'
    ]);

    //Property Management
    Route::post('/addProperty',[
        'uses'=>'PropertyController@addProperty',
        'as'=>'addProperty',
        'middleware'=>'auth'
    ]);

    Route::get('/addProperty',[
        'uses'=>'PropertyController@addProperty',
        'as'=>'addProperty',
        'middleware'=>'auth'
    ]);

    Route::get('/editProperty/{id_property}',[
        'uses'=>'PropertyController@editProperty',
        'as'=>'editProperty',
        'middleware'=>'auth'
    ]);

    Route::post('/editProperty/{id_property}',[
        'uses'=>'PropertyController@editProperty',
        'as'=>'editProperty',
        'middleware'=>'auth'
    ]);

    Route::post('/addAmenity',[
        'uses'=>'PropertyController@addAmenity',
        'as'=>'addAmenity',
        'middleware'=>'auth'
    ]);
    
    Route::get('/addAmenity',[
        'uses'=>'PropertyController@addAmenity',
        'as'=>'addAmenity',
        'middleware'=>'auth'
    ]);
    
    Route::post('/addLocationInfo',[
        'uses'=>'PropertyController@addLocationInfo',
        'as'=>'addLocationInfo',
        'middleware'=>'auth'
    ]);

    Route::get('/addLocationInfo',[
        'uses'=>'PropertyController@addLocationInfo',
        'as'=>'addLocationInfo',
        'middleware'=>'auth'
    ]);

    Route::post('/addImages',[
        'uses'=>'PropertyController@addImages',
        'as'=>'addImages',
        'middleware'=>'auth'
    ]);
    
    Route::get('/addImages',[
        'uses'=>'PropertyController@addImages',
        'as'=>'addImages',
        'middleware'=>'auth'
    ]);

    Route::post('/updateCover',[
        'uses'=>'PropertyController@updateCover',
        'as'=>'updateCover',
        'middleware'=>'auth'
    ]);

    Route::post('/updateSerial',[
        'uses'=>'PropertyController@updateSerial',
        'as'=>'updateSerial',
        'middleware'=>'auth'
    ]);

    Route::post('/updateImgAlt',[
        'uses'=>'PropertyController@updateImgAlt',
        'as'=>'updateImgAlt',
        'middleware'=>'auth'
    ]);
    
    Route::get('/addNearBy',[
        'uses'=>'PropertyController@addNearBy',
        'as'=>'addNearBy',
        'middleware'=>'auth'
    ]);

    Route::post('/addHouseRules',[
        'uses'=>'PropertyController@addHouseRules',
        'as'=>'addHouseRules',
        'middleware'=>'auth'
    ]);

    Route::get('/addHouseRules',[
        'uses'=>'PropertyController@addHouseRules',
        'as'=>'addHouseRules',
        'middleware'=>'auth'
    ]);

    Route::post('/addTags',[
        'uses'=>'PropertyController@addTags',
        'as'=>'addTags',
        'middleware'=>'auth'
    ]);
    
    Route::get('/addTags',[
        'uses'=>'PropertyController@addTags',
        'as'=>'addTags',
        'middleware'=>'auth'
    ]);

    Route::get('/getAllTags/{id_tag_type}',[
        'uses'=>'PropertyController@getAllTags',
        'as'=>'getAllTags',
        'middleware'=>'auth'
    ]);

    Route::get('/getAllSubTags/{id_tag}',[
        'uses'=>'PropertyController@getAllSubTags',
        'as'=>'getAllSubTags',
        'middleware'=>'auth'
    ]);

    Route::post('/addAdditionalInfo',[
        'uses'=>'PropertyController@addAdditionalInfo',
        'as'=>'addAdditionalInfo',
        'middleware'=>'auth'
    ]);

    Route::get('/addAdditionalInfo',[
        'uses'=>'PropertyController@addAdditionalInfo',
        'as'=>'addAdditionalInfo',
        'middleware'=>'auth'
    ]);

    Route::get('/addActivities',[
        'uses'=>'PropertyController@addActivities',
        'as'=>'addActivities',
        'middleware'=>'auth'
    ]);

    Route::get('/deletePropImage/{id_property_img}',[
        'uses'=>'PropertyController@deletePropImage',
        'as'=>'deletePropImage',
        'middleware'=>'auth'
    ]);
    
    Route::get('/deleteProperty/{id_property}',[
        'uses'=>'PropertyController@deleteProperty',
        'as'=>'deleteProperty',
        'middleware'=>'auth'
    ]);

    //Property Room Management

    Route::get('/getPropertyRooms/{id_property}',[
        'uses'=>'PropertyRoomController@getPropertyRooms',
        'as'=>'getPropertyRooms',
        'middleware'=>'auth'
    ]);
    
    Route::post('/addPropertyRoom',[
        'uses'=>'PropertyRoomController@addPropertyRoom',
        'as'=>'addPropertyRoom',
        'middleware'=>'auth'
    ]);

    Route::get('/addPropertyRoom',[
        'uses'=>'PropertyRoomController@addPropertyRoom',
        'as'=>'addPropertyRoom',
        'middleware'=>'auth'
    ]);

    Route::get('/editPropertyRoom/{id_property_room}',[
        'uses'=>'PropertyRoomController@editPropertyRoom',
        'as'=>'editPropertyRoom',
        'middleware'=>'auth'
    ]);

    Route::post('/editPropertyRoom/{id_property_room}',[
        'uses'=>'PropertyRoomController@editPropertyRoom',
        'as'=>'editPropertyRoom',
        'middleware'=>'auth'
    ]);

    Route::post('/addRoomPriceInfo',[
        'uses'=>'PropertyRoomController@addRoomPriceInfo',
        'as'=>'addRoomPriceInfo',
        'middleware'=>'auth'
    ]);

    Route::get('/addRoomPriceInfo',[
        'uses'=>'PropertyRoomController@addRoomPriceInfo',
        'as'=>'addRoomPriceInfo',
        'middleware'=>'auth'
    ]);

    Route::post('/addRoomAmenity',[
        'uses'=>'PropertyRoomController@addRoomAmenity',
        'as'=>'addRoomAmenity',
        'middleware'=>'auth'
    ]);
    
    Route::get('/addRoomAmenity',[
        'uses'=>'PropertyRoomController@addRoomAmenity',
        'as'=>'addRoomAmenity',
        'middleware'=>'auth'
    ]);
    
    Route::post('/addRoomMealPlan',[
        'uses'=>'PropertyRoomController@addRoomMealPlan',
        'as'=>'addRoomMealPlan',
        'middleware'=>'auth'
    ]);

    Route::get('/addRoomMealPlan',[
        'uses'=>'PropertyRoomController@addRoomMealPlan',
        'as'=>'addRoomMealPlan',
        'middleware'=>'auth'
    ]);

    Route::post('/addRoomImages',[
        'uses'=>'PropertyRoomController@addRoomImages',
        'as'=>'addRoomImages',
        'middleware'=>'auth'
    ]);
    
    Route::get('/addRoomImages',[
        'uses'=>'PropertyRoomController@addRoomImages',
        'as'=>'addRoomImages',
        'middleware'=>'auth'
    ]);

    Route::post('/updateRoomCover',[
        'uses'=>'PropertyRoomController@updateRoomCover',
        'as'=>'updateRoomCover',
        'middleware'=>'auth'
    ]);

    Route::post('/updateRoomSerial',[
        'uses'=>'PropertyRoomController@updateRoomSerial',
        'as'=>'updateRoomSerial',
        'middleware'=>'auth'
    ]);

    Route::post('/updateRoomImgAlt',[
        'uses'=>'PropertyRoomController@updateRoomImgAlt',
        'as'=>'updateRoomImgAlt',
        'middleware'=>'auth'
    ]);

    Route::post('/addRoomTags',[
        'uses'=>'PropertyRoomController@addRoomTags',
        'as'=>'addRoomTags',
        'middleware'=>'auth'
    ]);
    
    Route::get('/addRoomTags',[
        'uses'=>'PropertyRoomController@addRoomTags',
        'as'=>'addRoomTags',
        'middleware'=>'auth'
    ]);

    Route::post('/addRoomSEO',[
        'uses'=>'PropertyRoomController@addRoomSEO',
        'as'=>'addRoomSEO',
        'middleware'=>'auth'
    ]);
    
    Route::get('/addRoomSEO',[
        'uses'=>'PropertyRoomController@addRoomSEO',
        'as'=>'addRoomSEO',
        'middleware'=>'auth'
    ]);

    Route::post('/addCalendar',[
        'uses'=>'PropertyRoomController@addCalendar',
        'as'=>'addCalendar',
        'middleware'=>'auth'
    ]);
    
    Route::get('/addCalendar',[
        'uses'=>'PropertyRoomController@addCalendar',
        'as'=>'addCalendar',
        'middleware'=>'auth'
    ]);

    Route::get('/getAllRoomTags/{id_tag_type}',[
        'uses'=>'PropertyRoomController@getAllTags',
        'as'=>'getAllTags',
        'middleware'=>'auth'
    ]);

    Route::get('/getAllRoomSubTags/{id_tag}',[
        'uses'=>'PropertyRoomController@getAllSubTags',
        'as'=>'getAllSubTags',
        'middleware'=>'auth'
    ]);

    Route::get('/deletePropRoomImage/{id_property_room_img}',[
        'uses'=>'PropertyRoomController@deletePropRoomImage',
        'as'=>'deletePropRoomImage',
        'middleware'=>'auth'
    ]);
    
    Route::get('/deletePropertyRoom/{id_property_room}',[
        'uses'=>'PropertyController@deletePropertyRoom',
        'as'=>'deletePropertyRoom',
        'middleware'=>'auth'
    ]);

    Route::get('/addCSV',[
        'uses'=>'CsvController@addCSV',
        'as'=>'addCSV',
        'middleware'=>'auth'
    ]);

    Route::post('/addCSV',[
        'uses'=>'CsvController@readCSV',
        'as'=>'addCSV',
        'middleware'=>'auth'
    ]);

    Route::get('/downloadFile/{id}',function($id){
        $file= public_path(). "/storage/csv_file/".$id.'.csv';
       
        $headers = array(
            'Content-Type'=>'application/csv',
          );
        // return response()->download(storage_path($file));
        return response()->download($file,$id.'.csv', $headers);
    });
});
