<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


// Inventory API

    Route::post('/addInventoryData', [
        'uses' => 'InventoryApiController@postInventoryData'
    ]);

    Route::get('/getInventorySource', [
    'uses' => 'InventoryApiController@getAllInventorySource'
    ]);
    
    Route::get('/getInventoryByUser/{id}', [
    'uses' => 'InventoryApiController@getInventoryDataUserBasis'
    ]);
    
    Route::get('/getInventoryByProperty/{id}', [
    'uses' => 'InventoryApiController@getInventoryDataPropertyBasis'
    ]);

    Route::get('/getInventoryByListing/{id}', [
        'uses' => 'InventoryApiController@getInventoryDataPropertyListingBasis'
        ]);
    
    Route::post('/getInventoryByListingAndProperty', [
    'uses' => 'InventoryApiController@getInventoryByListingAndProperty'
    ]);    

    Route::put('/updateInventory/{id}', [
        'uses' => 'InventoryApiController@putInventoryData'
    ]);
    
    // Route::delete('/inventory/{id}', [
    //     'uses' => 'InventoryApiController@deleteInventoryData'
    // ]);

// User API

    Route::post('/loginUser', [
        'uses' => 'UserApiController@postLogin'
    ]);

    Route::post('/resendOtp', [
        'uses' => 'UserApiController@resendOtp'
    ]);

    Route::post('/otpVerify', [
        'uses' => 'UserApiController@otpVerify'
    ]);

// Property API

    Route::get('/getUserProperties/{id}', [
    'uses' => 'PropertyApiController@getUserProperties'
    ]);

    Route::get('/getPropertyListings/{id}', [
    'uses' => 'PropertyApiController@getPropertyListings'
    ]);
