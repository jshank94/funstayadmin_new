@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Property Room Images
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.property-room-mappings.property-misc-options.side-nav')
                        </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.property-room-mappings.property-misc-options.message-block')
                                <form action="" method="post" enctype="multipart/form-data">
                                    <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                        <label for="image">Select Image</label>
                                        <input type="file" class="form-control" id="image" name="image[]" multiple required>
                                    </div>   
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Upload</button>
                                </form>
                                @if(isset($propertyImages))
                                <div class="col-sm-3">
                                </div>
                                <div class="col-sm-6">
                                        <span style="width:100%; display:none;" class="alert alert-info" id="message_img_update"></span>
                                </div>
                                <div class="col-sm-3">
                                </div>
                                <br><br> 
                                    @foreach($propertyImages as $val)
                                        <div class="col-sm-4">
                                                <img src="{{ url('storage/app/public/property-room-images/'.$val->image) }}" style="height:150px; width:220px;" alt="{{ $val->img_alt }}">
                                                {{-- <p>{{ $val->img_alt }}</p> --}}
                                                <div class="col-sm-12">
                                                    <div class="form-group {{ $errors->has('img_alt') ? 'has-error' : '' }}">
                                                        <input type="text" class="form-control" id="img_alt" onblur="updateImgAlt(event, {{ $val->id }})" name="img_alt" value="{{ $val->img_alt }}" aria-describedby="img_alt" placeholder="Enter Image Alt">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="img_series">Serial</label>
                                                    <input type="number" class="form-control" onblur="updateSerial(event, {{ $val->id }})" id="img_series" name="img_series" value="{{ $val->ordernum }}" aria-describedby="img_series" min="0">
                                                </div>
                                                <div class="col-sm-6">
                                                    <label for="img_cover">Cover Photo</label>
                                                    {{-- <input type="checkbox" id="img_cover" name="img_cover" value="{{ Request::old('img_cover') }}" aria-describedby="img_cover"> Cover --}}
                                                    <select class="form-control is_cover" onchange="updateCover(event, {{ $val->id }})" name="img_cover">
                                                        <option value="N" @if($val->is_cover == 'N') selected @endif>No</option>
                                                        <option value="Y" @if($val->is_cover == 'Y') selected @endif>Yes</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-12">
                                                    <a href="deletePropRoomImage/{{ $val->id }}" style="width:100%" class="btn btn-lg btn-danger">Delete</a>
                                                </div>
                                            </div>
                                        @endforeach
                                    <a style="margin-top:34%" class="btn btn-sm btn-primary pull-right" href="{{ url('admin/addRoomAmenity') }}">Submit</a>
                                @endif
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var token = '{{ Session::token() }}';
        var updateCoverUrl = '{{ route('updateRoomCover') }}';
        var updateSerialUrl = '{{ route('updateRoomSerial') }}';
        var updateImgAltUrl = '{{ route('updateRoomImgAlt') }}';

        function updateCover(event, id){
            $.ajax({
                method: "POST",
                url: updateCoverUrl,
                data: {img_id: id, cover_val: event.target.value, _token:token}
            })
            .done(function(res){
                $("#message_img_update").show();
                $("#message_img_update").text(res);
            });
        }

        function updateSerial(event, id){
            $.ajax({
                method: "POST",
                url: updateSerialUrl,
                data: {img_id: id, serial_val: event.target.value, _token:token}
            })
            .done(function(res){
                $("#message_img_update").show();
                $("#message_img_update").text(res);
            });
        }

        function updateImgAlt(event, id){
            $.ajax({
                method: "POST",
                url: updateImgAltUrl,
                data: {img_id: id, alt_val: event.target.value, _token:token}
            })
            .done(function(res){
                $("#message_img_update").show();
                $("#message_img_update").text(res);
            });
        }
    </script>
@stop


