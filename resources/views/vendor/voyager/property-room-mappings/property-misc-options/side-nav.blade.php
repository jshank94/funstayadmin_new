<ul class="nav nav-pills nav-stacked" id="stacked-menu">
    <li>
        <ul class="nav nav-pills nav-stacked">
            <li><a class="btn btn-lg btn-primary" href="{{ url('admin/getPropertyRooms') }}/{{ Session::get('propertyCreate') }}">Go to Property Listing</a></li>
            <hr>
            <li class="{{ (Request::segment(2)=='addPropertyRoom' || Request::segment(2)=='editPropertyRoom')?'active':'' }}"><a @if(Session::get('propertyRoomCreate')) href="editPropertyRoom/{{ Session::get('propertyRoomCreate') }}" @else href="addPropertyRoom" @endif>Basic Info.</a></li>
            <li class="{{ (Request::segment(2)=='addRoomPriceInfo')?'active':'' }}"><a href="{{ url('admin/addRoomPriceInfo') }}">Price Info.</a></li>
            <li class="{{ (Request::segment(2)=='addRoomMealPlan')?'active':'' }}"><a href="{{ url('admin/addRoomMealPlan') }}">Meals Info</a></li>
            <li class="{{ (Request::segment(2)=='addRoomImages')?'active':'' }}"><a href="{{ url('admin/addRoomImages') }}">Images</a></li>
            <li class="{{ (Request::segment(2)=='addRoomAmenity')?'active':'' }}"><a href="{{ url('admin/addRoomAmenity') }}">Amenities</a></li>            
            <li class="{{ (Request::segment(2)=='addRoomTags')?'active':'' }}"><a href="{{ url('admin/addRoomTags') }}">Tags</a></li>
            <li class="{{ (Request::segment(2)=='addCalendar')?'active':'' }}"><a href="{{ url('admin/addCalendar') }}">Calendar IMS</a></li>
            <li class="{{ (Request::segment(2)=='addRoomSEO')?'active':'' }}"><a href="{{ url('admin/addRoomSEO') }}">SEO</a></li>
        </ul>
    </li>
</ul>