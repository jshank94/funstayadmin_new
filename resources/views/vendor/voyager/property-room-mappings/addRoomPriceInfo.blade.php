@extends('voyager::master')

@section('css')
    <style>
        .checkbox{
            margin: 2%;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Property Room Price Info.
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.property-room-mappings.property-misc-options.side-nav')
                        </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.property-room-mappings.property-misc-options.message-block')
                            <form action="" method="post">
                                <div class="form-group {{ $errors->has('currency') ? 'has-error' : '' }}">
                                    <label for="currency">Currency</label>
                                    {{-- <input type="text" class="form-control" id="currency" style="text-transform:uppercase" name="currency" @if(isset($property)) value="{{ $property->currency }}" @else value="{{ Request::old('currency') }}" @endif aria-describedby="title" placeholder="Enter Currency (USD, INR)"> --}}
                                    <select class="form-control" id="currency"  name="currency">
                                        <option value="INR">INR</option>
                                        <option value="USD">USD</option>
                                    <select>
                                </div>
                                @if(isset($propertyRoomInfo[0]) && ($propertyRoomInfo[0]->room_type == 3))
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('per_person') ? 'has-error' : '' }}">
                                        <label for="per_person">Price Per Person</label>
                                        <input type="number" class="form-control" id="per_person"  min="0" name="per_person" @if(isset($propertyRoomInfo[0])) value="{{ $propertyRoomInfo[0]->price_per_person }}" @else value="{{ Request::old('per_person') }}" @endif aria-describedby="per_person">
                                    </div>
                                </div>
                                @endif
                                @if(isset($propertyRoomInfo[0]) && ($propertyRoomInfo[0]->room_type == 2 || $propertyRoomInfo[0]->room_type == 1))
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('per_unit') ? 'has-error' : '' }}">
                                        <label for="per_unit">Price Per Unit</label>
                                        <input type="number" class="form-control" id="per_unit" min="0"  name="per_unit" @if(isset($propertyRoomInfo[0])) value="{{ $propertyRoomInfo[0]->price_per_unit }}" @else value="{{ Request::old('per_unit') }}" @endif aria-describedby="per_unit">
                                    </div>
                                </div>
                                @endif
                                @if(isset($propertyRoomInfo[0]) && ($propertyRoomInfo[0]->room_type == 1))
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('max_guest') ? 'has-error' : '' }}">
                                        <label for="max_guest">Max. Guest Allowed</label>
                                        <input type="number" class="form-control" id="max_guest" min="0"  name="max_guest" @if(isset($propertyRoomInfo[0])) value="{{ $propertyRoomInfo[0]->number_guests }}" @else value="{{ Request::old('max_guest') }}" @endif aria-describedby="max_guest">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('extra_bed_price') ? 'has-error' : '' }}">
                                        <label for="extra_bed_price">Extra Bed Price</label>
                                        <input type="number" class="form-control" id="extra_bed_price" min="0"  name="extra_bed_price" @if(isset($propertyRoomInfo[0])) value="{{ $propertyRoomInfo[0]->extra_bed_price }}" @else value="{{ Request::old('extra_bed_price') }}" @endif aria-describedby="extra_bed_price">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('child_price') ? 'has-error' : '' }}">
                                        <label for="child_price">Child Price</label>
                                        <input type="number" class="form-control" id="child_price" min="0"  name="child_price" @if(isset($propertyRoomInfo[0])) value="{{ $propertyRoomInfo[0]->extra_child_price }}" @else value="{{ Request::old('child_price') }}" @endif aria-describedby="child_price">
                                    </div>
                                </div>
                                @endif
                                <div class="col-sm-12">
                                    <h3>Additional Info.</h3>
                                        <div class="form-group {{ $errors->has('weekend_price') ? 'has-error' : '' }}">
                                            <label for="weekend_price">Weekend Price</label>
                                            <input type="number" class="form-control" id="weekend_price" min="0"  name="weekend_price" @if(isset($propertyRoomInfo[0])) value="{{ $propertyRoomInfo[0]->room_weekend_price }}" @else value="{{ Request::old('weekend_price') }}" @endif aria-describedby="weekend_price">
                                        </div>
                                </div>
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <div class="col-sm-12">
                                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

