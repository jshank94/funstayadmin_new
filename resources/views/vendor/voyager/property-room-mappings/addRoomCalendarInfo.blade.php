<link rel='stylesheet' href='https://fullcalendar.io/js/fullcalendar-3.1.0/fullcalendar.min.css' />

@extends('voyager::master')

@section('css')
    <style>
        .checkbox{
            margin: 2%;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Property Room Calendar Info.
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.property-room-mappings.property-misc-options.side-nav')
                        </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.property-room-mappings.property-misc-options.message-block')
                              <div id='calendar'></div>
                        </div>
                </div>
            </div>
        </div>
    </div>
{{-- <div id='calendar'></div> --}}
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery.min.js'></script>
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/jquery-ui.min.js'></script>
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/lib/moment.min.js'></script>
<script src='https://fullcalendar.io/js/fullcalendar-3.1.0/fullcalendar.min.js'></script>

<script>
    $(function() {
        $('#calendar').fullCalendar();
    });
</script>
@stop