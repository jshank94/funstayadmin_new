@extends('voyager::master')

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
                <i class="voyager-trees"></i>  Property Listings
        </h1>
        <a href="{{ route('addPropertyRoom') }}" class="btn btn-success btn-add-new">
            <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
        </a>
        <a href="{{ url('admin/properties') }}" class="btn btn-success btn-warning">
            <i class=""></i> <span>Go to Properties</span>
        </a>
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                    <td>Title</td>
                                    <td>Price</td>
                                    <td>Description</td>
                                    <td>Accommodate's</td>
                                    <td>Rooms</td>
                                    <td>Min. Stay</td>
                                    <td>Booking Type</td>
                                    <td>Rating Score</td>
                                    <td>Action</td>
                                </thead>
                                <tbody>
                                    @if(count($rooms)>0)
                                        @foreach($rooms as $data)
                                            @if($data->deleted_at===null)
                                            <tr>
                                                <td>
                                                    {{ $data->room_title }}
                                                </td>

                                                <td>
                                                    {{ $data->room_price }}
                                                </td>
                                                
                                                <td>
                                                    {{ $data->description }}
                                                </td>
                                                <td>
                                                    {{ $data->accommodates_no }}
                                                </td>
                                                <td>
                                                    {{ $data->no_rooms }}
                                                </td>
                                                <td>
                                                    {{ $data->minimum_stay }}
                                                </td>
                                                <td>
                                                    {{ $data->booking_type }}
                                                </td>
                                                <td>
                                                    {{ $data->rating_score }}
                                                </td>
                                                
                                                <td>                                               
                                                    <a href="{{ url('admin/deletePropertyRoom') }}/{{$data->id}}" class="btn btn-sm btn-danger"><i class="voyager-delete">Delete</i></a> 
                                                    
                                                    <a style="margin-left:10px;" href="{{ url('admin/editPropertyRoom') }}/{{ $data->id }} " class="btn btn-sm btn-warning pull-right"><i class="voyager-delete">Edit</i></a>    
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @else
                                    <tr>
                                            <td colspan="9" style="text-align: center;">
                                                <span style="color:black"> No Data Found </span>
                                            </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop