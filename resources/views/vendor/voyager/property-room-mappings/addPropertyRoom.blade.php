@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Property Room Info.
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.property-room-mappings.property-misc-options.side-nav')
                        </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                            @include('vendor.voyager.property-room-mappings.property-misc-options.message-block')
                        <form @if(isset($propertyRoomInfo)) action="{{ Session::get('propertyRoomCreate') }}" @else action="" @endif method="post">
                                <div class="form-group {{ $errors->has('property') ? 'has-error' : '' }}">
                                    <label for="property">Property</label>
                                    <input type="hidden" class="form-control" id="property" name="property" value="{{ $properties->id }}">
                                    <input type="text" class="form-control" id="property_name" name="property_name" value="{{ $properties->title }}" aria-describedby="property_name" placeholder="Property Name" readonly>
                                    {{-- <select class="form-control" name="property" data-live-search="true">
                                        @foreach($properties as $val)
                                            @if(isset($property))
                                                @if($property->id_property == $val->id)
                                                    <option value="{{ $val->id }}">{{ $val->title }}</option>
                                                @endif
                                            @endif
                                            <option value="{{ $val->id }}">{{ $val->title }}</option>
                                        @endforeach
                                    </select> --}}
                                </div>
                                <div class="form-group {{ $errors->has('room_title') ? 'has-error' : '' }}">
                                    <label for="room_title">Title</label>
                                <input type="text" class="form-control" id="room_title" name="room_title" @if(isset($propertyRoomInfo)) value="{{ $propertyRoomInfo->room_title }}" @else value="{{ Request::old('room_title') }}" @endif aria-describedby="room_title" placeholder="Enter Title">
                                </div>
                                
                                <div class="col-sm-6">
                                     <div class="form-group {{ $errors->has('roomtype') ? 'has-error' : '' }}">
                                        <label for="roomtype">Room Type</label>
                                        <select class="form-control" name="roomtype" data-live-search="true">
                                                @if(isset($propertyRoomInfo))
                                                    @foreach($roomTypes as $val)
                                                        @if($propertyRoomInfo->room_type === $val->id)
                                                            <option value="{{ $val->id }}">{{ $val->room_type }}</option>
                                                        @endif    
                                                    @endforeach
                                                @else
                                                    @foreach($roomTypes as $val)
                                                        <option value="{{ $val->id }}">{{ $val->room_type }}</option>
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('propertytype') ? 'has-error' : '' }}">
                                        <label for="propertytype">Property Type</label>
                                        <select class="form-control" name="propertytype" data-live-search="true">
                                                @if(isset($propertyRoomInfo))
                                                    @foreach($propertyTypes as $val)
                                                        @if($propertyRoomInfo->property_type === $val->id)
                                                            <option value="{{ $val->id }}">{{ $val->property_type }}</option>
                                                        @endif    
                                                    @endforeach
                                                @else
                                                    @foreach($propertyTypes as $val)
                                                        <option value="{{ $val->id }}">{{ $val->property_type }}</option>
                                                    @endforeach
                                                @endif
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('accommodation') ? 'has-error' : '' }}">
                                        <label for="accommodation">Accommodate's</label>
                                        <input type="number" class="form-control" id="accommodation" min="0" name="accommodation" @if(isset($propertyRoomInfo)) value="{{ $propertyRoomInfo->accommodates_no }}" @else value="{{ Request::old('accommodation') }}" @endif aria-describedby="accommodation">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('booking_type') ? 'has-error' : '' }}">
                                        <label for="booking_type">Booking Type</label>
                                        <br><br>
                                        <input type="radio" id="booking_type"  name="booking_type" value="Request" @if(isset($propertyRoomInfo) && ($propertyRoomInfo->booking_type == 'Request')) checked @endif> Request
                                        <input type="radio" id="booking_type"  name="booking_type" value="Instant" @if(isset($propertyRoomInfo) && ($propertyRoomInfo->booking_type == 'Instant')) checked @endif> Instant
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('extrabed') ? 'has-error' : '' }}">
                                        <label for="extrabed">Extra bed Allowed</label>
                                        <input type="number" class="form-control" id="extrabed" min="0" name="extrabed" @if(isset($propertyRoomInfo)) value="{{ $propertyRoomInfo->allowed_extra_bed }}" @else value="{{ Request::old('extrabed') }}" @endif aria-describedby="extrabed">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('no_of_rooms') ? 'has-error' : '' }}">
                                        <label for="no_of_rooms">Number of Rooms</label>
                                        <input type="number" class="form-control" id="no_of_rooms" min="0" name="no_of_rooms" @if(isset($propertyRoomInfo)) value="{{ $propertyRoomInfo->no_rooms }}" @else value="{{ Request::old('no_of_rooms') }}" @endif aria-describedby="no_of_rooms">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('min_stay') ? 'has-error' : '' }}">
                                        <label for="min_stay">Stay Allowed (Min.)</label>
                                        <input type="number" class="form-control" id="min_stay" min="0" name="min_stay" @if(isset($propertyRoomInfo)) value="{{ $propertyRoomInfo->minimum_stay }}" @else value="{{ Request::old('min_stay') }}" @endif aria-describedby="min_stay">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('max_stay') ? 'has-error' : '' }}">
                                        <label for="max_stay">Stay Allowed (Max.)</label>
                                        <input type="number" class="form-control" id="max_stay" min="0" name="max_stay" @if(isset($propertyRoomInfo)) value="{{ $propertyRoomInfo->maximum_stay }}" @else value="{{ Request::old('max_stay') }}" @endif aria-describedby="max_stay">
                                    </div>
                                </div>
                                <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" name="description" id="description" style="height:200px" placeholder="Add Description">@if(isset($propertyRoomInfo)) {{ $propertyRoomInfo->description }} @else {{ Request::old('description') }} @endif</textarea>
                                </div>
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

