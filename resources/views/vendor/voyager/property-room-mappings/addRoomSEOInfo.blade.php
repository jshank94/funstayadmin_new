@extends('voyager::master')

@section('css')
    <style>
        .checkbox{
            margin: 2%;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Property Room SEO Info.
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.property-room-mappings.property-misc-options.side-nav')
                        </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.property-room-mappings.property-misc-options.message-block')
                            <form action="" method="post">
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('meta_title') ? 'has-error' : '' }}">
                                        <label for="meta_title">Meta Title</label>
                                        <input type="text" class="form-control" id="meta_title" name="meta_title" @if(isset($propertyRoomInfo)) value="{{ $propertyRoomInfo[0]->meta_title }}" @else value="{{ Request::old('meta_title') }}" @endif aria-describedby="meta_title">
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('meta_keyword') ? 'has-error' : '' }}">
                                        <label for="meta_keyword">Meta Keyword **Comma(,) seperated**</label>
                                        <input type="text" class="form-control" id="meta_keyword" name="meta_keyword" @if(isset($propertyRoomInfo)) value="{{ $propertyRoomInfo[0]->meta_tags }}" @else value="{{ Request::old('meta_keyword') }}" @endif aria-describedby="meta_keyword">
                                    </div>
                                </div>
                                
                                <div class="col-sm-12">
                                    <div class="form-group {{ $errors->has('meta_description') ? 'has-error' : '' }}">
                                        <label for="meta_description">Meta Description</label>
                                        <textarea class="form-control richTextBox" name="meta_description" id="meta_description" style="height:200px" placeholder="Add Meta Description">@if(isset($propertyRoomInfo)) {{ $propertyRoomInfo[0]->meta_desc }} @else {{ Request::old('meta_description') }} @endif </textarea>
                                    </div>
                                </div>
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <div class="col-sm-12">
                                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

