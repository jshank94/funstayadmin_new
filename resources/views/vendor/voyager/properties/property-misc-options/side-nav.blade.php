<ul class="nav nav-pills nav-stacked" id="stacked-menu">
    <li>
        <ul class="nav nav-pills nav-stacked">
            <li><a class="btn btn-lg btn-primary" href="{{ url('admin/properties') }}">Go to Properties</a></li>
            <hr>
            <li class="{{ (Request::segment(2)=='addProperty' || Request::segment(2)=='editProperty')?'active':'' }}"><a @if(Session::get('propertyCreate')) href="editProperty/{{ Session::get('propertyCreate') }}" @else href="addProperty" @endif>Basic Info.</a></li>
            <li class="{{ (Request::segment(2)=='addLocationInfo')?'active':'' }}"><a href="{{ url('admin/addLocationInfo') }}">Location details</a></li>
            <li class="{{ (Request::segment(2)=='addImages')?'active':'' }}"><a href="{{ url('admin/addImages') }}">Images</a></li>
            {{-- <li class="{{ (Request::segment(2)=='addVendorInfo')?'active':'' }}"><a href="{{ url('admin/addVendorInfo') }}">Vendor Info.</a></li> --}}
            <li class="{{ (Request::segment(2)=='addAmenity')?'active':'' }}"><a href="{{ url('admin/addAmenity') }}">Amenities</a></li>
            <li class="{{ (Request::segment(2)=='addTags')?'active':'' }}"><a href="{{ url('admin/addTags') }}">Tags</a></li>
            <li class="{{ (Request::segment(2)=='addHouseRules')?'active':'' }}"><a href="{{ url('admin/addHouseRules') }}">House Rules</a></li>
            <li class="{{ (Request::segment(2)=='addAdditionalInfo')?'active':'' }}"><a href="{{ url('admin/addAdditionalInfo') }}">Additional Notes</a></li>
            {{-- <li class="{{ (Request::segment(2)=='addNearBy')?'active':'' }}"><a href="{{ url('admin/addNearBy') }}">Near by</a></li> --}}
            {{-- <li class="{{ (Request::segment(2)=='addActivities')?'active':'' }}"><a href="{{ url('admin/addActivities') }}">Activites</a></li> --}}
            <li class="{{ (Request::segment(2)=='addPropertyRoom')?'active':'' }}"><a @if(Session::get('propertyCreate')) href="{{ url('admin/getPropertyRooms') }}/{{ Session::get('propertyCreate') }}" @else href="{{ url('admin/addProperty') }}" @endif class="btn btn-sm btn-warning"> Manage Property Listings </a></li>
        </ul>
    </li>
</ul>