@extends('voyager::master')

@section('css')

    <style>
        .checkbox{
            margin: 2%;
        }
    </style>
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Property Tags
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.properties.property-misc-options.side-nav')
                        </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                        @if(isset($propertyTags)) 
                        <div class="col-sm-12" style="margin-top:10px">
                            @foreach($propertyTags as $vl) 
                                @foreach($tags as $t_vl)
                                    @if($t_vl['id']== $vl->id_tag)
                                        <span style="font-size:12px; cursor: pointer;" class="label label-primary">{{ $t_vl['name'] }}</span>
                                    @endif
                                @endforeach
                                @foreach($subTags as $st_vl)
                                    @if($st_vl['id']== $vl->id_sub_tag)
                                        <span style="font-size:12px; cursor: pointer;" class="label label-info">{{ $st_vl['name'] }}</span>
                                    @endif
                                @endforeach
                            @endforeach 
                        </div>
                        @endif
                        <div class="panel-body">
                            @include('vendor.voyager.properties.property-misc-options.message-block')
                            <form action="" method="post">
                                <div class="form-group {{ $errors->has('tagtypes') ? 'has-error' : '' }}">
                                    <label for="tagtypes">Select Tag Type</label>
                                    <select class="form-control select2" name="tagtype" onchange="selectTagType(event)">
                                        @foreach($tagTypes as $val)
                                            <option value="{{ $val->id }}">{{ $val->tag_type_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <label for="tags" id="allTitle"></label>
                                <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}" id="alltags">
                                </div>
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        function selectTagType(event) {
            $('#alltags').empty();
            var id = event.target.value;
            var getAllTagsUrl = '{{ url('admin/getAllTags/') }}';
            var getAllSubTagsUrl = '{{ url('admin/getAllSubTags/') }}';
            
            $('#alltags').html('Loading...');
            $.ajax({
                url: getAllTagsUrl+'/'+id,
                type: "GET",
                dataType: "json",
                success:function(data) {
                    $('#alltags').html('');
                    $('#allTitle').text('Select Tags');

                    $.each(data, function(key, value) {

                        $('#alltags').append('<input type="checkbox" name="tags[]" value="'+value.id+'">'+ value.name +'<br>');
                        
                        $.ajax({
                            url: getAllSubTagsUrl+'/'+value.id,
                            type: "GET",
                            async: false,
                            dataType: "json",
                            success:function(data2) {
                                if(data2.length > 0){
                                    $.each(data2, function(key2, value2) {
                                    $('#alltags').append('<input style="margin-left:10px" type="checkbox" name="subtags[]" value="'+value2.id+'">'+ value2.name +'<br>');
                                });
                                }
                            }
                        });
                    });
                }
            });
        }
    </script>
@stop    
