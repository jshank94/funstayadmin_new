@extends('voyager::master')

@section('css')

    <style>
        .checkbox{
            margin: 2%;
        }
    </style>

    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Amenities Info.
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.properties.property-misc-options.side-nav')
                        </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.properties.property-misc-options.message-block')
                                <form action="" method="post">
                                    <div class="form-group {{ $errors->has('amenities') ? 'has-error' : '' }}">
                                        <label for="amenities">Select Amenities</label>
                                        @foreach($amenities as $val)
                                            <div class="checkbox">
                                                <input type="checkbox" name="amenities[]" value="{{ $val->id }}"
                                                @if(isset($propertyAmenities)) 
                                                @foreach($propertyAmenities as $vl) 
                                                    @if($vl->id_amenity == $val->id) 
                                                    checked="checked"
                                                    @endif 
                                                @endforeach 
                                            @endif>{{ $val->name }}
                                            </div>
                                        @endforeach
                                    </div>
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                                </form>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

