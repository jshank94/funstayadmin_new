
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC_6CFitsmFzbjsCyBg5lC_HANadcgBI6k&v=3.exp&sensor=false&libraries=places"></script>

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Location Info.
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.properties.property-misc-options.side-nav')
                        </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.properties.property-misc-options.message-block')
                                <form action="" method="post">
                                    <div class="form-group {{ $errors->has('address1') ? 'has-error' : '' }}">
                                        <label for="address1">Address Line 1</label>
                                        <input type="text" class="form-control" id="address1" onBlur="update_location()" name="address1" @if(isset($locationInfo)) value="{{ $locationInfo->address_line_1 }}" @else value="{{ Request::old('address1') }}" @endif aria-describedby="address1" placeholder="Enter Address">
                                    </div>
                                    <div class="form-group {{ $errors->has('address2') ? 'has-error' : '' }}">
                                        <label for="address2">Address Line 2</label>
                                        <input type="text" class="form-control" id="address2" name="address2" @if(isset($locationInfo)) value="{{ $locationInfo->address_line_2 }}" @else value="{{ Request::old('address2') }}" @endif aria-describedby="address2" placeholder="Address Line 2">
                                    </div>
                                    <div class="form-group {{ $errors->has('latitude') ? 'has-error' : '' }}">
                                        <label for="latitude">Latitude</label>
                                        <input type="text" class="form-control" id="latitude" name="latitude" @if(isset($locationInfo)) value="{{ $locationInfo->latitude }}" @else value="{{ Request::old('latitude') }}" @endif aria-describedby="latitude" placeholder="Enter Latitude">
                                    </div>
                                    <div class="form-group {{ $errors->has('longitude') ? 'has-error' : '' }}">
                                        <label for="longitude">Longitude</label>
                                        <input type="text" class="form-control" id="longitude" name="longitude" @if(isset($locationInfo)) value="{{ $locationInfo->longitude }}" @else value="{{ Request::old('longitude') }}" @endif aria-describedby="longitude" placeholder="Enter Longitude">
                                    </div>
                                    <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                                        <label for="city">City</label>
                                        <select class="form-control" name="city" data-live-search="true">
                                            @if(isset($locationInfo))
                                                @foreach($cities as $val)
                                                    @if($locationInfo->id_city === $val->id)
                                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                    @endif    
                                                @endforeach
                                            @else
                                                @foreach($cities as $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                                        <label for="state">State</label>
                                        <select class="form-control" name="state" data-live-search="true">
                                            @if(isset($locationInfo))
                                                @foreach($states as $val)
                                                    @if($locationInfo->id_state === $val->id)
                                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                    @endif    
                                                @endforeach
                                            @else
                                                @foreach($states as $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group {{ $errors->has('pincode') ? 'has-error' : '' }}">
                                        <label for="pincode">PIN Code</label>
                                        <input type="text" class="form-control" id="pincode" name="pincode" @if(isset($locationInfo)) value="{{ $locationInfo->zipcode }}" @else value="{{ Request::old('pincode') }}" @endif aria-describedby="pincode" placeholder="Enter PIN Code">
                                    </div>
                                    <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                                        <label for="country">Country</label>
                                        <select class="form-control" name="country" data-live-search="true">
                                            @if(isset($locationInfo))
                                                @foreach($countries as $val)
                                                    @if($locationInfo->id_country === $val->id)
                                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                    @endif    
                                                @endforeach
                                            @else
                                                @foreach($countries as $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <input type="hidden" name="_token" value="{{ Session::token() }}">
                                    @if(isset($locationInfo)) 
                                        <input type="hidden" name="id_address" value="{{ $locationInfo->id }}">
                                    @endif
                                    <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                                </form>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        function init() {
            var input = document.getElementById('address1');
            var autocomplete = new google.maps.places.Autocomplete(input);
        }

        google.maps.event.addDomListener(window, 'load', init);
          
        function update_location(){	
            var geocoder = new google.maps.Geocoder();
            var address = document.getElementById('address1').value;

            geocoder.geocode( { 'address': address}, function(results, status) {

            if(status !== google.maps.GeocoderStatus.OK)
                {
                    alert(status);
                }
                // This is checking to see if the Geoeode Status is OK before proceeding    
                if (status == google.maps.GeocoderStatus.OK) {

                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();

                    //This is placing the returned address in the 'Address' field on the HTML form  
                    document.getElementById('latitude').value = latitude;
                    document.getElementById('longitude').value = longitude;

                    
                } 
            }); 
        }
</script>
@stop