@extends('voyager::master')

@section('css')
    <style>
        .checkbox{
            margin: 2%;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Property Info.
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                                @include('vendor.voyager.properties.property-misc-options.side-nav')
                        </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                        <div class="panel-body">
                            @include('vendor.voyager.properties.property-misc-options.message-block')
                        <form @if(isset($property)) action="{{ $idProperty }}" @else action="" @endif method="post">
                                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                    <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title" @if(isset($property)) value="{{ $property->title }}" @else value="{{ Request::old('title') }}" @endif aria-describedby="title" placeholder="Enter Title">
                                </div>
                                <div class="form-group {{ $errors->has('id_user') ? 'has-error' : '' }}">
                                    <label for="id_user">Select Vendor</label>
                                    @if(count($users)>0)
                                        <select class="form-control select2" name="id_user" data-live-search="true"> 
                                            @if(isset($property))
                                                @foreach($users as $val)
                                                    @if($property->id_user === $val->id)
                                                        <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                    @endif    
                                                @endforeach
                                            @else
                                                @foreach($users as $val)
                                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    @else 
                                        <br>
                                        <span><a class="btn btn-info" href="{{ url('admin/users') }}">Add Vendor first </a></span>
                                        <br>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                                    <label for="language">Select Vendor Language</label>
                                    @foreach($languages as $val)
                                        <div class="checkbox">
                                            <input type="checkbox" name="language[]" value="{{ $val->id }}" 
                                            @if(isset($vendLanguages)) 
                                                @foreach($vendLanguages as $vl) 
                                                    @if($vl->id_language == $val->id) 
                                                    checked="checked"
                                                    @endif 
                                                @endforeach 
                                            @endif >{{ $val->language_name }}
                                        </div>
                                    @endforeach
                                </div>
                                <input type="hidden" name="_token" value="{{ Session::token() }}">
                                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

