@extends('voyager::master')

@section('css')
    <style>
        .checkbox{
            margin: 2%;
        }
    </style>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-company"></i>
        Upload CSV file
    </h1>
  
    @include('voyager::multilingual.language-selector')
@stop
@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col-md-8">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <label for="sample">Get Sample File</label>
                        <select class="form-control"  onchange="myFunction(event)">
                            <option value="languages">languages</option>
                            <option value="locations">locations</option>
                            <option value="property_vendor_languages">Property Vendor Language</option>
                            <option value="property_images">property images</option>
                            <option value=" property_list_images"> property room images</option>
                            <option value="property_meals">property meals</option>
                            <option value="property_rooms_meals_map">property rooms meals map</option>
                            
                            <option value="tag_types">tag types</option>
                            <option value="sub_tags">sub tags</option>
                            <option value="tags">tags</option>
                            <option value="property_types">property types</option> -->
                            <!-- <option value="property_has_subtags">property has subtags</option> -->
                            <!-- <option value="property_location_nearby">property location nearby</option> -->
                            <!-- <option value="listing_cancellation_policy">listing cancellation policy</option> -->
                            <option value="users">Users</option>
                            <option value="properties">Add property</option>
                            <option value="property_listings">Property listings</option>
                            <option value="amenities">Add Amenities</option>
                            <option value="property_list_amenity_maps">Add Property List Amenities Mapping</option> -->
                        </select>   
                        <br><br>
                        <div id="tab" class="text-center"></div>
                        <br><br>
                        @include('vendor.voyager.properties.property-misc-options.message-block')
                        <form  action="" method="post" enctype="multipart/form-data">
                            <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                                <label for="file">Select file (.csv)</label>
                                <input type="file" class="form-control" id="file" name="file">
                            </div>
                            <input type="hidden" name="file_name" id="inp_filename" value="">
                            <input type="hidden" name="_token" value="{{ Session::token() }}">
                            <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop
@section('javascript')
<script>
        function myFunction(e) {
            $('#tab').empty();
            var id = e.target.value;
            var downloadUrl = "{{ url('admin/downloadFile/') }}";
            var readCsvUrl = "{{ url('') }}";
            $('#inp_filename').val(id);
            $('#tab').append('<a class="btn btn-sm btn-primary" href="'+downloadUrl+'/'+id+'">Click here to download dummy file '+id+'</a>');
        }
    </script>
@end
