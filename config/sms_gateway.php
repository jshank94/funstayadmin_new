<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sms Gateway configurations (will mainly be used for otp purposes)
    |--------------------------------------------------------------------------
    | It mainly refers to smsjust sms gateway service
    */

    'endpoint' => 'http://www.smsjust.com/blank/sms/user/urlsms.php',
    'parameters' => [
        'message' => '',
        'dest_mobileno' => '',
        'username' => 'funstay',
        'pass' => 123456,
        'senderid' => 'FNSTAY',
        'tempid' => 32980
    ],

    'msg91' => [
        'sender_id' => 'FNSTAY',
        'auth_key' => env('MSG91_AUTH_KEY'),
        'transactional_route' => env('MSG91_TRANSACTIONAL_ROUTE'),
        'promotional_route' => env('MSG91_PROMOTIONAL_ROUTE')
    ]
];
