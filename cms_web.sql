-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: cms_web
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (1,'Football','2019-01-17 06:54:46','2019-01-17 06:54:46');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pincode` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_property` int(11) DEFAULT NULL,
  `city` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (1,'GF4, SUMUKAHA HOMES,RAMANASHREE ENCLAVE','Karnataka',560076,'2019-01-16 05:35:52','2019-01-16 05:35:52',NULL,NULL,NULL,NULL,NULL),(2,'Funstay Services Pvt Ltd - Homestay in Coorg, Ooty, Ladakh, Andaman Tour Packages, 24th Main Road, BDA Layout, HSR Layout, Bengaluru, Karnataka, India','Karnataka',560076,'2019-01-22 06:50:45','2019-01-22 06:50:45',7,'Bengaluru','INDIA','38.3589164','-85.701864'),(3,'Polar Bear Ice Creams, Sector 3, HSR Layout, Bengaluru, Karnataka, India','Karnataka',560076,'2019-01-25 06:31:03','2019-01-25 06:31:03',5,'Bengaluru','INDIA','36.073051','-95.83927');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `amenities`
--

DROP TABLE IF EXISTS `amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `amenities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(488) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `amenities`
--

LOCK TABLES `amenities` WRITE;
/*!40000 ALTER TABLE `amenities` DISABLE KEYS */;
INSERT INTO `amenities` VALUES (1,'Swimming Pool','voyager-location','Test','Property','2019-01-16 05:41:43','2019-01-16 05:41:43');
/*!40000 ALTER TABLE `amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `best_seller_banner_details`
--

DROP TABLE IF EXISTS `best_seller_banner_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `best_seller_banner_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property` int(11) NOT NULL,
  `thumb_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb_desc` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `search_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumb_img` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activity_list` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `best_seller_banner_details`
--

LOCK TABLES `best_seller_banner_details` WRITE;
/*!40000 ALTER TABLE `best_seller_banner_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `best_seller_banner_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `best_seller_experiences`
--

DROP TABLE IF EXISTS `best_seller_experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `best_seller_experiences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_bestseller` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bgimg` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seperator_1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seperator_2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `best_seller_experiences`
--

LOCK TABLES `best_seller_experiences` WRITE;
/*!40000 ALTER TABLE `best_seller_experiences` DISABLE KEYS */;
/*!40000 ALTER TABLE `best_seller_experiences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `best_seller_seo_datas`
--

DROP TABLE IF EXISTS `best_seller_seo_datas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `best_seller_seo_datas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_bestseller` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_tag` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `best_seller_seo_datas`
--

LOCK TABLES `best_seller_seo_datas` WRITE;
/*!40000 ALTER TABLE `best_seller_seo_datas` DISABLE KEYS */;
/*!40000 ALTER TABLE `best_seller_seo_datas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_bills`
--

DROP TABLE IF EXISTS `booking_bills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_bills` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_booking` int(11) NOT NULL,
  `room_price` int(11) DEFAULT NULL,
  `guest_price` int(11) DEFAULT NULL,
  `extra_bed_price` int(11) DEFAULT NULL,
  `child_price` int(11) DEFAULT NULL,
  `meal_price` int(11) DEFAULT NULL,
  `booking_price` int(11) DEFAULT NULL,
  `funstay_commission` int(11) DEFAULT NULL,
  `total_discount` int(11) DEFAULT NULL,
  `igst` int(11) DEFAULT NULL,
  `sgst` int(11) DEFAULT NULL,
  `cgst` int(11) DEFAULT NULL,
  `final_price` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_bills`
--

LOCK TABLES `booking_bills` WRITE;
/*!40000 ALTER TABLE `booking_bills` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_bills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_payment_statuses`
--

DROP TABLE IF EXISTS `booking_payment_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_payment_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_payment_statuses`
--

LOCK TABLES `booking_payment_statuses` WRITE;
/*!40000 ALTER TABLE `booking_payment_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_payment_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_payments`
--

DROP TABLE IF EXISTS `booking_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_booking` int(11) NOT NULL,
  `id_payment_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transactionID` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderID` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_ref_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `failure_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_msg` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_payments`
--

LOCK TABLES `booking_payments` WRITE;
/*!40000 ALTER TABLE `booking_payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_statuses`
--

DROP TABLE IF EXISTS `booking_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_statuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_statuses`
--

LOCK TABLES `booking_statuses` WRITE;
/*!40000 ALTER TABLE `booking_statuses` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_taxes`
--

DROP TABLE IF EXISTS `booking_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_taxes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_taxes`
--

LOCK TABLES `booking_taxes` WRITE;
/*!40000 ALTER TABLE `booking_taxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_user_refunds`
--

DROP TABLE IF EXISTS `booking_user_refunds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_user_refunds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_booking` int(11) NOT NULL,
  `customer_refund_amount` int(11) DEFAULT NULL,
  `booking_amount` int(11) NOT NULL,
  `is_refund_to_user` int(11) NOT NULL,
  `refunded_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_user_refunds`
--

LOCK TABLES `booking_user_refunds` WRITE;
/*!40000 ALTER TABLE `booking_user_refunds` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_user_refunds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_property` int(11) NOT NULL,
  `check_in_date` date DEFAULT NULL,
  `check_out_date` date DEFAULT NULL,
  `no_of_rooms` int(11) DEFAULT NULL,
  `no_of_guests` int(11) DEFAULT NULL,
  `no_of_extra_beds` int(11) DEFAULT NULL,
  `no_of_child` int(11) DEFAULT NULL,
  `no_of_infant` int(11) DEFAULT NULL,
  `meal_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bookink_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_coupon` int(11) DEFAULT NULL,
  `id_status` int(11) DEFAULT NULL,
  `approved_at` timestamp NULL DEFAULT NULL,
  `complimentary_food` int(11) DEFAULT NULL,
  `check_in_time` time DEFAULT NULL,
  `check_out_time` time DEFAULT NULL,
  `customer_gstin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_gstin` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_guest_name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_customer_contact_no` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `primary_guest_email_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Category 1','category-1','2019-01-05 05:12:02','2019-01-05 05:12:02'),(2,NULL,1,'Category 2','category-2','2019-01-05 05:12:02','2019-01-05 05:12:02');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'Jamshedpur','2019-01-16 05:33:54','2019-01-16 05:33:54');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `complaints`
--

DROP TABLE IF EXISTS `complaints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `complaints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `for_id_user` int(11) DEFAULT NULL,
  `who_id_user` int(11) DEFAULT NULL,
  `is_read` int(11) DEFAULT NULL,
  `is_custom` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complaints`
--

LOCK TABLES `complaints` WRITE;
/*!40000 ALTER TABLE `complaints` DISABLE KEYS */;
/*!40000 ALTER TABLE `complaints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `iso` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nicename` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iso3` char(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'AF','AFGHANISTAN','Afghanistan','AFG',4,93),(2,'AL','ALBANIA','Albania','ALB',8,355),(3,'DZ','ALGERIA','Algeria','DZA',12,213),(4,'AS','AMERICAN SAMOA','American Samoa','ASM',16,1684),(5,'AD','ANDORRA','Andorra','AND',20,376),(6,'AO','ANGOLA','Angola','AGO',24,244),(7,'AI','ANGUILLA','Anguilla','AIA',660,1264),(8,'AQ','ANTARCTICA','Antarctica',NULL,NULL,0),(9,'AG','ANTIGUA AND BARBUDA','Antigua and Barbuda','ATG',28,1268),(10,'AR','ARGENTINA','Argentina','ARG',32,54),(11,'AM','ARMENIA','Armenia','ARM',51,374),(12,'AW','ARUBA','Aruba','ABW',533,297),(13,'AU','AUSTRALIA','Australia','AUS',36,61),(14,'AT','AUSTRIA','Austria','AUT',40,43),(15,'AZ','AZERBAIJAN','Azerbaijan','AZE',31,994),(16,'BS','BAHAMAS','Bahamas','BHS',44,1242),(17,'BH','BAHRAIN','Bahrain','BHR',48,973),(18,'BD','BANGLADESH','Bangladesh','BGD',50,880),(19,'BB','BARBADOS','Barbados','BRB',52,1246),(20,'BY','BELARUS','Belarus','BLR',112,375),(21,'BE','BELGIUM','Belgium','BEL',56,32),(22,'BZ','BELIZE','Belize','BLZ',84,501),(23,'BJ','BENIN','Benin','BEN',204,229),(24,'BM','BERMUDA','Bermuda','BMU',60,1441),(25,'BT','BHUTAN','Bhutan','BTN',64,975),(26,'BO','BOLIVIA','Bolivia','BOL',68,591),(27,'BA','BOSNIA AND HERZEGOVINA','Bosnia and Herzegovina','BIH',70,387),(28,'BW','BOTSWANA','Botswana','BWA',72,267),(29,'BV','BOUVET ISLAND','Bouvet Island',NULL,NULL,0),(30,'BR','BRAZIL','Brazil','BRA',76,55),(31,'IO','BRITISH INDIAN OCEAN TERRITORY','British Indian Ocean Territory',NULL,NULL,246),(32,'BN','BRUNEI DARUSSALAM','Brunei Darussalam','BRN',96,673),(33,'BG','BULGARIA','Bulgaria','BGR',100,359),(34,'BF','BURKINA FASO','Burkina Faso','BFA',854,226),(35,'BI','BURUNDI','Burundi','BDI',108,257),(36,'KH','CAMBODIA','Cambodia','KHM',116,855),(37,'CM','CAMEROON','Cameroon','CMR',120,237),(38,'CA','CANADA','Canada','CAN',124,1),(39,'CV','CAPE VERDE','Cape Verde','CPV',132,238),(40,'KY','CAYMAN ISLANDS','Cayman Islands','CYM',136,1345),(41,'CF','CENTRAL AFRICAN REPUBLIC','Central African Republic','CAF',140,236),(42,'TD','CHAD','Chad','TCD',148,235),(43,'CL','CHILE','Chile','CHL',152,56),(44,'CN','CHINA','China','CHN',156,86),(45,'CX','CHRISTMAS ISLAND','Christmas Island',NULL,NULL,61),(46,'CC','COCOS (KEELING) ISLANDS','Cocos (Keeling) Islands',NULL,NULL,672),(47,'CO','COLOMBIA','Colombia','COL',170,57),(48,'KM','COMOROS','Comoros','COM',174,269),(49,'CG','CONGO','Congo','COG',178,242),(50,'CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','Congo, the Democratic Republic of the','COD',180,242),(51,'CK','COOK ISLANDS','Cook Islands','COK',184,682),(52,'CR','COSTA RICA','Costa Rica','CRI',188,506),(53,'CI','COTE D\'IVOIRE','Cote D\'Ivoire','CIV',384,225),(54,'HR','CROATIA','Croatia','HRV',191,385),(55,'CU','CUBA','Cuba','CUB',192,53),(56,'CY','CYPRUS','Cyprus','CYP',196,357),(57,'CZ','CZECH REPUBLIC','Czech Republic','CZE',203,420),(58,'DK','DENMARK','Denmark','DNK',208,45),(59,'DJ','DJIBOUTI','Djibouti','DJI',262,253),(60,'DM','DOMINICA','Dominica','DMA',212,1767),(61,'DO','DOMINICAN REPUBLIC','Dominican Republic','DOM',214,1809),(62,'EC','ECUADOR','Ecuador','ECU',218,593),(63,'EG','EGYPT','Egypt','EGY',818,20),(64,'SV','EL SALVADOR','El Salvador','SLV',222,503),(65,'GQ','EQUATORIAL GUINEA','Equatorial Guinea','GNQ',226,240),(66,'ER','ERITREA','Eritrea','ERI',232,291),(67,'EE','ESTONIA','Estonia','EST',233,372),(68,'ET','ETHIOPIA','Ethiopia','ETH',231,251),(69,'FK','FALKLAND ISLANDS (MALVINAS)','Falkland Islands (Malvinas)','FLK',238,500),(70,'FO','FAROE ISLANDS','Faroe Islands','FRO',234,298),(71,'FJ','FIJI','Fiji','FJI',242,679),(72,'FI','FINLAND','Finland','FIN',246,358),(73,'FR','FRANCE','France','FRA',250,33),(74,'GF','FRENCH GUIANA','French Guiana','GUF',254,594),(75,'PF','FRENCH POLYNESIA','French Polynesia','PYF',258,689),(76,'TF','FRENCH SOUTHERN TERRITORIES','French Southern Territories',NULL,NULL,0),(77,'GA','GABON','Gabon','GAB',266,241),(78,'GM','GAMBIA','Gambia','GMB',270,220),(79,'GE','GEORGIA','Georgia','GEO',268,995),(80,'DE','GERMANY','Germany','DEU',276,49),(81,'GH','GHANA','Ghana','GHA',288,233),(82,'GI','GIBRALTAR','Gibraltar','GIB',292,350),(83,'GR','GREECE','Greece','GRC',300,30),(84,'GL','GREENLAND','Greenland','GRL',304,299),(85,'GD','GRENADA','Grenada','GRD',308,1473),(86,'GP','GUADELOUPE','Guadeloupe','GLP',312,590),(87,'GU','GUAM','Guam','GUM',316,1671),(88,'GT','GUATEMALA','Guatemala','GTM',320,502),(89,'GN','GUINEA','Guinea','GIN',324,224),(90,'GW','GUINEA-BISSAU','Guinea-Bissau','GNB',624,245),(91,'GY','GUYANA','Guyana','GUY',328,592),(92,'HT','HAITI','Haiti','HTI',332,509),(93,'HM','HEARD ISLAND AND MCDONALD ISLANDS','Heard Island and Mcdonald Islands',NULL,NULL,0),(94,'VA','HOLY SEE (VATICAN CITY STATE)','Holy See (Vatican City State)','VAT',336,39),(95,'HN','HONDURAS','Honduras','HND',340,504),(96,'HK','HONG KONG','Hong Kong','HKG',344,852),(97,'HU','HUNGARY','Hungary','HUN',348,36),(98,'IS','ICELAND','Iceland','ISL',352,354),(99,'IN','INDIA','India','IND',356,91),(100,'ID','INDONESIA','Indonesia','IDN',360,62),(101,'IR','IRAN, ISLAMIC REPUBLIC OF','Iran, Islamic Republic of','IRN',364,98),(102,'IQ','IRAQ','Iraq','IRQ',368,964),(103,'IE','IRELAND','Ireland','IRL',372,353),(104,'IL','ISRAEL','Israel','ISR',376,972),(105,'IT','ITALY','Italy','ITA',380,39),(106,'JM','JAMAICA','Jamaica','JAM',388,1876),(107,'JP','JAPAN','Japan','JPN',392,81),(108,'JO','JORDAN','Jordan','JOR',400,962),(109,'KZ','KAZAKHSTAN','Kazakhstan','KAZ',398,7),(110,'KE','KENYA','Kenya','KEN',404,254),(111,'KI','KIRIBATI','Kiribati','KIR',296,686),(112,'KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','Korea, Democratic People\'s Republic of','PRK',408,850),(113,'KR','KOREA, REPUBLIC OF','Korea, Republic of','KOR',410,82),(114,'KW','KUWAIT','Kuwait','KWT',414,965),(115,'KG','KYRGYZSTAN','Kyrgyzstan','KGZ',417,996),(116,'LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','Lao People\'s Democratic Republic','LAO',418,856),(117,'LV','LATVIA','Latvia','LVA',428,371),(118,'LB','LEBANON','Lebanon','LBN',422,961),(119,'LS','LESOTHO','Lesotho','LSO',426,266),(120,'LR','LIBERIA','Liberia','LBR',430,231),(121,'LY','LIBYAN ARAB JAMAHIRIYA','Libyan Arab Jamahiriya','LBY',434,218),(122,'LI','LIECHTENSTEIN','Liechtenstein','LIE',438,423),(123,'LT','LITHUANIA','Lithuania','LTU',440,370),(124,'LU','LUXEMBOURG','Luxembourg','LUX',442,352),(125,'MO','MACAO','Macao','MAC',446,853),(126,'MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','Macedonia, the Former Yugoslav Republic of','MKD',807,389),(127,'MG','MADAGASCAR','Madagascar','MDG',450,261),(128,'MW','MALAWI','Malawi','MWI',454,265),(129,'MY','MALAYSIA','Malaysia','MYS',458,60),(130,'MV','MALDIVES','Maldives','MDV',462,960),(131,'ML','MALI','Mali','MLI',466,223),(132,'MT','MALTA','Malta','MLT',470,356),(133,'MH','MARSHALL ISLANDS','Marshall Islands','MHL',584,692),(134,'MQ','MARTINIQUE','Martinique','MTQ',474,596),(135,'MR','MAURITANIA','Mauritania','MRT',478,222),(136,'MU','MAURITIUS','Mauritius','MUS',480,230),(137,'YT','MAYOTTE','Mayotte',NULL,NULL,269),(138,'MX','MEXICO','Mexico','MEX',484,52),(139,'FM','MICRONESIA, FEDERATED STATES OF','Micronesia, Federated States of','FSM',583,691),(140,'MD','MOLDOVA, REPUBLIC OF','Moldova, Republic of','MDA',498,373),(141,'MC','MONACO','Monaco','MCO',492,377),(142,'MN','MONGOLIA','Mongolia','MNG',496,976),(143,'MS','MONTSERRAT','Montserrat','MSR',500,1664),(144,'MA','MOROCCO','Morocco','MAR',504,212),(145,'MZ','MOZAMBIQUE','Mozambique','MOZ',508,258),(146,'MM','MYANMAR','Myanmar','MMR',104,95),(147,'NA','NAMIBIA','Namibia','NAM',516,264),(148,'NR','NAURU','Nauru','NRU',520,674),(149,'NP','NEPAL','Nepal','NPL',524,977),(150,'NL','NETHERLANDS','Netherlands','NLD',528,31),(151,'AN','NETHERLANDS ANTILLES','Netherlands Antilles','ANT',530,599),(152,'NC','NEW CALEDONIA','New Caledonia','NCL',540,687),(153,'NZ','NEW ZEALAND','New Zealand','NZL',554,64),(154,'NI','NICARAGUA','Nicaragua','NIC',558,505),(155,'NE','NIGER','Niger','NER',562,227),(156,'NG','NIGERIA','Nigeria','NGA',566,234),(157,'NU','NIUE','Niue','NIU',570,683),(158,'NF','NORFOLK ISLAND','Norfolk Island','NFK',574,672),(159,'MP','NORTHERN MARIANA ISLANDS','Northern Mariana Islands','MNP',580,1670),(160,'NO','NORWAY','Norway','NOR',578,47),(161,'OM','OMAN','Oman','OMN',512,968),(162,'PK','PAKISTAN','Pakistan','PAK',586,92),(163,'PW','PALAU','Palau','PLW',585,680),(164,'PS','PALESTINIAN TERRITORY, OCCUPIED','Palestinian Territory, Occupied',NULL,NULL,970),(165,'PA','PANAMA','Panama','PAN',591,507),(166,'PG','PAPUA NEW GUINEA','Papua New Guinea','PNG',598,675),(167,'PY','PARAGUAY','Paraguay','PRY',600,595),(168,'PE','PERU','Peru','PER',604,51),(169,'PH','PHILIPPINES','Philippines','PHL',608,63),(170,'PN','PITCAIRN','Pitcairn','PCN',612,0),(171,'PL','POLAND','Poland','POL',616,48),(172,'PT','PORTUGAL','Portugal','PRT',620,351),(173,'PR','PUERTO RICO','Puerto Rico','PRI',630,1787),(174,'QA','QATAR','Qatar','QAT',634,974),(175,'RE','REUNION','Reunion','REU',638,262),(176,'RO','ROMANIA','Romania','ROM',642,40),(177,'RU','RUSSIAN FEDERATION','Russian Federation','RUS',643,70),(178,'RW','RWANDA','Rwanda','RWA',646,250),(179,'SH','SAINT HELENA','Saint Helena','SHN',654,290),(180,'KN','SAINT KITTS AND NEVIS','Saint Kitts and Nevis','KNA',659,1869),(181,'LC','SAINT LUCIA','Saint Lucia','LCA',662,1758),(182,'PM','SAINT PIERRE AND MIQUELON','Saint Pierre and Miquelon','SPM',666,508),(183,'VC','SAINT VINCENT AND THE GRENADINES','Saint Vincent and the Grenadines','VCT',670,1784),(184,'WS','SAMOA','Samoa','WSM',882,684),(185,'SM','SAN MARINO','San Marino','SMR',674,378),(186,'ST','SAO TOME AND PRINCIPE','Sao Tome and Principe','STP',678,239),(187,'SA','SAUDI ARABIA','Saudi Arabia','SAU',682,966),(188,'SN','SENEGAL','Senegal','SEN',686,221),(189,'CS','SERBIA AND MONTENEGRO','Serbia and Montenegro',NULL,NULL,381),(190,'SC','SEYCHELLES','Seychelles','SYC',690,248),(191,'SL','SIERRA LEONE','Sierra Leone','SLE',694,232),(192,'SG','SINGAPORE','Singapore','SGP',702,65),(193,'SK','SLOVAKIA','Slovakia','SVK',703,421),(194,'SI','SLOVENIA','Slovenia','SVN',705,386),(195,'SB','SOLOMON ISLANDS','Solomon Islands','SLB',90,677),(196,'SO','SOMALIA','Somalia','SOM',706,252),(197,'ZA','SOUTH AFRICA','South Africa','ZAF',710,27),(198,'GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','South Georgia and the South Sandwich Islands',NULL,NULL,0),(199,'ES','SPAIN','Spain','ESP',724,34),(200,'LK','SRI LANKA','Sri Lanka','LKA',144,94),(201,'SD','SUDAN','Sudan','SDN',736,249),(202,'SR','SURINAME','Suriname','SUR',740,597),(203,'SJ','SVALBARD AND JAN MAYEN','Svalbard and Jan Mayen','SJM',744,47),(204,'SZ','SWAZILAND','Swaziland','SWZ',748,268),(205,'SE','SWEDEN','Sweden','SWE',752,46),(206,'CH','SWITZERLAND','Switzerland','CHE',756,41),(207,'SY','SYRIAN ARAB REPUBLIC','Syrian Arab Republic','SYR',760,963),(208,'TW','TAIWAN, PROVINCE OF CHINA','Taiwan, Province of China','TWN',158,886),(209,'TJ','TAJIKISTAN','Tajikistan','TJK',762,992),(210,'TZ','TANZANIA, UNITED REPUBLIC OF','Tanzania, United Republic of','TZA',834,255),(211,'TH','THAILAND','Thailand','THA',764,66),(212,'TL','TIMOR-LESTE','Timor-Leste',NULL,NULL,670),(213,'TG','TOGO','Togo','TGO',768,228),(214,'TK','TOKELAU','Tokelau','TKL',772,690),(215,'TO','TONGA','Tonga','TON',776,676),(216,'TT','TRINIDAD AND TOBAGO','Trinidad and Tobago','TTO',780,1868),(217,'TN','TUNISIA','Tunisia','TUN',788,216),(218,'TR','TURKEY','Turkey','TUR',792,90),(219,'TM','TURKMENISTAN','Turkmenistan','TKM',795,7370),(220,'TC','TURKS AND CAICOS ISLANDS','Turks and Caicos Islands','TCA',796,1649),(221,'TV','TUVALU','Tuvalu','TUV',798,688),(222,'UG','UGANDA','Uganda','UGA',800,256),(223,'UA','UKRAINE','Ukraine','UKR',804,380),(224,'AE','UNITED ARAB EMIRATES','United Arab Emirates','ARE',784,971),(225,'GB','UNITED KINGDOM','United Kingdom','GBR',826,44),(226,'US','UNITED STATES','United States','USA',840,1),(227,'UM','UNITED STATES MINOR OUTLYING ISLANDS','United States Minor Outlying Islands',NULL,NULL,1),(228,'UY','URUGUAY','Uruguay','URY',858,598),(229,'UZ','UZBEKISTAN','Uzbekistan','UZB',860,998),(230,'VU','VANUATU','Vanuatu','VUT',548,678),(231,'VE','VENEZUELA','Venezuela','VEN',862,58),(232,'VN','VIET NAM','Viet Nam','VNM',704,84),(233,'VG','VIRGIN ISLANDS, BRITISH','Virgin Islands, British','VGB',92,1284),(234,'VI','VIRGIN ISLANDS, U.S.','Virgin Islands, U.s.','VIR',850,1340),(235,'WF','WALLIS AND FUTUNA','Wallis and Futuna','WLF',876,681),(236,'EH','WESTERN SAHARA','Western Sahara','ESH',732,212),(237,'YE','YEMEN','Yemen','YEM',887,967),(238,'ZM','ZAMBIA','Zambia','ZMB',894,260),(239,'ZW','ZIMBABWE','Zimbabwe','ZWE',716,263);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coupons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_auto` int(11) DEFAULT NULL,
  `discount_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_price` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_price` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `max_discount` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_room` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coupons`
--

LOCK TABLES `coupons` WRITE;
/*!40000 ALTER TABLE `coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=513 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,'{}',1),(2,1,'name','text','Name',1,1,1,1,1,1,'{}',2),(3,1,'email','text','Email',1,1,1,1,1,1,'{}',3),(4,1,'password','password','Password',1,0,0,1,1,0,'{}',4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,'{}',5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,'{}',6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,'{}',8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(12,1,'settings','hidden','Settings',0,0,0,0,0,0,'{}',12),(13,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(14,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(15,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(16,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(17,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(18,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(19,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(20,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(21,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),(22,1,'role_id','text','Role',0,1,1,1,1,1,'{}',9),(23,4,'id','number','ID',1,0,0,0,0,0,NULL,1),(24,4,'parent_id','select_dropdown','Parent',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(25,4,'order','text','Order',1,1,1,1,1,1,'{\"default\":1}',3),(26,4,'name','text','Name',1,1,1,1,1,1,NULL,4),(27,4,'slug','text','Slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(28,4,'created_at','timestamp','Created At',0,0,1,0,0,0,NULL,6),(29,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(30,5,'id','number','ID',1,0,0,0,0,0,NULL,1),(31,5,'author_id','text','Author',1,0,1,1,0,1,NULL,2),(32,5,'category_id','text','Category',1,0,1,1,1,0,NULL,3),(33,5,'title','text','Title',1,1,1,1,1,1,NULL,4),(34,5,'excerpt','text_area','Excerpt',1,0,1,1,1,1,NULL,5),(35,5,'body','rich_text_box','Body',1,0,1,1,1,1,NULL,6),(36,5,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(37,5,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}',8),(38,5,'meta_description','text_area','Meta Description',1,0,1,1,1,1,NULL,9),(39,5,'meta_keywords','text_area','Meta Keywords',1,0,1,1,1,1,NULL,10),(40,5,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(41,5,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,12),(42,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,13),(43,5,'seo_title','text','SEO Title',0,1,1,1,1,1,NULL,14),(44,5,'featured','checkbox','Featured',1,1,1,1,1,1,NULL,15),(45,6,'id','number','ID',1,0,0,0,0,0,NULL,1),(46,6,'author_id','text','Author',1,0,0,0,0,0,NULL,2),(47,6,'title','text','Title',1,1,1,1,1,1,NULL,3),(48,6,'excerpt','text_area','Excerpt',1,0,1,1,1,1,NULL,4),(49,6,'body','rich_text_box','Body',1,0,1,1,1,1,NULL,5),(50,6,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}',6),(51,6,'meta_description','text','Meta Description',1,0,1,1,1,1,NULL,7),(52,6,'meta_keywords','text','Meta Keywords',1,0,1,1,1,1,NULL,8),(53,6,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(54,6,'created_at','timestamp','Created At',1,1,1,0,0,0,NULL,10),(55,6,'updated_at','timestamp','Updated At',1,0,0,0,0,0,NULL,11),(56,6,'image','image','Page Image',0,1,1,1,1,1,NULL,12),(57,8,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(58,8,'tag_type_name','text','Tag Type Name',1,1,1,1,1,1,'{}',2),(59,8,'is_active','select_dropdown','Is Active',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',3),(60,8,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',4),(61,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(62,13,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(63,13,'id_tag_type','text','Id Tag Type',1,1,1,1,1,1,'{}',3),(64,13,'name','text','Name',0,1,1,1,1,1,'{}',4),(65,13,'is_active','select_dropdown','Is Active',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',5),(66,13,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',6),(67,13,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(68,13,'tag_belongsto_tag_type_relationship','relationship','tag_types',0,1,1,1,1,1,'{\"model\":\"App\\\\TagType\",\"table\":\"tag_types\",\"type\":\"belongsTo\",\"column\":\"id_tag_type\",\"key\":\"id\",\"label\":\"tag_type_name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(69,14,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(70,14,'id_tag','text','Id Tag',1,1,1,1,1,1,'{}',3),(71,14,'name','text','Name',1,1,1,1,1,1,'{}',4),(72,14,'is_active','select_dropdown','Is Active',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',5),(73,14,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',6),(74,14,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(75,14,'sub_tag_belongsto_tag_relationship','relationship','tags',0,1,1,1,1,1,'{\"model\":\"App\\\\Tag\",\"table\":\"tags\",\"type\":\"belongsTo\",\"column\":\"id_tag\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(76,15,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(77,15,'name','text','Name',1,1,1,1,1,1,'{}',2),(78,15,'icon','text','Icon',0,1,1,1,1,1,'{}',3),(79,15,'description','text','Description',0,1,1,1,1,1,'{}',4),(80,15,'category','select_dropdown','Category',0,1,1,1,1,1,'{\"options\":{\"Property\":\"Property\",\"Room\":\"Room\"}}',5),(81,15,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',6),(82,15,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(83,16,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(84,16,'iso','text','Iso',0,1,1,1,1,1,'{}',2),(85,16,'name','text','Name',0,1,1,1,1,1,'{}',3),(86,16,'nicename','text','Nicename',0,1,1,1,1,1,'{}',4),(87,16,'iso3','text','Iso3',0,1,1,1,1,1,'{}',5),(88,16,'numcode','text','Numcode',0,1,1,1,1,1,'{}',6),(89,16,'phonecode','text','Phonecode',0,1,1,1,1,1,'{}',7),(90,17,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(91,17,'name','text','Name',0,1,1,1,1,1,'{}',2),(92,17,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(93,17,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(94,20,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(95,20,'name','text','Name',0,1,1,1,1,1,'{}',2),(96,20,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(97,20,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(98,21,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(99,21,'title','text','Title',0,1,1,1,1,1,'{}',2),(100,21,'description','text','Description',0,1,1,1,1,1,'{}',3),(101,21,'id_city','text','Id City',0,1,1,1,1,1,'{}',5),(102,21,'id_country','text','Id Country',0,1,1,1,1,1,'{}',7),(103,21,'type','text','Type',0,1,1,1,1,1,'{}',9),(104,21,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',10),(105,21,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',11),(106,21,'location_belongsto_city_relationship','relationship','cities',0,1,1,1,1,1,'{\"model\":\"App\\\\City\",\"table\":\"cities\",\"type\":\"belongsTo\",\"column\":\"id_city\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"amenities\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(107,21,'location_belongsto_country_relationship','relationship','countries',0,1,1,1,1,1,'{\"model\":\"App\\\\Country\",\"table\":\"countries\",\"type\":\"belongsTo\",\"column\":\"id_country\",\"key\":\"id\",\"label\":\"nicename\",\"pivot_table\":\"amenities\",\"pivot\":\"0\",\"taggable\":\"0\"}',6),(108,21,'location_belongsto_location_type_relationship','relationship','location_types',0,1,1,1,1,1,'{\"model\":\"App\\\\LocationType\",\"table\":\"location_types\",\"type\":\"belongsTo\",\"column\":\"type\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"amenities\",\"pivot\":\"0\",\"taggable\":\"0\"}',8),(109,22,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(110,22,'message','text','Message',1,1,1,1,1,1,'{}',2),(111,22,'for_id_user','text','For Id User',0,1,1,1,1,1,'{}',4),(112,22,'who_id_user','text','Who Id User',0,1,1,1,1,1,'{}',6),(113,22,'is_read','text','Is Read',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',7),(114,22,'is_custom','text','Is Custom',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',8),(115,22,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',9),(116,22,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',10),(117,22,'complaint_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"for_id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"amenities\",\"pivot\":\"0\",\"taggable\":\"0\"}',3),(118,22,'complaint_belongsto_user_relationship_1','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"who_id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"amenities\",\"pivot\":\"0\",\"taggable\":\"0\"}',5),(119,24,'user_id','text','User Id',1,1,1,1,1,1,'{}',1),(120,24,'role_id','text','Role Id',1,1,1,1,1,1,'{}',4),(121,24,'user_role_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"amenities\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(122,24,'user_role_belongsto_role_relationship','relationship','roles',0,1,1,1,1,1,'{\"model\":\"App\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"amenities\",\"pivot\":\"0\",\"taggable\":\"0\"}',3),(123,25,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(124,25,'id_tag','text','Id Tag',1,1,1,1,1,1,'{}',3),(125,25,'id_sub_tag','text','Id Sub Tag',1,1,1,1,1,1,'{}',5),(126,25,'id_property','text','Id Property',1,1,1,1,1,1,'{}',7),(127,25,'is_active','select_dropdown','Is Active',1,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',8),(128,25,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',9),(129,25,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',10),(130,26,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(131,26,'id_user','text','Id User',0,1,1,1,1,1,'{}',3),(132,26,'title','text','Title',0,1,1,1,1,1,'{}',4),(134,26,'house_rules','text','House Rules',0,1,1,1,1,1,'{}',7),(139,26,'is_archieve','text','Is Archieve',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',12),(140,26,'archieve_date','timestamp','Archieve Date',0,1,1,1,1,1,'{}',13),(141,26,'updated_by','text','Updated By',0,1,1,1,1,1,'{}',15),(142,26,'notes','text_area','Notes',0,1,1,1,1,1,'{}',16),(143,26,'status','text','Status',0,1,1,1,1,1,'{\"options\":{\"0\":\"Offline\",\"1\":\"Online\"}}',17),(144,26,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',18),(145,26,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',19),(146,27,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(147,27,'address1','text','Address1',1,1,1,1,1,1,'{}',2),(148,27,'address2','text','Address2',0,1,1,1,1,1,'{}',3),(149,27,'state','text','State',0,1,1,1,1,1,'{}',4),(150,27,'pincode','text','Pincode',0,1,1,1,1,1,'{}',5),(151,27,'id_location','text','Id Location',1,1,1,1,1,1,'{}',7),(152,27,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',8),(153,27,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',9),(154,27,'address_belongsto_location_relationship','relationship','locations',0,1,1,1,1,1,'{\"model\":\"App\\\\Location\",\"table\":\"locations\",\"type\":\"belongsTo\",\"column\":\"id_location\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',6),(155,26,'property_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(159,25,'property_has_sub_tag_belongsto_property_relationship','relationship','properties',0,1,1,1,1,1,'{\"model\":\"App\\\\Property\",\"table\":\"properties\",\"type\":\"belongsTo\",\"column\":\"id_property\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',6),(160,25,'property_has_sub_tag_belongsto_tag_relationship','relationship','tags',0,1,1,1,1,1,'{\"model\":\"App\\\\Tag\",\"table\":\"tags\",\"type\":\"belongsTo\",\"column\":\"id_tag\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(161,25,'property_has_sub_tag_belongsto_sub_tag_relationship','relationship','sub_tags',0,1,1,1,1,1,'{\"model\":\"App\\\\SubTag\",\"table\":\"sub_tags\",\"type\":\"belongsTo\",\"column\":\"id_sub_tag\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(162,28,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(163,28,'id_property','text','Id Property',1,1,1,1,1,1,'{}',3),(164,28,'image','multiple_images','Image',1,1,1,1,1,1,'{}',4),(165,28,'img_alt','text','Img Alt',0,1,1,1,1,1,'{}',5),(166,28,'ordernum','text','Ordernum',0,1,1,1,1,1,'{}',6),(167,28,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',7),(168,28,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(169,28,'property_image_belongsto_property_relationship','relationship','properties',0,1,1,1,1,1,'{\"model\":\"App\\\\Property\",\"table\":\"properties\",\"type\":\"belongsTo\",\"column\":\"id_property\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(170,30,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(171,30,'id_property','text','Id Property',1,1,1,1,1,1,'{}',4),(175,30,'allowed_extra_bed_count','text','Allowed Extra Bed Count',0,1,1,1,1,1,'{}',5),(177,30,'stay_type','text','Stay Type',0,1,1,1,1,1,'{}',7),(178,30,'min_stay','text','Min Stay',0,1,1,1,1,1,'{}',8),(179,30,'booking_type','text','Booking Type',0,1,1,1,1,1,'{}',9),(180,30,'cancellation_policy','text','Cancellation Policy',0,1,1,1,1,1,'{}',10),(181,30,'description','text','Description',0,1,1,1,1,1,'{}',11),(183,30,'rating_score','text','Rating Score',0,1,1,1,1,1,'{}',12),(184,30,'is_archieve','text','Is Archieve',0,1,1,1,1,1,'{}',13),(185,30,'archieve','text','Archieve',0,1,1,1,1,1,'{}',14),(188,30,'extra_child_price','text','Extra Child Price',0,1,1,1,1,1,'{}',17),(189,30,'no_of_guest','text','No Of Guest',0,1,1,1,1,1,'{}',19),(190,30,'max_child_count','text','Max Child Count',0,1,1,1,1,1,'{}',22),(191,30,'commission','text','Commission',0,1,1,1,1,1,'{}',23),(192,30,'status','text','Status',0,1,1,1,1,1,'{}',26),(193,30,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',27),(194,30,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',28),(195,30,'property_room_mapping_belongsto_property_relationship','relationship','properties',0,1,1,1,1,1,'{\"model\":\"App\\\\Property\",\"table\":\"properties\",\"type\":\"belongsTo\",\"column\":\"id_property\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',3),(196,31,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(197,31,'room_type','text','Room Type',1,1,1,1,1,1,'{}',2),(198,31,'description','text','Description',0,1,1,1,1,1,'{}',3),(199,31,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',4),(200,31,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(201,32,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(202,32,'id_room','text','Id Room',0,1,1,1,1,1,'{}',3),(203,32,'image','image','Image',0,1,1,1,1,1,'{}',4),(204,32,'order_num','text','Order Num',0,1,1,1,1,1,'{}',5),(205,32,'img_alt','text','Img Alt',0,1,1,1,1,1,'{}',6),(206,32,'is_thumbnail','text','Is Thumbnail',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',7),(207,32,'order','text','Order',0,1,1,1,1,1,'{}',8),(208,32,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',9),(209,32,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',10),(210,32,'property_room_image_belongsto_property_room_mapping_relationship','relationship','property_room_mappings',0,1,1,1,1,1,'{\"model\":\"App\\\\PropertyRoomMapping\",\"table\":\"property_room_mappings\",\"type\":\"belongsTo\",\"column\":\"id_room\",\"key\":\"id\",\"label\":\"stay_type\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(211,33,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(212,33,'meal_name','text','Meal Name',1,1,1,1,1,1,'{}',2),(213,33,'description','text','Description',0,1,1,1,1,1,'{}',3),(214,33,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',4),(215,33,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(216,35,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(217,35,'id_meal','text','Id Meal',1,1,1,1,1,1,'{}',2),(218,35,'id_room','text','Id Room',1,1,1,1,1,1,'{}',3),(219,35,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',4),(220,35,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(221,36,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(222,36,'id_meal','text','Id Meal',1,1,1,1,1,1,'{}',3),(223,36,'id_room','text','Id Room',1,1,1,1,1,1,'{}',5),(224,36,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',6),(225,36,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(226,36,'property_meal_map_belongsto_property_meal_relationship','relationship','property_meals',0,1,1,1,1,1,'{\"model\":\"App\\\\PropertyMeal\",\"table\":\"property_meals\",\"type\":\"belongsTo\",\"column\":\"id_meal\",\"key\":\"id\",\"label\":\"meal_name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(227,36,'property_meal_map_belongsto_property_room_mapping_relationship','relationship','property_room_mappings',0,1,1,1,1,1,'{\"model\":\"App\\\\PropertyRoomMapping\",\"table\":\"property_room_mappings\",\"type\":\"belongsTo\",\"column\":\"id_room\",\"key\":\"id\",\"label\":\"stay_type\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(228,37,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(229,37,'id_property','text','Id Property',1,1,1,1,1,1,'{}',3),(230,37,'id_location','text','Id Location',1,1,1,1,1,1,'{}',5),(231,37,'distance','text','Distance',1,1,1,1,1,1,'{}',6),(232,37,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',7),(233,37,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(234,37,'property_nearby_location_belongsto_property_relationship','relationship','properties',0,1,1,1,1,1,'{\"model\":\"App\\\\Property\",\"table\":\"properties\",\"type\":\"belongsTo\",\"column\":\"id_property\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(235,37,'property_nearby_location_belongsto_location_relationship','relationship','locations',0,1,1,1,1,1,'{\"model\":\"App\\\\Location\",\"table\":\"locations\",\"type\":\"belongsTo\",\"column\":\"id_location\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(236,1,'email_verified_at','timestamp','Email Verified At',0,1,1,1,1,1,'{}',6),(237,1,'auth_id','text','Auth Id',0,1,1,1,1,1,'{}',12),(238,1,'auth_provider','text','Auth Provider',0,1,1,1,1,1,'{}',13),(239,1,'country_code','text','Country Code',0,1,1,1,1,1,'{}',14),(240,1,'mobile_no','text','Mobile No',0,1,1,1,1,1,'{}',15),(241,38,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(242,38,'name','text','Name',1,1,1,1,1,1,'{}',2),(243,38,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(244,38,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(245,1,'user_belongsto_user_type_relationship','relationship','user_types',0,1,1,1,1,1,'{\"model\":\"App\\\\UserType\",\"table\":\"user_types\",\"type\":\"belongsTo\",\"column\":\"id_user_type\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',16),(246,1,'id_user_type','text','Id User Type',0,1,1,1,1,1,'{}',16),(247,39,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(248,39,'id_user','text','Id User',1,1,1,1,1,1,'{}',3),(249,39,'unique_id','text','Unique Id',0,1,1,1,1,1,'{}',4),(250,39,'first_name','text','First Name',0,1,1,1,1,1,'{}',5),(251,39,'last_name','text','Last Name',0,1,1,1,1,1,'{}',6),(252,39,'alt_email','text','Alt Email',0,1,1,1,1,1,'{}',7),(253,39,'gender','select_dropdown','Gender',0,1,1,1,1,1,'{\"options\":{\"Male\":\"Male\",\"Female\":\"Female\",\"Other\":\"Other\"}}',8),(254,39,'gender_visible','select_dropdown','Gender Visible',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',9),(255,39,'dob','text','Dob',0,1,1,1,1,1,'{}',10),(256,39,'dob_visible','select_dropdown','Dob Visible',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',11),(257,39,'alt_mobile','text','Alt Mobile',0,1,1,1,1,1,'{}',12),(258,39,'temp_mobile','text','Temp Mobile',0,1,1,1,1,1,'{}',13),(259,39,'email_verified','select_dropdown','Email Verified',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',14),(260,39,'mobile_verified','select_dropdown','Mobile Verified',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',15),(261,39,'about','text','About',0,1,1,1,1,1,'{}',16),(262,39,'education','text','Education',0,1,1,1,1,1,'{}',17),(263,39,'work','text','Work',0,1,1,1,1,1,'{}',18),(264,39,'marital_status','select_dropdown','Marital Status',0,1,1,1,1,1,'{\"options\":{\"Married\":\"Married\",\"Single\":\"Single\"}}',19),(265,39,'id_address','text','Id Address',0,1,1,1,1,1,'{}',21),(266,39,'is_archieved','select_dropdown','Is Archieved',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',22),(267,39,'archieved_date','timestamp','Archieved Date',0,1,1,1,1,1,'{}',23),(268,39,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',24),(269,39,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',25),(270,39,'user_info_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(271,39,'user_info_belongsto_address_relationship','relationship','addresses',0,1,1,1,1,1,'{\"model\":\"App\\\\Address\",\"table\":\"addresses\",\"type\":\"belongsTo\",\"column\":\"id_address\",\"key\":\"id\",\"label\":\"address1\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',20),(272,40,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(273,40,'language_name','text','Language Name',1,1,1,1,1,1,'{}',2),(274,40,'language_code','text','Language Code',1,1,1,1,1,1,'{}',3),(275,40,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',4),(276,40,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(277,41,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(278,41,'id_language','text','Id Language',1,1,1,1,1,1,'{}',3),(279,41,'id_user','text','Id User',1,1,1,1,1,1,'{}',5),(280,41,'user_language_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(281,41,'user_language_belongsto_language_relationship','relationship','languages',0,1,1,1,1,1,'{\"model\":\"App\\\\Language\",\"table\":\"languages\",\"type\":\"belongsTo\",\"column\":\"id_language\",\"key\":\"id\",\"label\":\"language_name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(282,42,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(283,42,'id_property','text','Id Property',1,1,1,1,1,1,'{}',3),(284,42,'id_language','text','Id Language',1,1,1,1,1,1,'{}',5),(285,42,'property_vendor_language_belongsto_property_relationship','relationship','properties',0,1,1,1,1,1,'{\"model\":\"App\\\\Property\",\"table\":\"properties\",\"type\":\"belongsTo\",\"column\":\"id_property\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(286,42,'property_vendor_language_belongsto_language_relationship','relationship','languages',0,1,1,1,1,1,'{\"model\":\"App\\\\Language\",\"table\":\"languages\",\"type\":\"belongsTo\",\"column\":\"id_language\",\"key\":\"id\",\"label\":\"language_name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(287,43,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(288,43,'title','text','Title',0,1,1,1,1,1,'{}',2),(289,43,'code','text','Code',0,1,1,1,1,1,'{}',3),(290,43,'is_auto','select_dropdown','Is Auto',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',4),(291,43,'discount_type','text','Discount Type',0,1,1,1,1,1,'{}',5),(292,43,'discount_price','text','Discount Price',0,1,1,1,1,1,'{}',6),(293,43,'min_price','text','Min Price',0,1,1,1,1,1,'{}',7),(294,43,'max_discount','text','Max Discount',0,1,1,1,1,1,'{}',8),(295,43,'type','text','Type',0,1,1,1,1,1,'{}',9),(296,43,'id_user','text','Id User',0,1,1,1,1,1,'{}',11),(297,43,'id_room','text','Id Room',0,1,1,1,1,1,'{}',13),(298,43,'status','select_dropdown','Status',0,1,1,1,1,1,'{\"options\":{\"0\":\"In-Active\",\"1\":\"Active\"}}',14),(299,43,'start_date','timestamp','Start Date',0,1,1,1,1,1,'{}',15),(300,43,'end_date','timestamp','End Date',0,1,1,1,1,1,'{}',16),(301,43,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',17),(302,43,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',18),(303,43,'coupon_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',10),(304,43,'coupon_belongsto_property_room_mapping_relationship','relationship','property_room_mappings',0,1,1,1,1,1,'{\"model\":\"App\\\\PropertyRoomMapping\",\"table\":\"property_room_mappings\",\"type\":\"belongsTo\",\"column\":\"id_room\",\"key\":\"id\",\"label\":\"stay_type\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',12),(305,44,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(306,44,'id_property','text','Id Property',0,1,1,1,1,1,'{}',3),(308,44,'id_amenity','text','Id Amenity',0,1,1,1,1,1,'{}',7),(309,44,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',8),(310,44,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',9),(312,44,'property_amenity_mapping_belongsto_property_relationship','relationship','properties',0,1,1,1,1,1,'{\"model\":\"App\\\\Property\",\"table\":\"properties\",\"type\":\"belongsTo\",\"column\":\"id_property\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(313,44,'property_amenity_mapping_belongsto_amenity_relationship','relationship','amenities',0,1,1,1,1,1,'{\"model\":\"App\\\\Amenity\",\"table\":\"amenities\",\"type\":\"belongsTo\",\"column\":\"id_amenity\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',6),(314,45,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(315,45,'id_property','text','Id Property',1,1,1,1,1,1,'{}',3),(316,45,'days','text','Days',0,1,1,1,1,1,'{}',4),(317,45,'percentage','text','Percentage',0,1,1,1,1,1,'{}',5),(318,45,'description','text','Description',0,1,1,1,1,1,'{}',6),(319,45,'is_default','select_dropdown','Is Default',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',7),(320,45,'policy_name','text','Policy Name',0,1,1,1,1,1,'{}',8),(321,45,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',9),(322,45,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',10),(323,45,'listing_cancellation_policy_belongsto_property_relationship','relationship','properties',0,1,1,1,1,1,'{\"model\":\"App\\\\Property\",\"table\":\"properties\",\"type\":\"belongsTo\",\"column\":\"id_property\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(324,46,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(325,46,'id_property','text','Id Property',1,1,1,1,1,1,'{}',3),(326,46,'thumb_name','text','Thumb Name',0,1,1,1,1,1,'{}',4),(327,46,'slug','text','Slug',0,1,1,1,1,1,'{}',5),(328,46,'thumb_desc','text','Thumb Desc',0,1,1,1,1,1,'{}',6),(329,46,'search_url','text','Search Url',0,1,1,1,1,1,'{}',7),(330,46,'banner_title','text','Banner Title',0,1,1,1,1,1,'{}',8),(331,46,'thumb_img','image','Thumb Img',0,1,1,1,1,1,'{}',9),(332,46,'banner_description','text','Banner Description',0,1,1,1,1,1,'{}',10),(333,46,'activity_list','text','Activity List',0,1,1,1,1,1,'{}',11),(334,46,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',12),(335,46,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',13),(336,46,'best_seller_banner_detail_belongsto_property_relationship','relationship','properties',0,1,1,1,1,1,'{\"model\":\"App\\\\Property\",\"table\":\"properties\",\"type\":\"belongsTo\",\"column\":\"id_property\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(337,47,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(338,47,'id_bestseller','text','Id Bestseller',0,1,1,1,1,1,'{}',3),(339,47,'title','text','Title',0,1,1,1,1,1,'{}',4),(340,47,'meta_tag','text','Meta Tag',0,1,1,1,1,1,'{}',5),(341,47,'meta_description','text','Meta Description',0,1,1,1,1,1,'{}',6),(342,47,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',7),(343,47,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(344,47,'best_seller_seo_data_belongsto_best_seller_banner_detail_relationship','relationship','best_seller_banner_details',0,1,1,1,1,1,'{\"model\":\"App\\\\BestSellerBannerDetail\",\"table\":\"best_seller_banner_details\",\"type\":\"belongsTo\",\"column\":\"id_bestseller\",\"key\":\"id\",\"label\":\"thumb_name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(345,48,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(346,48,'id_bestseller','text','Id Bestseller',1,1,1,1,1,1,'{}',2),(347,48,'title','text','Title',0,1,1,1,1,1,'{}',3),(348,48,'sub_title','text','Sub Title',0,1,1,1,1,1,'{}',4),(349,48,'bgimg','image','Bgimg',0,1,1,1,1,1,'{}',5),(350,48,'description','text','Description',0,1,1,1,1,1,'{}',6),(351,48,'rating','text','Rating',0,1,1,1,1,1,'{}',7),(352,48,'seperator_1','text','Seperator 1',0,1,1,1,1,1,'{}',8),(353,48,'seperator_2','text','Seperator 2',0,1,1,1,1,1,'{}',9),(354,48,'order','text','Order',0,1,1,1,1,1,'{}',10),(355,48,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',11),(356,48,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',12),(357,48,'best_seller_experience_belongsto_best_seller_banner_detail_relationship','relationship','best_seller_banner_details',0,1,1,1,1,1,'{\"model\":\"App\\\\BestSellerBannerDetail\",\"table\":\"best_seller_banner_details\",\"type\":\"belongsTo\",\"column\":\"id_bestseller\",\"key\":\"id\",\"label\":\"thumb_name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":null}',13),(358,49,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(359,49,'name','text','Name',1,1,1,1,1,1,'{}',2),(360,49,'description','text','Description',0,1,1,1,1,1,'{}',3),(361,49,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',4),(362,49,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(363,50,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(364,50,'id_user','text','Id User',1,1,1,1,1,1,'{}',3),(365,50,'id_user_interest','text','Id User Interest',1,1,1,1,1,1,'{}',5),(366,50,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',6),(367,50,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(368,50,'user_interest_mapping_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(369,50,'user_interest_mapping_belongsto_user_interest_relationship','relationship','user_interests',0,1,1,1,1,1,'{\"model\":\"App\\\\UserInterest\",\"table\":\"user_interests\",\"type\":\"belongsTo\",\"column\":\"id_user_interest\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(370,51,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(371,51,'status','text','Status',1,1,1,1,1,1,'{}',2),(372,51,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(373,51,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(374,52,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(375,52,'id_user','text','Id User',1,1,1,1,1,1,'{}',3),(376,52,'id_property','text','Id Property',1,1,1,1,1,1,'{}',5),(377,52,'check_in_date','text','Check In Date',0,1,1,1,1,1,'{}',6),(378,52,'check_out_date','text','Check Out Date',0,1,1,1,1,1,'{}',7),(379,52,'no_of_rooms','text','No Of Rooms',0,1,1,1,1,1,'{}',8),(380,52,'no_of_guests','text','No Of Guests',0,1,1,1,1,1,'{}',9),(381,52,'no_of_extra_beds','text','No Of Extra Beds',0,1,1,1,1,1,'{}',10),(382,52,'no_of_child','text','No Of Child',0,1,1,1,1,1,'{}',11),(383,52,'no_of_infant','text','No Of Infant',0,1,1,1,1,1,'{}',12),(384,52,'meal_type','text','Meal Type',0,1,1,1,1,1,'{}',13),(385,52,'bookink_type','text','Bookink Type',0,1,1,1,1,1,'{}',14),(386,52,'id_coupon','text','Id Coupon',0,1,1,1,1,1,'{}',16),(387,52,'id_status','text','Id Status',0,1,1,1,1,1,'{}',18),(388,52,'approved_at','timestamp','Approved At',0,1,1,1,1,1,'{}',19),(389,52,'complimentary_food','text','Complimentary Food',0,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',20),(390,52,'check_in_time','text','Check In Time',0,1,1,1,1,1,'{}',21),(391,52,'check_out_time','text','Check Out Time',0,1,1,1,1,1,'{}',22),(392,52,'customer_gstin','text','Customer Gstin',0,1,1,1,1,1,'{}',23),(393,52,'vendor_gstin','text','Vendor Gstin',0,1,1,1,1,1,'{}',24),(394,52,'primary_guest_name','text','Primary Guest Name',0,1,1,1,1,1,'{}',25),(395,52,'primary_customer_contact_no','text','Primary Customer Contact No',0,1,1,1,1,1,'{}',26),(396,52,'primary_guest_email_id','text','Primary Guest Email Id',0,1,1,1,1,1,'{}',27),(397,52,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',28),(398,52,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',29),(399,52,'booking_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"id_user\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(400,52,'booking_belongsto_property_relationship','relationship','properties',0,1,1,1,1,1,'{\"model\":\"App\\\\Property\",\"table\":\"properties\",\"type\":\"belongsTo\",\"column\":\"id_property\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(401,52,'booking_belongsto_booking_status_relationship','relationship','booking_statuses',0,1,1,1,1,1,'{\"model\":\"App\\\\BookingStatus\",\"table\":\"booking_statuses\",\"type\":\"belongsTo\",\"column\":\"id_status\",\"key\":\"id\",\"label\":\"status\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',17),(402,52,'booking_belongsto_coupon_relationship','relationship','coupons',0,1,1,1,1,1,'{\"model\":\"App\\\\Coupon\",\"table\":\"coupons\",\"type\":\"belongsTo\",\"column\":\"id_coupon\",\"key\":\"id\",\"label\":\"code\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',15),(403,53,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(404,53,'id_booking','text','Id Booking',1,1,1,1,1,1,'{}',3),(405,53,'room_price','text','Room Price',0,1,1,1,1,1,'{}',4),(406,53,'guest_price','text','Guest Price',0,1,1,1,1,1,'{}',5),(407,53,'extra_bed_price','text','Extra Bed Price',0,1,1,1,1,1,'{}',6),(408,53,'child_price','text','Child Price',0,1,1,1,1,1,'{}',7),(409,53,'meal_price','text','Meal Price',0,1,1,1,1,1,'{}',8),(410,53,'booking_price','text','Booking Price',0,1,1,1,1,1,'{}',9),(411,53,'funstay_commission','text','Funstay Commission',0,1,1,1,1,1,'{}',10),(412,53,'total_discount','text','Total Discount',0,1,1,1,1,1,'{}',11),(413,53,'igst','text','Igst',0,1,1,1,1,1,'{}',12),(414,53,'sgst','text','Sgst',0,1,1,1,1,1,'{}',13),(415,53,'cgst','text','Cgst',0,1,1,1,1,1,'{}',14),(416,53,'final_price','text','Final Price',1,1,1,1,1,1,'{}',15),(417,53,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',16),(418,53,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',17),(419,53,'booking_bill_belongsto_booking_relationship','relationship','bookings',0,1,1,1,1,1,'{\"model\":\"App\\\\Booking\",\"table\":\"bookings\",\"type\":\"belongsTo\",\"column\":\"id_booking\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(420,54,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(421,54,'id_booking','text','Id Booking',1,1,1,1,1,1,'{}',3),(422,54,'customer_refund_amount','text','Customer Refund Amount',0,1,1,1,1,1,'{}',4),(423,54,'booking_amount','text','Booking Amount',1,1,1,1,1,1,'{}',5),(424,54,'is_refund_to_user','text','Is Refund To User',1,1,1,1,1,1,'{\"options\":{\"0\":\"No\",\"1\":\"Yes\"}}',6),(425,54,'refunded_date','timestamp','Refunded Date',1,1,1,1,1,1,'{}',7),(426,54,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',8),(427,54,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',9),(428,54,'booking_user_refund_belongsto_booking_relationship','relationship','bookings',0,1,1,1,1,1,'{\"model\":\"App\\\\Booking\",\"table\":\"bookings\",\"type\":\"belongsTo\",\"column\":\"id_booking\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(429,55,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(430,55,'status','text','Status',1,1,1,1,1,1,'{}',2),(431,55,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(432,55,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(433,56,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(434,56,'id_booking','text','Id Booking',1,1,1,1,1,1,'{}',3),(435,56,'id_payment_status','text','Id Payment Status',0,1,1,1,1,1,'{}',4),(436,56,'payment_method','text','Payment Method',0,1,1,1,1,1,'{}',5),(437,56,'payment_description','text','Payment Description',0,1,1,1,1,1,'{}',6),(438,56,'transactionID','text','TransactionID',0,1,1,1,1,1,'{}',7),(439,56,'orderID','text','OrderID',0,1,1,1,1,1,'{}',8),(440,56,'bank_ref_no','text','Bank Ref No',0,1,1,1,1,1,'{}',9),(441,56,'order_status','text','Order Status',0,1,1,1,1,1,'{}',10),(442,56,'failure_msg','text','Failure Msg',0,1,1,1,1,1,'{}',11),(443,56,'card_name','text','Card Name',0,1,1,1,1,1,'{}',12),(444,56,'status_code','text','Status Code',0,1,1,1,1,1,'{}',13),(445,56,'status_msg','text','Status Msg',0,1,1,1,1,1,'{}',14),(446,56,'currency','text','Currency',0,1,1,1,1,1,'{}',15),(447,56,'amount','text','Amount',0,1,1,1,1,1,'{}',16),(448,56,'transaction_date','timestamp','Transaction Date',0,1,1,1,1,1,'{}',17),(449,56,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',18),(450,56,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',19),(451,56,'booking_payment_belongsto_booking_relationship','relationship','bookings',0,1,1,1,1,1,'{\"model\":\"App\\\\Booking\",\"table\":\"bookings\",\"type\":\"belongsTo\",\"column\":\"id_booking\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"addresses\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(452,57,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(453,57,'name','text','Name',1,1,1,1,1,1,'{}',2),(454,57,'description','text','Description',0,1,1,1,1,1,'{}',3),(455,57,'value','text','Value',1,1,1,1,1,1,'{}',4),(456,57,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',5),(457,57,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',6),(458,30,'title','text','Title',1,1,1,1,1,1,'{}',2),(459,26,'image','image','Image',0,1,1,1,1,1,'{}',17),(460,58,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(461,58,'name','text','Name',1,1,1,1,1,1,'{}',2),(462,58,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(463,58,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(472,26,'is_all_info','text','Is All Info',0,1,1,1,1,1,'{}',13),(473,26,'id_deleted','text','Id Deleted',0,1,1,1,1,1,'{}',14),(474,59,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(475,59,'name','text','Name',1,1,1,1,1,1,'{}',2),(476,59,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',3),(477,59,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',4),(478,60,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(479,60,'id_stay_type','text','Id Stay Type',1,1,1,1,1,1,'{}',3),(480,60,'name','text','Name',1,1,1,1,1,1,'{}',4),(481,60,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',5),(482,60,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',6),(483,60,'property_stay_sub_type_belongsto_property_stay_type_relationship','relationship','property_stay_types',0,1,1,1,1,1,'{\"model\":\"App\\\\PropertyStayType\",\"table\":\"property_stay_types\",\"type\":\"belongsTo\",\"column\":\"id_stay_type\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"activities\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(484,30,'property_room_mapping_belongsto_property_stay_type_relationship','relationship','property_stay_types',0,1,1,1,1,1,'{\"model\":\"App\\\\PropertyStayType\",\"table\":\"property_stay_types\",\"type\":\"belongsTo\",\"column\":\"stay_type\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"activities\",\"pivot\":\"0\",\"taggable\":\"0\"}',6),(485,30,'property_room_mapping_belongsto_room_type_relationship','relationship','room_types',0,1,1,1,1,1,'{\"model\":\"App\\\\RoomType\",\"table\":\"room_types\",\"type\":\"belongsTo\",\"column\":\"id_roomtype\",\"key\":\"id\",\"label\":\"room_type\",\"pivot_table\":\"activities\",\"pivot\":\"0\",\"taggable\":\"0\"}',20),(486,30,'max_stay','text','Max Stay',0,1,1,1,1,1,'{}',15),(487,30,'accommodation','text','Accommodation',0,1,1,1,1,1,'{}',16),(488,30,'quantity','text','Quantity',0,1,1,1,1,1,'{}',18),(489,30,'id_roomtype','text','Id Roomtype',0,1,1,1,1,1,'{}',21),(490,30,'property_room_mapping_belongsto_property_stay_sub_type_relationship','relationship','property_stay_sub_types',0,1,1,1,1,1,'{\"model\":\"App\\\\PropertyStaySubType\",\"table\":\"property_stay_sub_types\",\"type\":\"belongsTo\",\"column\":\"stay_sub_type\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"activities\",\"pivot\":\"0\",\"taggable\":\"0\"}',24),(491,30,'stay_sub_type','text','Stay Sub Type',0,1,1,1,1,1,'{}',25),(492,30,'property_room_mapping_belongsto_property_meal_relationship','relationship','property_meals',0,1,1,1,1,1,'{\"model\":\"App\\\\PropertyMeal\",\"table\":\"property_meals\",\"type\":\"belongsTo\",\"column\":\"id_mealplan\",\"key\":\"id\",\"label\":\"meal_name\",\"pivot_table\":\"activities\",\"pivot\":\"0\",\"taggable\":\"0\"}',29),(493,30,'price_per_person','text','Price Per Person',0,1,1,1,1,1,'{}',25),(494,30,'price_per_unit','text','Price Per Unit',0,1,1,1,1,1,'{}',26),(495,30,'weekend_price','text','Weekend Price',0,1,1,1,1,1,'{}',27),(496,30,'currency','text','Currency',0,1,1,1,1,1,'{}',28),(497,30,'id_mealplan','text','Id Mealplan',0,1,1,1,1,1,'{}',29),(498,30,'meal_price','text','Meal Price',0,1,1,1,1,1,'{}',30),(499,61,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(500,61,'id_property_room','text','Id Property Room',1,1,1,1,1,1,'{}',3),(501,61,'id_amenity','text','Id Amenity',1,1,1,1,1,1,'{}',5),(502,61,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',6),(503,61,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(504,61,'property_room_amenity_mapping_belongsto_property_room_mapping_relationship','relationship','property_room_mappings',0,1,1,1,1,1,'{\"model\":\"App\\\\PropertyRoomMapping\",\"table\":\"property_room_mappings\",\"type\":\"belongsTo\",\"column\":\"id_property_room\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"activities\",\"pivot\":\"0\",\"taggable\":\"0\"}',2),(505,61,'property_room_amenity_mapping_belongsto_amenity_relationship','relationship','amenities',0,1,1,1,1,1,'{\"model\":\"App\\\\Amenity\",\"table\":\"amenities\",\"type\":\"belongsTo\",\"column\":\"id_amenity\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"activities\",\"pivot\":\"0\",\"taggable\":\"0\"}',4),(506,62,'id','hidden','Id',1,0,0,0,0,0,'{}',1),(507,62,'id_property_room','text','Id Property Room',1,1,1,1,1,1,'{}',2),(508,62,'id_tag','text','Id Tag',1,1,1,1,1,1,'{}',3),(509,62,'id_sub_tag','text','Id Sub Tag',0,1,1,1,1,1,'{}',4),(510,62,'is_active','text','Is Active',0,1,1,1,1,1,'{}',5),(511,62,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',6),(512,62,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy',NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-05 05:11:25','2019-01-10 02:26:45'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2019-01-05 05:11:25','2019-01-05 05:11:25'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2019-01-05 05:11:25','2019-01-05 05:11:25'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,NULL,'2019-01-05 05:12:02','2019-01-05 05:12:02'),(5,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy','','',1,0,NULL,'2019-01-05 05:12:02','2019-01-05 05:12:02'),(6,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,NULL,'2019-01-05 05:12:03','2019-01-05 05:12:03'),(8,'tag_types','tag-types','Tag Type','Tag Types','voyager-ticket','App\\TagType',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"tag_type_name\",\"order_direction\":\"desc\",\"default_search_key\":\"tag_type_name\"}','2019-01-05 05:44:13','2019-01-05 05:54:26'),(13,'tags','tags','Tag','Tags','voyager-archive','App\\Tag',NULL,NULL,NULL,1,0,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-05 06:11:21','2019-01-05 06:18:58'),(14,'sub_tags','sub-tags','Sub Tag','Sub Tags','voyager-archive','App\\SubTag',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-05 06:24:07','2019-01-05 07:22:51'),(15,'amenities','amenities','Amenity','Amenities','voyager-trees','App\\Amenity',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-08 14:33:23','2019-01-08 14:43:48'),(16,'countries','countries','Country','Countries','voyager-rum','App\\Country',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-08 14:39:31','2019-01-08 14:43:37'),(17,'cities','cities','City','Cities','voyager-shop','App\\City',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-08 14:43:27','2019-01-08 14:43:27'),(20,'location_types','location-types','Location Type','Location Types','voyager-location','App\\LocationType',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-08 14:47:47','2019-01-08 14:47:47'),(21,'locations','locations','Location','Locations','voyager-location','App\\Location',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"title\",\"order_direction\":\"desc\",\"default_search_key\":\"title\"}','2019-01-08 14:51:40','2019-01-08 14:54:27'),(22,'complaints','complaints','Complaint','Complaints','voyager-frown','App\\Complaint',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"message\",\"order_direction\":\"desc\",\"default_search_key\":\"message\"}','2019-01-09 01:15:23','2019-01-09 01:28:01'),(24,'user_roles','user-roles','User Role','User Roles','voyager-helm','App\\UserRole',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-09 01:32:13','2019-01-09 01:35:48'),(25,'property_has_sub_tags','property-has-sub-tags','Property Has Sub Tag','Property Has Sub Tags','voyager-window-list','App\\PropertyHasSubTag',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-09 01:43:15','2019-01-16 05:43:22'),(26,'properties','properties','Property','Properties','voyager-home','App\\Property',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"title\",\"order_direction\":\"desc\",\"default_search_key\":\"title\"}','2019-01-09 01:54:02','2019-01-22 01:15:25'),(27,'addresses','addresses','Address','Addresses','voyager-ship','App\\Address',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"address1\",\"order_direction\":\"desc\",\"default_search_key\":\"address1\"}','2019-01-09 02:00:54','2019-01-09 02:01:57'),(28,'property_images','property-images','Property Image','Property Images','voyager-photos','App\\PropertyImage',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":\"img_alt\"}','2019-01-09 02:47:19','2019-01-16 06:17:08'),(29,'property_room_mapping','property-room-mapping','Property Room Mapping','Property Room Mappings','voyager-milestone','App\\PropertyRoomMapping',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-09 03:03:25','2019-01-09 03:03:25'),(30,'property_room_mappings','property-room-mappings','Property Room Mapping','Property Room Mappings','voyager-milestone','App\\PropertyRoomMapping',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-09 03:11:08','2019-01-28 05:14:41'),(31,'room_types','room-types','Room Type','Room Types','voyager-bolt','App\\RoomType',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"room_type\",\"order_direction\":\"desc\",\"default_search_key\":\"room_type\"}','2019-01-09 04:23:57','2019-01-09 04:23:57'),(32,'property_room_images','property-room-images','Property Room Image','Property Room Images','voyager-photo','App\\PropertyRoomImage',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 01:35:07','2019-01-10 01:43:17'),(33,'property_meals','property-meals','Property Meal','Property Meals','voyager-pizza','App\\PropertyMeal',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"meal_name\",\"order_direction\":\"asc\",\"default_search_key\":\"meal_name\"}','2019-01-10 01:48:46','2019-01-10 01:48:46'),(35,'property_meal_map','property-meal-map','Property Meal Map','Property Meal Maps','voyager-pizza','App\\PropertyMealMap',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 01:52:24','2019-01-10 01:53:41'),(36,'property_meal_maps','property-meal-maps','Property Meal Map','Property Meal Maps','voyager-pizza','App\\PropertyMealMap',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 01:55:40','2019-01-10 01:58:55'),(37,'property_nearby_locations','property-nearby-locations','Property Nearby Location','Property Nearby Locations','voyager-location','App\\PropertyNearbyLocation',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 02:01:22','2019-01-10 02:02:54'),(38,'user_types','user-types','User Type','User Types','voyager-eye','App\\UserType',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-10 02:25:33','2019-01-10 02:25:33'),(39,'user_infos','user-infos','User Info','User Infos','voyager-news','App\\UserInfo',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"first_name\",\"order_direction\":\"desc\",\"default_search_key\":\"first_name\"}','2019-01-10 02:44:37','2019-01-10 02:51:21'),(40,'languages','languages','Language','Languages','voyager-chat','App\\Language',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"language_name\",\"order_direction\":\"desc\",\"default_search_key\":\"language_name\"}','2019-01-10 02:57:02','2019-01-10 02:57:02'),(41,'user_languages','user-languages','User Language','User Languages','voyager-chat','App\\UserLanguage',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 02:57:35','2019-01-10 02:59:13'),(42,'property_vendor_languages','property-vendor-languages','Property Vendor Language','Property Vendor Languages','voyager-chat','App\\PropertyVendorLanguage',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 02:59:53','2019-01-10 03:01:16'),(43,'coupons','coupons','Coupon','Coupons','voyager-wand','App\\Coupon',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"title\",\"order_direction\":\"desc\",\"default_search_key\":\"title\"}','2019-01-10 03:09:22','2019-01-10 03:14:56'),(44,'property_amenity_mappings','property-amenity-mappings','Property Amenity Mapping','Property Amenity Mappings','voyager-trees','App\\PropertyAmenityMapping',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 03:29:35','2019-01-22 04:06:55'),(45,'listing_cancellation_policies','listing-cancellation-policies','Listing Cancellation Policy','Listing Cancellation Policies','voyager-receipt','App\\ListingCancellationPolicy',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"policy_name\",\"order_direction\":\"desc\",\"default_search_key\":\"policy_name\"}','2019-01-10 03:37:18','2019-01-10 03:38:42'),(46,'best_seller_banner_details','best-seller-banner-details','Best Seller Banner Detail','Best Seller Banner Details','voyager-bubble-hear','App\\BestSellerBannerDetail',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"thumb_name\",\"order_direction\":\"desc\",\"default_search_key\":\"thumb_name\"}','2019-01-10 03:47:19','2019-01-10 03:48:10'),(47,'best_seller_seo_datas','best-seller-seo-datas','Best Seller Seo Data','Best Seller Seo Datas','voyager-bubble-hear','App\\BestSellerSeoData',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"title\",\"order_direction\":\"desc\",\"default_search_key\":\"title\"}','2019-01-10 03:50:56','2019-01-10 03:54:52'),(48,'best_seller_experiences','best-seller-experiences','Best Seller Experience','Best Seller Experiences','voyager-bubble-hear','App\\BestSellerExperience',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"title\",\"order_direction\":\"desc\",\"default_search_key\":\"title\"}','2019-01-10 04:03:58','2019-01-10 04:03:58'),(49,'user_interests','user-interests','User Interest','User Interests','voyager-star-two','App\\UserInterest',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-10 04:08:33','2019-01-10 04:08:33'),(50,'user_interest_mappings','user-interest-mappings','User Interest Mapping','User Interest Mappings','voyager-star-two','App\\UserInterestMapping',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 04:10:59','2019-01-10 04:13:08'),(51,'booking_statuses','booking-statuses','Booking Status','Booking Statuses','voyager-check-circle','App\\BookingStatus',NULL,NULL,NULL,1,1,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-01-10 04:22:59','2019-01-10 04:22:59'),(52,'bookings','bookings','Booking','Bookings','voyager-calendar','App\\Booking',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 04:38:55','2019-01-10 04:43:25'),(53,'booking_bills','booking-bills','Booking Bill','Booking Bills','voyager-receipt','App\\BookingBill',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 05:42:22','2019-01-10 05:43:46'),(54,'booking_user_refunds','booking-user-refunds','Booking User Refund','Booking User Refunds','voyager-magnet','App\\BookingUserRefund',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 05:48:56','2019-01-10 05:50:01'),(55,'booking_payment_statuses','booking-payment-statuses','Booking Payment Status','Booking Payment Statuses','voyager-leaf','App\\BookingPaymentStatus',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"status\",\"order_direction\":\"desc\",\"default_search_key\":\"status\"}','2019-01-10 05:53:18','2019-01-10 05:53:18'),(56,'booking_payments','booking-payments','Booking Payment','Booking Payments','voyager-receipt','App\\BookingPayment',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-10 06:01:49','2019-01-10 06:03:06'),(57,'booking_taxes','booking-taxes','Booking Tax','Booking Taxes','voyager-bar-chart','App\\BookingTax',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-10 06:06:03','2019-01-10 06:06:03'),(58,'activities','activities','Activity','Activities','voyager-brush','App\\Activity',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-17 06:54:28','2019-01-17 06:54:28'),(59,'property_stay_types','property-stay-types','Property Stay Type','Property Stay Types','voyager-tree','App\\PropertyStayType',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-28 01:49:02','2019-01-28 01:49:02'),(60,'property_stay_sub_types','property-stay-sub-types','Property Stay Sub Type','Property Stay Sub Types','voyager-leaf','App\\PropertyStaySubType',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":\"name\",\"order_direction\":\"desc\",\"default_search_key\":\"name\"}','2019-01-28 01:50:41','2019-01-28 01:55:56'),(61,'property_room_amenity_mappings','property-room-amenity-mappings','Property Room Amenity Mapping','Property Room Amenity Mappings','voyager-trees','App\\PropertyRoomAmenityMapping',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null}','2019-01-28 05:41:47','2019-01-28 05:51:24'),(62,'property_room_has_sub_tags','property-room-has-sub-tags','Property Room Has Sub Tag','Property Room Has Sub Tags','voyager-company','App\\PropertyRoomHasSubTag',NULL,NULL,NULL,1,1,'{\"order_column\":\"id\",\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-01-28 06:57:45','2019-01-28 06:57:45');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'English','ENG','2019-01-22 02:16:00','2019-01-22 02:17:46'),(2,'Hindi','HINDI','2019-01-22 02:16:00','2019-01-22 02:17:55');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listing_cancellation_policies`
--

DROP TABLE IF EXISTS `listing_cancellation_policies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listing_cancellation_policies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property` int(11) NOT NULL,
  `days` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percentage` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` int(11) DEFAULT NULL,
  `policy_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listing_cancellation_policies`
--

LOCK TABLES `listing_cancellation_policies` WRITE;
/*!40000 ALTER TABLE `listing_cancellation_policies` DISABLE KEYS */;
/*!40000 ALTER TABLE `listing_cancellation_policies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location_types`
--

DROP TABLE IF EXISTS `location_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(192) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location_types`
--

LOCK TABLES `location_types` WRITE;
/*!40000 ALTER TABLE `location_types` DISABLE KEYS */;
INSERT INTO `location_types` VALUES (1,'Hotel','2019-01-16 05:34:09','2019-01-16 05:34:09');
/*!40000 ALTER TABLE `location_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(495) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `id_country` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES (1,'Test Location','Test',1,99,1,'2019-01-16 05:34:38','2019-01-16 05:34:38');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2019-01-05 05:11:25','2019-01-12 07:03:54','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,75,1,'2019-01-05 05:11:25','2019-01-28 23:37:23','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,67,2,'2019-01-05 05:11:25','2019-01-12 07:03:35','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,34,1,'2019-01-05 05:11:25','2019-01-12 07:03:47','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2019-01-05 05:11:25','2019-01-28 23:38:25',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,2,'2019-01-05 05:11:25','2019-01-28 23:37:30','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,3,'2019-01-05 05:11:25','2019-01-28 23:37:30','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,4,'2019-01-05 05:11:25','2019-01-28 23:37:30','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,5,'2019-01-05 05:11:25','2019-01-28 23:37:30','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,11,'2019-01-05 05:11:25','2019-01-28 23:38:25','voyager.settings.index',NULL),(11,1,'Hooks','','_self','voyager-hook',NULL,5,6,'2019-01-05 05:11:26','2019-01-28 23:37:30','voyager.hooks',NULL),(12,1,'Categories','','_self','voyager-categories',NULL,75,2,'2019-01-05 05:12:02','2019-01-28 23:37:23','voyager.categories.index',NULL),(14,1,'Pages','','_self','voyager-file-text',NULL,5,1,'2019-01-05 05:12:03','2019-01-28 23:37:30','voyager.pages.index',NULL),(16,1,'Tag Types','','_self',NULL,NULL,19,1,'2019-01-05 05:44:13','2019-01-05 08:02:09','voyager.tag-types.index',NULL),(17,1,'Tags','','_self',NULL,NULL,19,2,'2019-01-05 06:11:21','2019-01-05 08:02:09','voyager.tags.index',NULL),(18,1,'Sub Tags','','_self',NULL,NULL,20,1,'2019-01-05 06:24:07','2019-01-05 07:35:33','voyager.sub-tags.index',NULL),(19,1,'Tags Management','','_self','voyager-categories','#000000',NULL,5,'2019-01-05 06:28:45','2019-01-28 23:38:25',NULL,''),(21,1,'Sub Tags','','_self','voyager-dot-3','#000000',19,3,'2019-01-05 07:42:22','2019-01-05 07:42:50','voyager.sub-tags.index',NULL),(23,1,'sdsdsd','','_self',NULL,'#000000',22,1,'2019-01-05 07:49:13','2019-01-05 07:49:20',NULL,''),(24,1,'sdfdsf','','_self',NULL,'#000000',23,1,'2019-01-05 07:50:04','2019-01-05 07:50:11',NULL,''),(25,1,'Amenities','','_self','voyager-trees',NULL,75,5,'2019-01-08 14:33:23','2019-01-28 23:37:23','voyager.amenities.index',NULL),(26,1,'Countries','','_self','voyager-rum',NULL,30,1,'2019-01-08 14:39:31','2019-01-09 01:09:41','voyager.countries.index',NULL),(27,1,'Cities','','_self','voyager-shop',NULL,30,2,'2019-01-08 14:43:27','2019-01-09 01:44:38','voyager.cities.index',NULL),(28,1,'Location Types','','_self','voyager-location',NULL,30,3,'2019-01-08 14:47:47','2019-01-09 01:44:38','voyager.location-types.index',NULL),(29,1,'Locations','','_self','voyager-location',NULL,30,4,'2019-01-08 14:51:40','2019-01-09 01:45:13','voyager.locations.index',NULL),(30,1,'Locations','','_self','voyager-location','#000000',NULL,6,'2019-01-09 01:09:22','2019-01-28 23:38:25',NULL,''),(31,1,'Complaints','','_self','voyager-frown','#000000',75,3,'2019-01-09 01:15:23','2019-01-28 23:37:23','voyager.complaints.index','null'),(32,1,'User Roles','','_self','voyager-helm',NULL,34,2,'2019-01-09 01:32:13','2019-01-12 07:03:59','voyager.user-roles.index',NULL),(33,1,'Property Has Sub Tags','','_self','voyager-window-list',NULL,19,4,'2019-01-09 01:43:15','2019-01-09 01:45:14','voyager.property-has-sub-tags.index',NULL),(34,1,'Role Management','','_self','voyager-lock','#000000',NULL,2,'2019-01-09 01:44:24','2019-01-12 07:03:54',NULL,''),(35,1,'Properties','','_self','voyager-home',NULL,66,1,'2019-01-09 01:54:03','2019-01-12 06:55:51','voyager.properties.index',NULL),(36,1,'Addresses','','_self','voyager-ship',NULL,66,12,'2019-01-09 02:00:54','2019-01-28 23:36:04','voyager.addresses.index',NULL),(37,1,'Property Images','','_self','voyager-photos',NULL,66,2,'2019-01-09 02:47:19','2019-01-12 07:03:31','voyager.property-images.index',NULL),(39,1,'Property Room Mappings','','_self','voyager-milestone',NULL,66,4,'2019-01-09 03:11:08','2019-01-12 07:03:31','voyager.property-room-mappings.index',NULL),(40,1,'Room Types','','_self','voyager-bolt',NULL,66,11,'2019-01-09 04:23:57','2019-01-28 23:36:04','voyager.room-types.index',NULL),(41,1,'Property Room Images','','_self','voyager-photo',NULL,66,3,'2019-01-10 01:35:07','2019-01-12 07:03:31','voyager.property-room-images.index',NULL),(44,1,'Property Meal Maps','','_self','voyager-pizza',NULL,66,7,'2019-01-10 01:55:41','2019-01-12 07:03:31','voyager.property-meal-maps.index',NULL),(45,1,'Property Nearby Locations','','_self','voyager-location',NULL,66,6,'2019-01-10 02:01:22','2019-01-12 07:03:31','voyager.property-nearby-locations.index',NULL),(46,1,'User Types','','_self','voyager-eye',NULL,67,1,'2019-01-10 02:25:34','2019-01-12 06:59:37','voyager.user-types.index',NULL),(47,1,'User Infos','','_self','voyager-news',NULL,67,3,'2019-01-10 02:44:38','2019-01-12 07:03:35','voyager.user-infos.index',NULL),(48,1,'Languages','','_self','voyager-chat',NULL,75,6,'2019-01-10 02:57:02','2019-01-28 23:37:23','voyager.languages.index',NULL),(49,1,'User Languages','','_self','voyager-chat',NULL,67,5,'2019-01-10 02:57:35','2019-01-12 07:03:35','voyager.user-languages.index',NULL),(50,1,'Property Vendor Languages','','_self','voyager-chat',NULL,66,8,'2019-01-10 02:59:53','2019-01-12 07:03:31','voyager.property-vendor-languages.index',NULL),(52,1,'Property Amenity Mappings','','_self','voyager-trees',NULL,66,5,'2019-01-10 03:29:35','2019-01-12 07:03:31','voyager.property-amenity-mappings.index',NULL),(53,1,'Listing Cancellation Policies','','_self','voyager-receipt',NULL,75,4,'2019-01-10 03:37:19','2019-01-28 23:37:23','voyager.listing-cancellation-policies.index',NULL),(54,1,'Best Seller Banner Details','','_self','voyager-bubble-hear',NULL,68,1,'2019-01-10 03:47:19','2019-01-12 07:00:51','voyager.best-seller-banner-details.index',NULL),(55,1,'Best Seller Seo Datas','','_self','voyager-bubble-hear',NULL,68,2,'2019-01-10 03:50:56','2019-01-12 07:00:52','voyager.best-seller-seo-datas.index',NULL),(56,1,'Best Seller Experiences','','_self','voyager-bubble-hear',NULL,68,3,'2019-01-10 04:03:58','2019-01-12 07:00:54','voyager.best-seller-experiences.index',NULL),(57,1,'User Interests','','_self','voyager-star-two',NULL,67,4,'2019-01-10 04:08:33','2019-01-12 07:03:35','voyager.user-interests.index',NULL),(58,1,'User Interest Mappings','','_self','voyager-star-two',NULL,67,6,'2019-01-10 04:10:59','2019-01-12 07:03:35','voyager.user-interest-mappings.index',NULL),(59,1,'Booking Statuses','','_self','voyager-check-circle',NULL,69,6,'2019-01-10 04:22:59','2019-01-12 07:03:07','voyager.booking-statuses.index',NULL),(60,1,'Bookings','','_self','voyager-calendar',NULL,69,1,'2019-01-10 04:38:56','2019-01-12 07:02:44','voyager.bookings.index',NULL),(61,1,'Booking Bills','','_self','voyager-receipt',NULL,69,2,'2019-01-10 05:42:22','2019-01-12 07:02:54','voyager.booking-bills.index',NULL),(62,1,'Booking User Refunds','','_self','voyager-magnet',NULL,69,3,'2019-01-10 05:48:56','2019-01-12 07:03:00','voyager.booking-user-refunds.index',NULL),(63,1,'Booking Payment Statuses','','_self','voyager-leaf',NULL,69,5,'2019-01-10 05:53:18','2019-01-12 07:03:07','voyager.booking-payment-statuses.index',NULL),(64,1,'Booking Payments','','_self','voyager-receipt',NULL,69,4,'2019-01-10 06:01:49','2019-01-12 07:03:07','voyager.booking-payments.index',NULL),(65,1,'Booking Taxes','','_self','voyager-bar-chart',NULL,69,7,'2019-01-10 06:06:03','2019-01-12 07:03:09','voyager.booking-taxes.index',NULL),(66,1,'Property Management','','_self','voyager-company','#000000',NULL,4,'2019-01-12 06:54:47','2019-01-28 23:38:25',NULL,''),(67,1,'User Management','','_self','voyager-people','#000000',NULL,3,'2019-01-12 06:58:13','2019-01-12 07:07:19',NULL,''),(68,1,'Best Sellers','','_self','voyager-paper-plane','#000000',NULL,8,'2019-01-12 07:00:35','2019-01-28 23:38:25',NULL,''),(69,1,'Booking Management','','_self','voyager-credit-cards','#000000',NULL,7,'2019-01-12 07:02:30','2019-01-28 23:38:25',NULL,''),(70,1,'Activities','','_self','voyager-brush',NULL,75,7,'2019-01-17 06:54:29','2019-01-28 23:37:23','voyager.activities.index',NULL),(71,1,'Property Stay Types','','_self','voyager-tree',NULL,75,8,'2019-01-28 01:49:02','2019-01-28 23:37:23','voyager.property-stay-types.index',NULL),(72,1,'Property Stay Sub Types','','_self','voyager-leaf',NULL,75,9,'2019-01-28 01:50:41','2019-01-28 23:37:23','voyager.property-stay-sub-types.index',NULL),(73,1,'Property Room Amenity Mappings','','_self','voyager-trees',NULL,66,10,'2019-01-28 05:41:47','2019-01-28 23:36:04','voyager.property-room-amenity-mappings.index',NULL),(74,1,'Property Room Has Sub Tags','','_self','voyager-company',NULL,66,9,'2019-01-28 06:57:45','2019-01-28 23:35:53','voyager.property-room-has-sub-tags.index',NULL),(75,1,'Master Data','','_self','voyager-terminal','#000000',NULL,10,'2019-01-28 23:35:18','2019-01-28 23:38:25',NULL,'');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2019-01-05 05:11:25','2019-01-05 05:11:25');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2016_01_01_000000_create_pages_table',2),(24,'2016_01_01_000000_create_posts_table',2),(25,'2016_02_15_204651_create_categories_table',2),(26,'2017_04_11_000000_alter_post_nullable_fields_table',2);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/page1.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2019-01-05 05:12:03','2019-01-05 05:12:03');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(67,1),(68,1),(69,1),(70,1),(71,1),(72,1),(73,1),(74,1),(75,1),(76,1),(77,1),(78,1),(79,1),(80,1),(81,1),(82,1),(83,1),(84,1),(85,1),(86,1),(87,1),(88,1),(89,1),(90,1),(91,1),(92,1),(93,1),(94,1),(95,1),(96,1),(97,1),(98,1),(99,1),(100,1),(101,1),(102,1),(103,1),(104,1),(105,1),(106,1),(107,1),(108,1),(109,1),(110,1),(111,1),(112,1),(113,1),(114,1),(115,1),(116,1),(117,1),(118,1),(119,1),(120,1),(121,1),(122,1),(123,1),(124,1),(125,1),(126,1),(127,1),(128,1),(129,1),(130,1),(131,1),(132,1),(133,1),(134,1),(135,1),(136,1),(137,1),(138,1),(139,1),(140,1),(141,1),(142,1),(143,1),(144,1),(145,1),(146,1),(147,1),(148,1),(149,1),(150,1),(151,1),(152,1),(153,1),(154,1),(155,1),(156,1),(157,1),(158,1),(159,1),(160,1),(161,1),(162,1),(163,1),(164,1),(165,1),(166,1),(167,1),(168,1),(169,1),(170,1),(171,1),(172,1),(173,1),(174,1),(175,1),(176,1),(177,1),(178,1),(179,1),(180,1),(181,1),(182,1),(183,1),(184,1),(185,1),(186,1),(187,1),(188,1),(189,1),(190,1),(191,1),(192,1),(193,1),(194,1),(195,1),(196,1),(197,1),(198,1),(199,1),(200,1),(201,1),(202,1),(203,1),(204,1),(205,1),(206,1),(207,1),(208,1),(209,1),(210,1),(211,1),(212,1),(213,1),(214,1),(215,1),(216,1),(217,1),(218,1),(219,1),(220,1),(221,1),(222,1),(223,1),(224,1),(225,1),(226,1),(227,1),(228,1),(229,1),(230,1),(231,1),(232,1),(233,1),(234,1),(235,1),(236,1),(237,1),(238,1),(239,1),(240,1),(241,1),(242,1),(243,1),(244,1),(245,1),(246,1),(247,1),(248,1),(249,1),(250,1),(251,1),(252,1),(253,1),(254,1),(255,1),(256,1),(257,1),(258,1),(259,1),(260,1),(261,1),(262,1),(263,1),(264,1),(265,1),(266,1),(267,1),(268,1),(269,1),(270,1),(271,1),(272,1),(273,1),(274,1),(275,1),(276,1),(277,1),(278,1),(279,1),(280,1),(281,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=282 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2019-01-05 05:11:25','2019-01-05 05:11:25'),(2,'browse_bread',NULL,'2019-01-05 05:11:25','2019-01-05 05:11:25'),(3,'browse_database',NULL,'2019-01-05 05:11:25','2019-01-05 05:11:25'),(4,'browse_media',NULL,'2019-01-05 05:11:25','2019-01-05 05:11:25'),(5,'browse_compass',NULL,'2019-01-05 05:11:25','2019-01-05 05:11:25'),(6,'browse_menus','menus','2019-01-05 05:11:25','2019-01-05 05:11:25'),(7,'read_menus','menus','2019-01-05 05:11:25','2019-01-05 05:11:25'),(8,'edit_menus','menus','2019-01-05 05:11:25','2019-01-05 05:11:25'),(9,'add_menus','menus','2019-01-05 05:11:25','2019-01-05 05:11:25'),(10,'delete_menus','menus','2019-01-05 05:11:25','2019-01-05 05:11:25'),(11,'browse_roles','roles','2019-01-05 05:11:25','2019-01-05 05:11:25'),(12,'read_roles','roles','2019-01-05 05:11:25','2019-01-05 05:11:25'),(13,'edit_roles','roles','2019-01-05 05:11:25','2019-01-05 05:11:25'),(14,'add_roles','roles','2019-01-05 05:11:25','2019-01-05 05:11:25'),(15,'delete_roles','roles','2019-01-05 05:11:25','2019-01-05 05:11:25'),(16,'browse_users','users','2019-01-05 05:11:25','2019-01-05 05:11:25'),(17,'read_users','users','2019-01-05 05:11:25','2019-01-05 05:11:25'),(18,'edit_users','users','2019-01-05 05:11:25','2019-01-05 05:11:25'),(19,'add_users','users','2019-01-05 05:11:25','2019-01-05 05:11:25'),(20,'delete_users','users','2019-01-05 05:11:25','2019-01-05 05:11:25'),(21,'browse_settings','settings','2019-01-05 05:11:25','2019-01-05 05:11:25'),(22,'read_settings','settings','2019-01-05 05:11:25','2019-01-05 05:11:25'),(23,'edit_settings','settings','2019-01-05 05:11:25','2019-01-05 05:11:25'),(24,'add_settings','settings','2019-01-05 05:11:25','2019-01-05 05:11:25'),(25,'delete_settings','settings','2019-01-05 05:11:25','2019-01-05 05:11:25'),(26,'browse_hooks',NULL,'2019-01-05 05:11:26','2019-01-05 05:11:26'),(27,'browse_categories','categories','2019-01-05 05:12:02','2019-01-05 05:12:02'),(28,'read_categories','categories','2019-01-05 05:12:02','2019-01-05 05:12:02'),(29,'edit_categories','categories','2019-01-05 05:12:02','2019-01-05 05:12:02'),(30,'add_categories','categories','2019-01-05 05:12:02','2019-01-05 05:12:02'),(31,'delete_categories','categories','2019-01-05 05:12:02','2019-01-05 05:12:02'),(32,'browse_posts','posts','2019-01-05 05:12:03','2019-01-05 05:12:03'),(33,'read_posts','posts','2019-01-05 05:12:03','2019-01-05 05:12:03'),(34,'edit_posts','posts','2019-01-05 05:12:03','2019-01-05 05:12:03'),(35,'add_posts','posts','2019-01-05 05:12:03','2019-01-05 05:12:03'),(36,'delete_posts','posts','2019-01-05 05:12:03','2019-01-05 05:12:03'),(37,'browse_pages','pages','2019-01-05 05:12:03','2019-01-05 05:12:03'),(38,'read_pages','pages','2019-01-05 05:12:03','2019-01-05 05:12:03'),(39,'edit_pages','pages','2019-01-05 05:12:03','2019-01-05 05:12:03'),(40,'add_pages','pages','2019-01-05 05:12:03','2019-01-05 05:12:03'),(41,'delete_pages','pages','2019-01-05 05:12:03','2019-01-05 05:12:03'),(47,'browse_tag_types','tag_types','2019-01-05 05:44:13','2019-01-05 05:44:13'),(48,'read_tag_types','tag_types','2019-01-05 05:44:13','2019-01-05 05:44:13'),(49,'edit_tag_types','tag_types','2019-01-05 05:44:13','2019-01-05 05:44:13'),(50,'add_tag_types','tag_types','2019-01-05 05:44:13','2019-01-05 05:44:13'),(51,'delete_tag_types','tag_types','2019-01-05 05:44:13','2019-01-05 05:44:13'),(52,'browse_tags','tags','2019-01-05 06:11:21','2019-01-05 06:11:21'),(53,'read_tags','tags','2019-01-05 06:11:21','2019-01-05 06:11:21'),(54,'edit_tags','tags','2019-01-05 06:11:21','2019-01-05 06:11:21'),(55,'add_tags','tags','2019-01-05 06:11:21','2019-01-05 06:11:21'),(56,'delete_tags','tags','2019-01-05 06:11:21','2019-01-05 06:11:21'),(57,'browse_sub_tags','sub_tags','2019-01-05 06:24:07','2019-01-05 06:24:07'),(58,'read_sub_tags','sub_tags','2019-01-05 06:24:07','2019-01-05 06:24:07'),(59,'edit_sub_tags','sub_tags','2019-01-05 06:24:07','2019-01-05 06:24:07'),(60,'add_sub_tags','sub_tags','2019-01-05 06:24:07','2019-01-05 06:24:07'),(61,'delete_sub_tags','sub_tags','2019-01-05 06:24:07','2019-01-05 06:24:07'),(62,'browse_amenities','amenities','2019-01-08 14:33:23','2019-01-08 14:33:23'),(63,'read_amenities','amenities','2019-01-08 14:33:23','2019-01-08 14:33:23'),(64,'edit_amenities','amenities','2019-01-08 14:33:23','2019-01-08 14:33:23'),(65,'add_amenities','amenities','2019-01-08 14:33:23','2019-01-08 14:33:23'),(66,'delete_amenities','amenities','2019-01-08 14:33:23','2019-01-08 14:33:23'),(67,'browse_countries','countries','2019-01-08 14:39:31','2019-01-08 14:39:31'),(68,'read_countries','countries','2019-01-08 14:39:31','2019-01-08 14:39:31'),(69,'edit_countries','countries','2019-01-08 14:39:31','2019-01-08 14:39:31'),(70,'add_countries','countries','2019-01-08 14:39:31','2019-01-08 14:39:31'),(71,'delete_countries','countries','2019-01-08 14:39:31','2019-01-08 14:39:31'),(72,'browse_cities','cities','2019-01-08 14:43:27','2019-01-08 14:43:27'),(73,'read_cities','cities','2019-01-08 14:43:27','2019-01-08 14:43:27'),(74,'edit_cities','cities','2019-01-08 14:43:27','2019-01-08 14:43:27'),(75,'add_cities','cities','2019-01-08 14:43:27','2019-01-08 14:43:27'),(76,'delete_cities','cities','2019-01-08 14:43:27','2019-01-08 14:43:27'),(77,'browse_location_types','location_types','2019-01-08 14:47:47','2019-01-08 14:47:47'),(78,'read_location_types','location_types','2019-01-08 14:47:47','2019-01-08 14:47:47'),(79,'edit_location_types','location_types','2019-01-08 14:47:47','2019-01-08 14:47:47'),(80,'add_location_types','location_types','2019-01-08 14:47:47','2019-01-08 14:47:47'),(81,'delete_location_types','location_types','2019-01-08 14:47:47','2019-01-08 14:47:47'),(82,'browse_locations','locations','2019-01-08 14:51:40','2019-01-08 14:51:40'),(83,'read_locations','locations','2019-01-08 14:51:40','2019-01-08 14:51:40'),(84,'edit_locations','locations','2019-01-08 14:51:40','2019-01-08 14:51:40'),(85,'add_locations','locations','2019-01-08 14:51:40','2019-01-08 14:51:40'),(86,'delete_locations','locations','2019-01-08 14:51:40','2019-01-08 14:51:40'),(87,'browse_complaints','complaints','2019-01-09 01:15:23','2019-01-09 01:15:23'),(88,'read_complaints','complaints','2019-01-09 01:15:23','2019-01-09 01:15:23'),(89,'edit_complaints','complaints','2019-01-09 01:15:23','2019-01-09 01:15:23'),(90,'add_complaints','complaints','2019-01-09 01:15:23','2019-01-09 01:15:23'),(91,'delete_complaints','complaints','2019-01-09 01:15:23','2019-01-09 01:15:23'),(92,'browse_user_roles','user_roles','2019-01-09 01:32:13','2019-01-09 01:32:13'),(93,'read_user_roles','user_roles','2019-01-09 01:32:13','2019-01-09 01:32:13'),(94,'edit_user_roles','user_roles','2019-01-09 01:32:13','2019-01-09 01:32:13'),(95,'add_user_roles','user_roles','2019-01-09 01:32:13','2019-01-09 01:32:13'),(96,'delete_user_roles','user_roles','2019-01-09 01:32:13','2019-01-09 01:32:13'),(97,'browse_property_has_sub_tags','property_has_sub_tags','2019-01-09 01:43:15','2019-01-09 01:43:15'),(98,'read_property_has_sub_tags','property_has_sub_tags','2019-01-09 01:43:15','2019-01-09 01:43:15'),(99,'edit_property_has_sub_tags','property_has_sub_tags','2019-01-09 01:43:15','2019-01-09 01:43:15'),(100,'add_property_has_sub_tags','property_has_sub_tags','2019-01-09 01:43:15','2019-01-09 01:43:15'),(101,'delete_property_has_sub_tags','property_has_sub_tags','2019-01-09 01:43:15','2019-01-09 01:43:15'),(102,'browse_properties','properties','2019-01-09 01:54:02','2019-01-09 01:54:02'),(103,'read_properties','properties','2019-01-09 01:54:02','2019-01-09 01:54:02'),(104,'edit_properties','properties','2019-01-09 01:54:02','2019-01-09 01:54:02'),(105,'add_properties','properties','2019-01-09 01:54:02','2019-01-09 01:54:02'),(106,'delete_properties','properties','2019-01-09 01:54:02','2019-01-09 01:54:02'),(107,'browse_addresses','addresses','2019-01-09 02:00:54','2019-01-09 02:00:54'),(108,'read_addresses','addresses','2019-01-09 02:00:54','2019-01-09 02:00:54'),(109,'edit_addresses','addresses','2019-01-09 02:00:54','2019-01-09 02:00:54'),(110,'add_addresses','addresses','2019-01-09 02:00:54','2019-01-09 02:00:54'),(111,'delete_addresses','addresses','2019-01-09 02:00:54','2019-01-09 02:00:54'),(112,'browse_property_images','property_images','2019-01-09 02:47:19','2019-01-09 02:47:19'),(113,'read_property_images','property_images','2019-01-09 02:47:19','2019-01-09 02:47:19'),(114,'edit_property_images','property_images','2019-01-09 02:47:19','2019-01-09 02:47:19'),(115,'add_property_images','property_images','2019-01-09 02:47:19','2019-01-09 02:47:19'),(116,'delete_property_images','property_images','2019-01-09 02:47:19','2019-01-09 02:47:19'),(117,'browse_property_room_mapping','property_room_mapping','2019-01-09 03:03:25','2019-01-09 03:03:25'),(118,'read_property_room_mapping','property_room_mapping','2019-01-09 03:03:25','2019-01-09 03:03:25'),(119,'edit_property_room_mapping','property_room_mapping','2019-01-09 03:03:25','2019-01-09 03:03:25'),(120,'add_property_room_mapping','property_room_mapping','2019-01-09 03:03:25','2019-01-09 03:03:25'),(121,'delete_property_room_mapping','property_room_mapping','2019-01-09 03:03:25','2019-01-09 03:03:25'),(122,'browse_property_room_mappings','property_room_mappings','2019-01-09 03:11:08','2019-01-09 03:11:08'),(123,'read_property_room_mappings','property_room_mappings','2019-01-09 03:11:08','2019-01-09 03:11:08'),(124,'edit_property_room_mappings','property_room_mappings','2019-01-09 03:11:08','2019-01-09 03:11:08'),(125,'add_property_room_mappings','property_room_mappings','2019-01-09 03:11:08','2019-01-09 03:11:08'),(126,'delete_property_room_mappings','property_room_mappings','2019-01-09 03:11:08','2019-01-09 03:11:08'),(127,'browse_room_types','room_types','2019-01-09 04:23:57','2019-01-09 04:23:57'),(128,'read_room_types','room_types','2019-01-09 04:23:57','2019-01-09 04:23:57'),(129,'edit_room_types','room_types','2019-01-09 04:23:57','2019-01-09 04:23:57'),(130,'add_room_types','room_types','2019-01-09 04:23:57','2019-01-09 04:23:57'),(131,'delete_room_types','room_types','2019-01-09 04:23:57','2019-01-09 04:23:57'),(132,'browse_property_room_images','property_room_images','2019-01-10 01:35:07','2019-01-10 01:35:07'),(133,'read_property_room_images','property_room_images','2019-01-10 01:35:07','2019-01-10 01:35:07'),(134,'edit_property_room_images','property_room_images','2019-01-10 01:35:07','2019-01-10 01:35:07'),(135,'add_property_room_images','property_room_images','2019-01-10 01:35:07','2019-01-10 01:35:07'),(136,'delete_property_room_images','property_room_images','2019-01-10 01:35:07','2019-01-10 01:35:07'),(137,'browse_property_meals','property_meals','2019-01-10 01:48:46','2019-01-10 01:48:46'),(138,'read_property_meals','property_meals','2019-01-10 01:48:46','2019-01-10 01:48:46'),(139,'edit_property_meals','property_meals','2019-01-10 01:48:46','2019-01-10 01:48:46'),(140,'add_property_meals','property_meals','2019-01-10 01:48:46','2019-01-10 01:48:46'),(141,'delete_property_meals','property_meals','2019-01-10 01:48:46','2019-01-10 01:48:46'),(142,'browse_property_meal_map','property_meal_map','2019-01-10 01:52:24','2019-01-10 01:52:24'),(143,'read_property_meal_map','property_meal_map','2019-01-10 01:52:24','2019-01-10 01:52:24'),(144,'edit_property_meal_map','property_meal_map','2019-01-10 01:52:24','2019-01-10 01:52:24'),(145,'add_property_meal_map','property_meal_map','2019-01-10 01:52:24','2019-01-10 01:52:24'),(146,'delete_property_meal_map','property_meal_map','2019-01-10 01:52:24','2019-01-10 01:52:24'),(147,'browse_property_meal_maps','property_meal_maps','2019-01-10 01:55:40','2019-01-10 01:55:40'),(148,'read_property_meal_maps','property_meal_maps','2019-01-10 01:55:40','2019-01-10 01:55:40'),(149,'edit_property_meal_maps','property_meal_maps','2019-01-10 01:55:40','2019-01-10 01:55:40'),(150,'add_property_meal_maps','property_meal_maps','2019-01-10 01:55:40','2019-01-10 01:55:40'),(151,'delete_property_meal_maps','property_meal_maps','2019-01-10 01:55:40','2019-01-10 01:55:40'),(152,'browse_property_nearby_locations','property_nearby_locations','2019-01-10 02:01:22','2019-01-10 02:01:22'),(153,'read_property_nearby_locations','property_nearby_locations','2019-01-10 02:01:22','2019-01-10 02:01:22'),(154,'edit_property_nearby_locations','property_nearby_locations','2019-01-10 02:01:22','2019-01-10 02:01:22'),(155,'add_property_nearby_locations','property_nearby_locations','2019-01-10 02:01:22','2019-01-10 02:01:22'),(156,'delete_property_nearby_locations','property_nearby_locations','2019-01-10 02:01:22','2019-01-10 02:01:22'),(157,'browse_user_types','user_types','2019-01-10 02:25:34','2019-01-10 02:25:34'),(158,'read_user_types','user_types','2019-01-10 02:25:34','2019-01-10 02:25:34'),(159,'edit_user_types','user_types','2019-01-10 02:25:34','2019-01-10 02:25:34'),(160,'add_user_types','user_types','2019-01-10 02:25:34','2019-01-10 02:25:34'),(161,'delete_user_types','user_types','2019-01-10 02:25:34','2019-01-10 02:25:34'),(162,'browse_user_infos','user_infos','2019-01-10 02:44:38','2019-01-10 02:44:38'),(163,'read_user_infos','user_infos','2019-01-10 02:44:38','2019-01-10 02:44:38'),(164,'edit_user_infos','user_infos','2019-01-10 02:44:38','2019-01-10 02:44:38'),(165,'add_user_infos','user_infos','2019-01-10 02:44:38','2019-01-10 02:44:38'),(166,'delete_user_infos','user_infos','2019-01-10 02:44:38','2019-01-10 02:44:38'),(167,'browse_languages','languages','2019-01-10 02:57:02','2019-01-10 02:57:02'),(168,'read_languages','languages','2019-01-10 02:57:02','2019-01-10 02:57:02'),(169,'edit_languages','languages','2019-01-10 02:57:02','2019-01-10 02:57:02'),(170,'add_languages','languages','2019-01-10 02:57:02','2019-01-10 02:57:02'),(171,'delete_languages','languages','2019-01-10 02:57:02','2019-01-10 02:57:02'),(172,'browse_user_languages','user_languages','2019-01-10 02:57:35','2019-01-10 02:57:35'),(173,'read_user_languages','user_languages','2019-01-10 02:57:35','2019-01-10 02:57:35'),(174,'edit_user_languages','user_languages','2019-01-10 02:57:35','2019-01-10 02:57:35'),(175,'add_user_languages','user_languages','2019-01-10 02:57:35','2019-01-10 02:57:35'),(176,'delete_user_languages','user_languages','2019-01-10 02:57:35','2019-01-10 02:57:35'),(177,'browse_property_vendor_languages','property_vendor_languages','2019-01-10 02:59:53','2019-01-10 02:59:53'),(178,'read_property_vendor_languages','property_vendor_languages','2019-01-10 02:59:53','2019-01-10 02:59:53'),(179,'edit_property_vendor_languages','property_vendor_languages','2019-01-10 02:59:53','2019-01-10 02:59:53'),(180,'add_property_vendor_languages','property_vendor_languages','2019-01-10 02:59:53','2019-01-10 02:59:53'),(181,'delete_property_vendor_languages','property_vendor_languages','2019-01-10 02:59:53','2019-01-10 02:59:53'),(182,'browse_coupons','coupons','2019-01-10 03:09:22','2019-01-10 03:09:22'),(183,'read_coupons','coupons','2019-01-10 03:09:22','2019-01-10 03:09:22'),(184,'edit_coupons','coupons','2019-01-10 03:09:22','2019-01-10 03:09:22'),(185,'add_coupons','coupons','2019-01-10 03:09:22','2019-01-10 03:09:22'),(186,'delete_coupons','coupons','2019-01-10 03:09:22','2019-01-10 03:09:22'),(187,'browse_property_amenity_mappings','property_amenity_mappings','2019-01-10 03:29:35','2019-01-10 03:29:35'),(188,'read_property_amenity_mappings','property_amenity_mappings','2019-01-10 03:29:35','2019-01-10 03:29:35'),(189,'edit_property_amenity_mappings','property_amenity_mappings','2019-01-10 03:29:35','2019-01-10 03:29:35'),(190,'add_property_amenity_mappings','property_amenity_mappings','2019-01-10 03:29:35','2019-01-10 03:29:35'),(191,'delete_property_amenity_mappings','property_amenity_mappings','2019-01-10 03:29:35','2019-01-10 03:29:35'),(192,'browse_listing_cancellation_policies','listing_cancellation_policies','2019-01-10 03:37:19','2019-01-10 03:37:19'),(193,'read_listing_cancellation_policies','listing_cancellation_policies','2019-01-10 03:37:19','2019-01-10 03:37:19'),(194,'edit_listing_cancellation_policies','listing_cancellation_policies','2019-01-10 03:37:19','2019-01-10 03:37:19'),(195,'add_listing_cancellation_policies','listing_cancellation_policies','2019-01-10 03:37:19','2019-01-10 03:37:19'),(196,'delete_listing_cancellation_policies','listing_cancellation_policies','2019-01-10 03:37:19','2019-01-10 03:37:19'),(197,'browse_best_seller_banner_details','best_seller_banner_details','2019-01-10 03:47:19','2019-01-10 03:47:19'),(198,'read_best_seller_banner_details','best_seller_banner_details','2019-01-10 03:47:19','2019-01-10 03:47:19'),(199,'edit_best_seller_banner_details','best_seller_banner_details','2019-01-10 03:47:19','2019-01-10 03:47:19'),(200,'add_best_seller_banner_details','best_seller_banner_details','2019-01-10 03:47:19','2019-01-10 03:47:19'),(201,'delete_best_seller_banner_details','best_seller_banner_details','2019-01-10 03:47:19','2019-01-10 03:47:19'),(202,'browse_best_seller_seo_datas','best_seller_seo_datas','2019-01-10 03:50:56','2019-01-10 03:50:56'),(203,'read_best_seller_seo_datas','best_seller_seo_datas','2019-01-10 03:50:56','2019-01-10 03:50:56'),(204,'edit_best_seller_seo_datas','best_seller_seo_datas','2019-01-10 03:50:56','2019-01-10 03:50:56'),(205,'add_best_seller_seo_datas','best_seller_seo_datas','2019-01-10 03:50:56','2019-01-10 03:50:56'),(206,'delete_best_seller_seo_datas','best_seller_seo_datas','2019-01-10 03:50:56','2019-01-10 03:50:56'),(207,'browse_best_seller_experiences','best_seller_experiences','2019-01-10 04:03:58','2019-01-10 04:03:58'),(208,'read_best_seller_experiences','best_seller_experiences','2019-01-10 04:03:58','2019-01-10 04:03:58'),(209,'edit_best_seller_experiences','best_seller_experiences','2019-01-10 04:03:58','2019-01-10 04:03:58'),(210,'add_best_seller_experiences','best_seller_experiences','2019-01-10 04:03:58','2019-01-10 04:03:58'),(211,'delete_best_seller_experiences','best_seller_experiences','2019-01-10 04:03:58','2019-01-10 04:03:58'),(212,'browse_user_interests','user_interests','2019-01-10 04:08:33','2019-01-10 04:08:33'),(213,'read_user_interests','user_interests','2019-01-10 04:08:33','2019-01-10 04:08:33'),(214,'edit_user_interests','user_interests','2019-01-10 04:08:33','2019-01-10 04:08:33'),(215,'add_user_interests','user_interests','2019-01-10 04:08:33','2019-01-10 04:08:33'),(216,'delete_user_interests','user_interests','2019-01-10 04:08:33','2019-01-10 04:08:33'),(217,'browse_user_interest_mappings','user_interest_mappings','2019-01-10 04:10:59','2019-01-10 04:10:59'),(218,'read_user_interest_mappings','user_interest_mappings','2019-01-10 04:10:59','2019-01-10 04:10:59'),(219,'edit_user_interest_mappings','user_interest_mappings','2019-01-10 04:10:59','2019-01-10 04:10:59'),(220,'add_user_interest_mappings','user_interest_mappings','2019-01-10 04:10:59','2019-01-10 04:10:59'),(221,'delete_user_interest_mappings','user_interest_mappings','2019-01-10 04:10:59','2019-01-10 04:10:59'),(222,'browse_booking_statuses','booking_statuses','2019-01-10 04:22:59','2019-01-10 04:22:59'),(223,'read_booking_statuses','booking_statuses','2019-01-10 04:22:59','2019-01-10 04:22:59'),(224,'edit_booking_statuses','booking_statuses','2019-01-10 04:22:59','2019-01-10 04:22:59'),(225,'add_booking_statuses','booking_statuses','2019-01-10 04:22:59','2019-01-10 04:22:59'),(226,'delete_booking_statuses','booking_statuses','2019-01-10 04:22:59','2019-01-10 04:22:59'),(227,'browse_bookings','bookings','2019-01-10 04:38:56','2019-01-10 04:38:56'),(228,'read_bookings','bookings','2019-01-10 04:38:56','2019-01-10 04:38:56'),(229,'edit_bookings','bookings','2019-01-10 04:38:56','2019-01-10 04:38:56'),(230,'add_bookings','bookings','2019-01-10 04:38:56','2019-01-10 04:38:56'),(231,'delete_bookings','bookings','2019-01-10 04:38:56','2019-01-10 04:38:56'),(232,'browse_booking_bills','booking_bills','2019-01-10 05:42:22','2019-01-10 05:42:22'),(233,'read_booking_bills','booking_bills','2019-01-10 05:42:22','2019-01-10 05:42:22'),(234,'edit_booking_bills','booking_bills','2019-01-10 05:42:22','2019-01-10 05:42:22'),(235,'add_booking_bills','booking_bills','2019-01-10 05:42:22','2019-01-10 05:42:22'),(236,'delete_booking_bills','booking_bills','2019-01-10 05:42:22','2019-01-10 05:42:22'),(237,'browse_booking_user_refunds','booking_user_refunds','2019-01-10 05:48:56','2019-01-10 05:48:56'),(238,'read_booking_user_refunds','booking_user_refunds','2019-01-10 05:48:56','2019-01-10 05:48:56'),(239,'edit_booking_user_refunds','booking_user_refunds','2019-01-10 05:48:56','2019-01-10 05:48:56'),(240,'add_booking_user_refunds','booking_user_refunds','2019-01-10 05:48:56','2019-01-10 05:48:56'),(241,'delete_booking_user_refunds','booking_user_refunds','2019-01-10 05:48:56','2019-01-10 05:48:56'),(242,'browse_booking_payment_statuses','booking_payment_statuses','2019-01-10 05:53:18','2019-01-10 05:53:18'),(243,'read_booking_payment_statuses','booking_payment_statuses','2019-01-10 05:53:18','2019-01-10 05:53:18'),(244,'edit_booking_payment_statuses','booking_payment_statuses','2019-01-10 05:53:18','2019-01-10 05:53:18'),(245,'add_booking_payment_statuses','booking_payment_statuses','2019-01-10 05:53:18','2019-01-10 05:53:18'),(246,'delete_booking_payment_statuses','booking_payment_statuses','2019-01-10 05:53:18','2019-01-10 05:53:18'),(247,'browse_booking_payments','booking_payments','2019-01-10 06:01:49','2019-01-10 06:01:49'),(248,'read_booking_payments','booking_payments','2019-01-10 06:01:49','2019-01-10 06:01:49'),(249,'edit_booking_payments','booking_payments','2019-01-10 06:01:49','2019-01-10 06:01:49'),(250,'add_booking_payments','booking_payments','2019-01-10 06:01:49','2019-01-10 06:01:49'),(251,'delete_booking_payments','booking_payments','2019-01-10 06:01:49','2019-01-10 06:01:49'),(252,'browse_booking_taxes','booking_taxes','2019-01-10 06:06:03','2019-01-10 06:06:03'),(253,'read_booking_taxes','booking_taxes','2019-01-10 06:06:03','2019-01-10 06:06:03'),(254,'edit_booking_taxes','booking_taxes','2019-01-10 06:06:03','2019-01-10 06:06:03'),(255,'add_booking_taxes','booking_taxes','2019-01-10 06:06:03','2019-01-10 06:06:03'),(256,'delete_booking_taxes','booking_taxes','2019-01-10 06:06:03','2019-01-10 06:06:03'),(257,'browse_activities','activities','2019-01-17 06:54:29','2019-01-17 06:54:29'),(258,'read_activities','activities','2019-01-17 06:54:29','2019-01-17 06:54:29'),(259,'edit_activities','activities','2019-01-17 06:54:29','2019-01-17 06:54:29'),(260,'add_activities','activities','2019-01-17 06:54:29','2019-01-17 06:54:29'),(261,'delete_activities','activities','2019-01-17 06:54:29','2019-01-17 06:54:29'),(262,'browse_property_stay_types','property_stay_types','2019-01-28 01:49:02','2019-01-28 01:49:02'),(263,'read_property_stay_types','property_stay_types','2019-01-28 01:49:02','2019-01-28 01:49:02'),(264,'edit_property_stay_types','property_stay_types','2019-01-28 01:49:02','2019-01-28 01:49:02'),(265,'add_property_stay_types','property_stay_types','2019-01-28 01:49:02','2019-01-28 01:49:02'),(266,'delete_property_stay_types','property_stay_types','2019-01-28 01:49:02','2019-01-28 01:49:02'),(267,'browse_property_stay_sub_types','property_stay_sub_types','2019-01-28 01:50:41','2019-01-28 01:50:41'),(268,'read_property_stay_sub_types','property_stay_sub_types','2019-01-28 01:50:41','2019-01-28 01:50:41'),(269,'edit_property_stay_sub_types','property_stay_sub_types','2019-01-28 01:50:41','2019-01-28 01:50:41'),(270,'add_property_stay_sub_types','property_stay_sub_types','2019-01-28 01:50:41','2019-01-28 01:50:41'),(271,'delete_property_stay_sub_types','property_stay_sub_types','2019-01-28 01:50:41','2019-01-28 01:50:41'),(272,'browse_property_room_amenity_mappings','property_room_amenity_mappings','2019-01-28 05:41:47','2019-01-28 05:41:47'),(273,'read_property_room_amenity_mappings','property_room_amenity_mappings','2019-01-28 05:41:47','2019-01-28 05:41:47'),(274,'edit_property_room_amenity_mappings','property_room_amenity_mappings','2019-01-28 05:41:47','2019-01-28 05:41:47'),(275,'add_property_room_amenity_mappings','property_room_amenity_mappings','2019-01-28 05:41:47','2019-01-28 05:41:47'),(276,'delete_property_room_amenity_mappings','property_room_amenity_mappings','2019-01-28 05:41:47','2019-01-28 05:41:47'),(277,'browse_property_room_has_sub_tags','property_room_has_sub_tags','2019-01-28 06:57:45','2019-01-28 06:57:45'),(278,'read_property_room_has_sub_tags','property_room_has_sub_tags','2019-01-28 06:57:45','2019-01-28 06:57:45'),(279,'edit_property_room_has_sub_tags','property_room_has_sub_tags','2019-01-28 06:57:45','2019-01-28 06:57:45'),(280,'add_property_room_has_sub_tags','property_room_has_sub_tags','2019-01-28 06:57:45','2019-01-28 06:57:45'),(281,'delete_property_room_has_sub_tags','property_room_has_sub_tags','2019-01-28 06:57:45','2019-01-28 06:57:45');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,0,NULL,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/post1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-01-05 05:12:03','2019-01-05 05:12:03'),(2,0,NULL,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>','posts/post2.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-01-05 05:12:03','2019-01-05 05:12:03'),(3,0,NULL,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/post3.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-01-05 05:12:03','2019-01-05 05:12:03'),(4,0,NULL,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/post4.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2019-01-05 05:12:03','2019-01-05 05:12:03');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties`
--

DROP TABLE IF EXISTS `properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `house_rules` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_archieve` int(11) DEFAULT NULL,
  `archieve_date` timestamp NULL DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `notes` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_all_info` int(2) DEFAULT NULL,
  `is_deleted` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties`
--

LOCK TABLES `properties` WRITE;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` VALUES (1,2,'Test Property Edit','Test Rules',NULL,NULL,NULL,'test notes',NULL,'2019-01-16 05:37:38','2019-01-25 01:54:51',NULL,NULL,'N'),(2,NULL,'asd',NULL,NULL,NULL,NULL,'asd',NULL,'2019-01-21 02:24:43','2019-01-21 02:24:43',NULL,1,'N'),(3,NULL,'asd',NULL,NULL,NULL,NULL,'asd',NULL,'2019-01-21 02:29:49','2019-01-21 02:29:49',NULL,1,'N'),(4,2,'this is breaking',NULL,NULL,NULL,NULL,'this is desc.',0,'2019-01-22 01:41:07','2019-01-22 01:41:07',NULL,1,'N'),(5,2,'this is breaking asa','Test Rules',NULL,NULL,NULL,'asd as',1,'2019-01-22 01:41:59','2019-01-25 07:16:48',NULL,10,'N'),(6,2,'this is breaking asas',NULL,NULL,NULL,NULL,'asas',0,'2019-01-22 01:44:25','2019-01-29 04:56:24',NULL,4,'N'),(7,2,'this is breaking 7',NULL,NULL,NULL,NULL,'asdasda',1,'2019-01-22 01:48:48','2019-01-25 05:21:08',NULL,9,'Y');
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_amenity_mappings`
--

DROP TABLE IF EXISTS `property_amenity_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_amenity_mappings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property` int(11) DEFAULT NULL,
  `id_amenity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_amenity_mappings`
--

LOCK TABLES `property_amenity_mappings` WRITE;
/*!40000 ALTER TABLE `property_amenity_mappings` DISABLE KEYS */;
INSERT INTO `property_amenity_mappings` VALUES (1,7,1,'2019-01-16 05:41:57','2019-01-16 05:41:57'),(2,5,1,'2019-01-25 06:30:33','2019-01-25 06:30:33');
/*!40000 ALTER TABLE `property_amenity_mappings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_has_sub_tags`
--

DROP TABLE IF EXISTS `property_has_sub_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_has_sub_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tag` int(11) DEFAULT NULL,
  `id_sub_tag` int(11) DEFAULT NULL,
  `id_property` int(11) NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_has_sub_tags`
--

LOCK TABLES `property_has_sub_tags` WRITE;
/*!40000 ALTER TABLE `property_has_sub_tags` DISABLE KEYS */;
INSERT INTO `property_has_sub_tags` VALUES (3,2,NULL,5,1,'2019-01-25 06:38:28','2019-01-25 06:38:28'),(6,3,NULL,6,1,'2019-01-29 04:56:24','2019-01-29 04:56:24'),(7,NULL,1,6,1,'2019-01-29 04:56:24','2019-01-29 04:56:24');
/*!40000 ALTER TABLE `property_has_sub_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_images`
--

DROP TABLE IF EXISTS `property_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property` int(11) NOT NULL,
  `image` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_alt` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordernum` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_cover` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_images`
--

LOCK TABLES `property_images` WRITE;
/*!40000 ALTER TABLE `property_images` DISABLE KEYS */;
INSERT INTO `property_images` VALUES (1,7,'3rZnwxHW','Test',NULL,'2019-01-25 05:21:08','2019-01-25 05:21:08',NULL),(4,5,'fnZlYQvejpg','Test alt img',NULL,'2019-01-25 07:16:21','2019-01-25 07:16:21',NULL),(5,5,'kOhdoIRbjpg','Test alt img',NULL,'2019-01-25 07:16:48','2019-01-25 07:16:48',NULL),(6,5,'KcRusNnVjpg','Test alt img',NULL,'2019-01-25 07:16:48','2019-01-25 07:16:48',NULL),(7,5,'8eYK4E75jpg','Test alt img',NULL,'2019-01-25 07:16:48','2019-01-25 07:16:48',NULL),(8,6,'zT2VQfc8jpg','Test','1','2019-01-28 23:58:56','2019-01-29 01:55:23','N'),(9,6,'ehHgCmZKjpg','Test','2','2019-01-28 23:58:56','2019-01-29 01:55:26','Y'),(10,6,'AGvcNQFgjpg','Test alt img',NULL,'2019-01-29 00:34:55','2019-01-29 01:36:58','N');
/*!40000 ALTER TABLE `property_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_meal_maps`
--

DROP TABLE IF EXISTS `property_meal_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_meal_maps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_meal` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_meal_maps`
--

LOCK TABLES `property_meal_maps` WRITE;
/*!40000 ALTER TABLE `property_meal_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_meal_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_meals`
--

DROP TABLE IF EXISTS `property_meals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_meals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meal_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_meals`
--

LOCK TABLES `property_meals` WRITE;
/*!40000 ALTER TABLE `property_meals` DISABLE KEYS */;
INSERT INTO `property_meals` VALUES (1,'BreakFast',NULL,'2019-01-28 04:58:55','2019-01-28 04:58:55'),(2,'BreakFast+Meals',NULL,'2019-01-28 04:59:11','2019-01-28 04:59:11'),(3,'Dinner',NULL,'2019-01-28 04:59:21','2019-01-28 04:59:21'),(4,'BreakFast+Meals+Dinner',NULL,'2019-01-28 04:59:46','2019-01-28 04:59:46');
/*!40000 ALTER TABLE `property_meals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_nearby_locations`
--

DROP TABLE IF EXISTS `property_nearby_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_nearby_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property` int(11) NOT NULL,
  `id_location` int(11) NOT NULL,
  `distance` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_nearby_locations`
--

LOCK TABLES `property_nearby_locations` WRITE;
/*!40000 ALTER TABLE `property_nearby_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_nearby_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_room_amenity_mappings`
--

DROP TABLE IF EXISTS `property_room_amenity_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_room_amenity_mappings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property_room` int(11) NOT NULL,
  `id_amenity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_room_amenity_mappings`
--

LOCK TABLES `property_room_amenity_mappings` WRITE;
/*!40000 ALTER TABLE `property_room_amenity_mappings` DISABLE KEYS */;
INSERT INTO `property_room_amenity_mappings` VALUES (1,1,1,'2019-01-28 06:51:22','2019-01-28 06:51:22');
/*!40000 ALTER TABLE `property_room_amenity_mappings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_room_has_sub_tags`
--

DROP TABLE IF EXISTS `property_room_has_sub_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_room_has_sub_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property_room` int(11) DEFAULT NULL,
  `id_tag` int(11) DEFAULT NULL,
  `id_sub_tag` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_room_has_sub_tags`
--

LOCK TABLES `property_room_has_sub_tags` WRITE;
/*!40000 ALTER TABLE `property_room_has_sub_tags` DISABLE KEYS */;
INSERT INTO `property_room_has_sub_tags` VALUES (9,1,1,NULL,NULL,'2019-01-29 07:32:24','2019-01-29 07:32:24'),(10,1,4,NULL,NULL,'2019-01-29 07:32:24','2019-01-29 07:32:24'),(11,6,NULL,2,NULL,'2019-01-29 07:32:24','2019-01-29 07:32:24');
/*!40000 ALTER TABLE `property_room_has_sub_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_room_images`
--

DROP TABLE IF EXISTS `property_room_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_room_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property_room` int(11) DEFAULT NULL,
  `image` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordernum` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_alt` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_cover` int(11) DEFAULT NULL,
  `order` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_room_images`
--

LOCK TABLES `property_room_images` WRITE;
/*!40000 ALTER TABLE `property_room_images` DISABLE KEYS */;
INSERT INTO `property_room_images` VALUES (2,1,'wyBsWuv8jpg','1','Test alt img Room',NULL,NULL,'2019-01-28 06:51:49','2019-01-29 06:54:47');
/*!40000 ALTER TABLE `property_room_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_room_mappings`
--

DROP TABLE IF EXISTS `property_room_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_room_mappings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property` int(11) NOT NULL,
  `allowed_extra_bed_count` int(11) DEFAULT NULL,
  `stay_type` int(11) DEFAULT NULL,
  `min_stay` int(11) DEFAULT NULL,
  `booking_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancellation_policy` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating_score` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_archieve` int(11) DEFAULT NULL,
  `archieve` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extra_child_price` int(11) DEFAULT NULL,
  `no_of_guest` int(11) DEFAULT NULL,
  `max_child_count` int(11) DEFAULT NULL,
  `commission` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_stay` int(11) DEFAULT NULL,
  `accommodation` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `id_roomtype` int(11) DEFAULT NULL,
  `stay_sub_type` int(11) DEFAULT NULL,
  `price_per_person` int(11) DEFAULT NULL,
  `price_per_unit` int(11) DEFAULT NULL,
  `weekend_price` int(11) DEFAULT NULL,
  `currency` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_mealplan` int(11) DEFAULT NULL,
  `meal_price` int(11) DEFAULT NULL,
  `is_deleted` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_room_mappings`
--

LOCK TABLES `property_room_mappings` WRITE;
/*!40000 ALTER TABLE `property_room_mappings` DISABLE KEYS */;
INSERT INTO `property_room_mappings` VALUES (1,6,1,3,1,NULL,NULL,'test descr',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'2019-01-28 06:49:01','2019-01-28 06:50:10','this is breaking 7 Room',3,2,2,3,1,300,2000,9000,'INR',4,100,'N');
/*!40000 ALTER TABLE `property_room_mappings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_stay_sub_types`
--

DROP TABLE IF EXISTS `property_stay_sub_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_stay_sub_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_stay_type` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_stay_sub_types`
--

LOCK TABLES `property_stay_sub_types` WRITE;
/*!40000 ALTER TABLE `property_stay_sub_types` DISABLE KEYS */;
INSERT INTO `property_stay_sub_types` VALUES (1,1,'Super Delux','2019-01-28 01:56:29','2019-01-28 01:56:29');
/*!40000 ALTER TABLE `property_stay_sub_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_stay_types`
--

DROP TABLE IF EXISTS `property_stay_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_stay_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_stay_types`
--

LOCK TABLES `property_stay_types` WRITE;
/*!40000 ALTER TABLE `property_stay_types` DISABLE KEYS */;
INSERT INTO `property_stay_types` VALUES (1,'Delux Room','2019-01-28 01:53:39','2019-01-28 01:53:39'),(2,'Cottage','2019-01-28 01:53:46','2019-01-28 01:53:46'),(3,'Standard Room','2019-01-28 01:53:59','2019-01-28 01:53:59');
/*!40000 ALTER TABLE `property_stay_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_vendor_languages`
--

DROP TABLE IF EXISTS `property_vendor_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_vendor_languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_property` int(11) NOT NULL,
  `id_language` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_vendor_languages`
--

LOCK TABLES `property_vendor_languages` WRITE;
/*!40000 ALTER TABLE `property_vendor_languages` DISABLE KEYS */;
INSERT INTO `property_vendor_languages` VALUES (1,7,1,'2019-01-22 03:51:40','2019-01-22 03:51:40'),(2,7,2,'2019-01-22 03:51:40','2019-01-22 03:51:40'),(3,5,2,'2019-01-25 06:30:24','2019-01-25 06:30:24');
/*!40000 ALTER TABLE `property_vendor_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2019-01-05 05:11:25','2019-01-05 05:11:25'),(2,'user','Normal User','2019-01-05 05:11:25','2019-01-05 05:11:25');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_types`
--

DROP TABLE IF EXISTS `room_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_types`
--

LOCK TABLES `room_types` WRITE;
/*!40000 ALTER TABLE `room_types` DISABLE KEYS */;
INSERT INTO `room_types` VALUES (1,'Private',NULL,'2019-01-28 02:05:26','2019-01-28 02:05:26'),(2,'Shared',NULL,'2019-01-28 02:05:36','2019-01-28 02:05:36'),(3,'Entire Property',NULL,'2019-01-28 02:05:46','2019-01-28 02:05:46');
/*!40000 ALTER TABLE `room_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Funstay Admin','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','settings/January2019/Qxk574nkW7YbIo34awJ0.png','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','Voyager','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)',NULL,'','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_tags`
--

DROP TABLE IF EXISTS `sub_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tag` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_tags`
--

LOCK TABLES `sub_tags` WRITE;
/*!40000 ALTER TABLE `sub_tags` DISABLE KEYS */;
INSERT INTO `sub_tags` VALUES (1,1,'Snow',0,'2019-01-05 06:45:26','2019-01-05 06:45:26'),(2,1,'Hill',1,'2019-01-29 03:49:14','2019-01-29 03:49:14'),(3,2,'Hotel',1,'2019-01-29 03:49:24','2019-01-29 03:49:24');
/*!40000 ALTER TABLE `sub_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_types`
--

DROP TABLE IF EXISTS `tag_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_type_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_types`
--

LOCK TABLES `tag_types` WRITE;
/*!40000 ALTER TABLE `tag_types` DISABLE KEYS */;
INSERT INTO `tag_types` VALUES (1,'Mountain',1,'2019-01-05 05:51:00','2019-01-05 05:52:44'),(2,'Hill Station',1,'2019-01-05 05:53:19','2019-01-05 05:53:19');
/*!40000 ALTER TABLE `tag_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_tag_type` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,1,'Himalaya',0,'2019-01-05 06:19:00','2019-01-05 06:19:41'),(2,2,'Ooty',1,'2019-01-05 06:20:00','2019-01-05 06:20:00'),(3,1,'Hill',1,'2019-01-29 03:51:04','2019-01-29 03:51:04'),(4,1,'Himalaya Mt.',1,'2019-01-29 03:51:00','2019-01-29 03:54:02');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',5,'pt','Post','2019-01-05 05:12:03','2019-01-05 05:12:03'),(2,'data_types','display_name_singular',6,'pt','Página','2019-01-05 05:12:03','2019-01-05 05:12:03'),(3,'data_types','display_name_singular',1,'pt','Utilizador','2019-01-05 05:12:03','2019-01-05 05:12:03'),(4,'data_types','display_name_singular',4,'pt','Categoria','2019-01-05 05:12:03','2019-01-05 05:12:03'),(5,'data_types','display_name_singular',2,'pt','Menu','2019-01-05 05:12:03','2019-01-05 05:12:03'),(6,'data_types','display_name_singular',3,'pt','Função','2019-01-05 05:12:03','2019-01-05 05:12:03'),(7,'data_types','display_name_plural',5,'pt','Posts','2019-01-05 05:12:03','2019-01-05 05:12:03'),(8,'data_types','display_name_plural',6,'pt','Páginas','2019-01-05 05:12:03','2019-01-05 05:12:03'),(9,'data_types','display_name_plural',1,'pt','Utilizadores','2019-01-05 05:12:03','2019-01-05 05:12:03'),(10,'data_types','display_name_plural',4,'pt','Categorias','2019-01-05 05:12:03','2019-01-05 05:12:03'),(11,'data_types','display_name_plural',2,'pt','Menus','2019-01-05 05:12:03','2019-01-05 05:12:03'),(12,'data_types','display_name_plural',3,'pt','Funções','2019-01-05 05:12:03','2019-01-05 05:12:03'),(13,'categories','slug',1,'pt','categoria-1','2019-01-05 05:12:03','2019-01-05 05:12:03'),(14,'categories','name',1,'pt','Categoria 1','2019-01-05 05:12:03','2019-01-05 05:12:03'),(15,'categories','slug',2,'pt','categoria-2','2019-01-05 05:12:03','2019-01-05 05:12:03'),(16,'categories','name',2,'pt','Categoria 2','2019-01-05 05:12:03','2019-01-05 05:12:03'),(17,'pages','title',1,'pt','Olá Mundo','2019-01-05 05:12:03','2019-01-05 05:12:03'),(18,'pages','slug',1,'pt','ola-mundo','2019-01-05 05:12:03','2019-01-05 05:12:03'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2019-01-05 05:12:03','2019-01-05 05:12:03'),(20,'menu_items','title',1,'pt','Painel de Controle','2019-01-05 05:12:03','2019-01-05 05:12:03'),(21,'menu_items','title',2,'pt','Media','2019-01-05 05:12:03','2019-01-05 05:12:03'),(22,'menu_items','title',13,'pt','Publicações','2019-01-05 05:12:03','2019-01-05 05:12:03'),(23,'menu_items','title',3,'pt','Utilizadores','2019-01-05 05:12:03','2019-01-05 05:12:03'),(24,'menu_items','title',12,'pt','Categorias','2019-01-05 05:12:03','2019-01-05 05:12:03'),(25,'menu_items','title',14,'pt','Páginas','2019-01-05 05:12:03','2019-01-05 05:12:03'),(26,'menu_items','title',4,'pt','Funções','2019-01-05 05:12:03','2019-01-05 05:12:03'),(27,'menu_items','title',5,'pt','Ferramentas','2019-01-05 05:12:03','2019-01-05 05:12:03'),(28,'menu_items','title',6,'pt','Menus','2019-01-05 05:12:03','2019-01-05 05:12:03'),(29,'menu_items','title',7,'pt','Base de dados','2019-01-05 05:12:03','2019-01-05 05:12:03'),(30,'menu_items','title',10,'pt','Configurações','2019-01-05 05:12:03','2019-01-05 05:12:03');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_infos`
--

DROP TABLE IF EXISTS `user_infos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_infos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `unique_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt_email` int(11) DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender_visible` int(11) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `dob_visible` int(11) DEFAULT NULL,
  `alt_mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `temp_mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified` int(11) DEFAULT NULL,
  `mobile_verified` int(11) DEFAULT NULL,
  `about` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_address` int(11) DEFAULT NULL,
  `is_archieved` int(11) DEFAULT NULL,
  `archieved_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_infos`
--

LOCK TABLES `user_infos` WRITE;
/*!40000 ALTER TABLE `user_infos` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_infos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_interest_mappings`
--

DROP TABLE IF EXISTS `user_interest_mappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_interest_mappings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_user_interest` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_interest_mappings`
--

LOCK TABLES `user_interest_mappings` WRITE;
/*!40000 ALTER TABLE `user_interest_mappings` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_interest_mappings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_interests`
--

DROP TABLE IF EXISTS `user_interests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_interests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_interests`
--

LOCK TABLES `user_interests` WRITE;
/*!40000 ALTER TABLE `user_interests` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_interests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_languages`
--

DROP TABLE IF EXISTS `user_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_language` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_languages`
--

LOCK TABLES `user_languages` WRITE;
/*!40000 ALTER TABLE `user_languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_types`
--

LOCK TABLES `user_types` WRITE;
/*!40000 ALTER TABLE `user_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `auth_id` int(11) DEFAULT NULL,
  `auth_provider` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` int(11) DEFAULT NULL,
  `mobile_no` int(11) DEFAULT NULL,
  `id_user_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','admin@admin.com','users/default.png',NULL,'$2y$10$etHn9U3HdQ3SZS8rR28iXOJMkFZIffG6e.RrOyZAiqcK0oOV1XgdS','SjIlNdgzPx2B4lmRpn6OXTlyldoOzWz3CziqhUvWEh8HESzeZIAyjlPxcMKK',NULL,'2019-01-05 05:12:02','2019-01-05 05:12:02',NULL,NULL,NULL,NULL,NULL),(2,2,'Jai Shankar','jshank94@outlook.com','users/default.png',NULL,'$2y$10$yFapbA4e8OSZRCgYIrcgZu2hZRPmSZngQUX4G6Yt54lG2kIgwIaDW',NULL,NULL,'2019-01-16 05:32:19','2019-01-16 05:32:19',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-29 18:35:00
