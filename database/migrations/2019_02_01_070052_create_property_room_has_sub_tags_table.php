<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyRoomHasSubTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_room_has_sub_tags', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_property_room');
            $table->integer('id_tag')->nullable();
            $table->integer('id_sub_tag')->nullable();
            $table->integer('id_active')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_room_has_sub_tags');
    }
}
