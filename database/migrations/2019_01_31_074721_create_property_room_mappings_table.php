<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyRoomMappingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_room_mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_property');
            $table->string('allowed_extra_bed_count')->nullable();
            $table->string('stay_type')->nullable();
            $table->string('min_stay')->nullable();
            $table->string('booking_type')->nullable();
            $table->integer('cancellation_policy')->nullable();
            $table->string('description')->nullable();
            $table->string('rating_score')->nullable();
            $table->string('is_archieve')->nullable();
            $table->string('archieve')->nullable();
            $table->string('extra_child_price')->nullable();
            $table->string('no_of_guest')->nullable();
            $table->integer('max_child_count')->nullable();
            $table->string('commission')->nullable();
            $table->string('status')->nullable();
            $table->string('accommodation')->nullable();
            $table->string('quantity')->nullable();
            $table->string('id_roomtype')->nullable();
            $table->string('stay_sub_type')->nullable();
            $table->integer('price_per_person')->nullable();
            $table->string('price_per_unit')->nullable();
            $table->string('weekend_price')->nullable();
            $table->string('currency')->nullable();
            $table->string('id_mealplan')->nullable();
            $table->string('meal_price')->nullable();
            $table->string('is_deleted')->nullable();
            $table->integer('extra_bed_price')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_room_mappings');
    }
}
